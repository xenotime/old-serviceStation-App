var cxControllers = angular.module('cxControllers', []);

var menuEnabled = false;

/* master controller */
cxControllers.controller('masterCtrl', ['$scope', '$routeParams', '$rootScope',  
    function($scope, $routeParams, $rootScope) {
			$scope.picklistTitle = "";
			$scope.pick =function(e, values, callback, property){
				var _this = $(e.currentTarget);
				$scope.picklistTitle = _this.text();
				$scope.values = values;
				$scope.callback = callback;
				$scope.property = property;
				$rootScope.go('/picklist', 'slideLeft');
			};
			$scope.getClass = function(type){
	    	if(type === 'Technical_Service_Request'){
	    		return 'technical-service';
	    	} else if (type === 'Software_Download_Service_Request') {
	    		return 'software'
	    	} else return 'rma';
	    };
    }
])

var menuEnabled = false;
/* home controller */
cxControllers.controller('homeCtrl', ['$scope', '$routeParams', '$rootScope', '$http',
	'caseService', 'queryService', function($scope, $routeParams, $rootScope, $http, caseService, queryService) {
								
		$scope.menuEnabled = false;
		if(menuEnabled){
			window.setTimeout(function(){
				menuEnabled = false;	
			},100);
			
			$scope.menuEnabled = true;
		}

		$scope.query = queryService.query;
		$scope.enabledTypes = queryService.enabledTypes;
		$scope.enabledStatus = queryService.enabledStatus;
		
		$scope.search = function(){
			app.showLoading();
			caseService.getAll(queryService.query)
				.success(function(data){
					$scope.casesData = data;
					app.hideLoading();
				})
				.error(function(errorMessage){
					$scope.errorMessage = data;
					app.hideLoading();
				});
		}

		$scope.search();

		$scope.changeSort = function(sort){
			queryService.query.sort = sort;
			$scope.search();
		}

		$scope.changeFilter = function(filter){
			if(filter == 'Technical Service'){
				queryService.toggleType('tech');
			} else if(filter == 'Software Request'){
				queryService.toggleType('sw');
			} else if(filter == 'RMA'){
				queryService.toggleType('rma');
			} else if(filter == 'Unassigned'){
				queryService.toggleStatus('unassigned')
			} else if(filter == 'Closed'){
				queryService.toggleStatus('closed')
			}
			$scope.search();
		}

		$scope.toggleNav = function(){
			$scope.menuEnabled  = !$scope.menuEnabled ;
		}
		
		//sidebar menu toggle
		$scope.toggleList = function(e){
			angular.element(e.target).parent().toggleClass('collapsed');
		}
		
		
		$scope.navTo = function(e,id){
			var page = angular.element(e.currentTarget).attr('class').split( )[0];
			$rootScope.go('case-'+page,'slideLeft','id='+id);
		}
		
		
		$('.sidebarMenu .sortOptions li .option').off().on('click',function(){
			$('.sortEnabled',$(this).closest('.sortOptions')).removeClass('sortEnabled');
			$(this).parent().addClass('sortEnabled');
			return false;
		});
		
		$('.sidebarMenu .filterOptions li .option').off().on('click',function(){
			$scope.changeFilter($(this).context.innerText);
			$(this).parent().toggleClass('sortEnabled');
			return false;
		});	
	}
]);

/* home search controller */
cxControllers.controller('homeSearchCtrl', ['$scope', '$routeParams', '$rootScope','caseService',
    function($scope, $routeParams, $rootScope, caseService) {
    
		angular.element('.searchInput').focus();
		$scope.searchFilter = '';
		
		app.showLoading();
		caseService.getAll()
			.success(function(data){
				$scope.caseData = data;
				app.hideLoading();
			})
			.error(function(errorMessage){
				$scope.errorMessage = errorMessage;
				app.hideLoading();
			});

		$scope.navTo = function(e,id){
			var page = angular.element(e.currentTarget).attr('class').split( )[0];
			$rootScope.go('case-'+page, 'slideLeft','id='+id);
		}
		
		$scope.goTohome = function(){
			$rootScope.go('back', 'slideRight');
		}
		
		
    }
]);

/* case controller */
cxControllers.controller('caseCtrl', ['$scope', '$routeParams','caseService', '$rootScope',
    function($scope, $routeParams, caseService, $rootScope) {

	if(angular.isDefined($routeParams.loaded)){
	    // If already loaded - open by URL
	    $scope.caseData = $rootScope.caseData;
	    $rootScope.caseData = null;
	} else {
	    // Else load from service
	    app.showLoading();
	    caseService.getCaseById($routeParams.id)
		    .success(function(data) {
		      $scope.caseData = data;
		      app.hideLoading();
		    }).error(function(err){
		    	app.hideLoading();
		  		$scope.errorDetail = err.message;
		  		app.showPopup($('.errorPopup'));
		    });
	}
		  $scope.closeCase = function(id) {
		  	app.hidePopup($('.confirmPopup'));
		  	app.showLoading();
		  	caseService.closeById(id)
		  	.success(function(){
		  	    gaPlugin.trackEvent(null, null, "Case", "Closed", 'Case Number', $scope.caseData.caseNumber);
		  		$rootScope.go('home','slideLeft');
		  		app.hideLoading();
		  	}).error(function(errorMessage){
		  		// close popup and show error
		  		app.hideLoading();
		  		$scope.errorDetail = errorMessage;
		  		app.showPopup($('.errorPopup'));
		  	});
		  }

			$scope.confirmClose = function(){
				app.showPopup($('.confirmPopup'));
			}

			$scope.showRmaWarning = function(){
				app.showPopup($('.rmaWarningPopup'));
			}

    }
]);

/* comments controller*/
cxControllers.controller('commentsCtrl',['$scope','$route','$routeParams','$window','commentService',
    function($scope, $route, $routeParams, $window, commentService){
		
		if(angular.isDefined($routeParams.id)){
			app.showLoading();
			$scope.caseId = $routeParams.id;
			
			// query comments if not on new
			if($route.current.loadedTemplateUrl.indexOf('new') == -1){
				commentService.getAllByCaseId($routeParams.id)
					.success(function(data){
						$scope.commentList = data;
						app.hideLoading();
					})
					.error(function(err){
						app.hideLoading();
						$scope.errorDetail = err.message;
		  		app.showPopup($('.errorPopup'));
					})
			} else {
				app.hideLoading();
			}
		}

		$scope.nav2Detials = function(){
			$window.history.go(-1);
		}
		
		$scope.postAdded = function(){
			if(angular.isUndefined($scope.comment) || $scope.comment == ''){
				$scope.errorDetail = {
					message: 'Validation Error',
					details: 'Comment cannot be empty'
				};
				app.showPopup($('.errorPopup'));
			} else { 
				app.showLoading();
				commentService.save($routeParams.id,$scope.comment)
					.success(function(data){
						app.hideLoading();
						gaPlugin.trackEvent(null, null, "Comment", "Created", "Comment Id", data.id);
						app.showPopup($('.confirmPopup'));
						window.setTimeout(function(){
							$window.history.go(-1);
						},3000);
					}).error(function(err){
						app.hideLoading();
			  		    $scope.errorDetail = err.message;
			  		    app.showPopup($('.errorPopup'));
					});
			}		
			
			$('.postedComment .inner').on('click',function(){
				$(this).toggleClass('fluid');
			})
			
			/* expand list on click */
			window.setTimeout(function(){
			$('.postedComment .inner').off().on('click',function(){
				$(this).toggleClass('fluid');
			})
			
			},400);
		}
		
}]);

/* attachments controller */
cxControllers.controller('attachmentsCtrl',['$scope','$routeParams','attachmentService',
	function($scope,$routeParams,attachmentService){
		if(angular.isDefined($routeParams.id)){
			app.showLoading();
			$scope.caseId = $routeParams.id;
			// query attachments
			attachmentService.getAllByCaseId($routeParams.id)
				.then(function(data){
					$scope.attachmentList = data;
					app.hideLoading();
				});
		}
}])

/* edit Attachment  controller */
cxControllers.controller('attachmentEditCtrl',['$scope','$routeParams', '$window','attachmentService',
function($scope,$routeParams,$window, attachmentService){
	$scope.caseId = $routeParams.id;
	$scope.attachmentId = $routeParams.attachmentId;

	app.showLoading();
	attachmentService.getByCaseIdAttachmentId($scope.caseId,$scope.attachmentId)
		.then(function(data){
			$scope.attachment = data;
			app.hideLoading();
		});

	$scope.fileEdited = function(){
		app.showPopup($('.confirmPopup'));
		window.setTimeout(function(){
			$window.location = '#/home';
		},3000);
	}	
	
	$scope.confirmDelete = function(){
		app.showPopup($('.delConfirmPopup'));
	}
	$('.closePopup').off().on('click',function(){
		$('.con header .title').html(currentTitle); currentTitle="";
		$('.overlay').hide();
		$(this).closest('.popup').hide();
	})
}
])

/* newComment controller*/
cxControllers.controller('newCommentCtrl',['$scope','$routeParams',
function($scope,$routeParams){
	$scope.navTo = function(e){
		var page = angular.element(e.currentTarget).attr('class').split( )[0];
		$rootScope.go('case-'+page, 'slideLeft');
	}
}
])

/* newAttachment  controller */
cxControllers.controller('newAttachmentCtrl',['$scope','$routeParams', '$window', '$rootScope', 'attachmentService',
function($scope,$routeParams, $window, $rootScope, attachmentService){
    $scope.libraryLabel = browseLibraryLabel;
    $scope.imageUri = null;
    $scope.fileUri = null;
    
	$scope.getPicture = function(sourceType){
	    // Get picture via native code
	    navigator.camera.getPicture(function(imageUri){
	        SFHybridApp.logToConsole(imageUri);
	        $scope.imageUri = imageUri;
	        // Resolve File URI returned from native code
	        window.resolveLocalFileSystemURI(imageUri, $scope.gotFileEntry, $scope.getFileEntryFailed);
	       
	    }, function(errorMsg){
	        SFHybridApp.logToConsole(errorMsg);
	    }, {
	        quality: 50,
	        sourceType: sourceType,
	        destinationType: Camera.DestinationType.FILE_URI
	    });
	}

    // Fetch metadata when image URI was resolved to local file URI
    $scope.gotFileEntry = function(fileEntry) {
        SFHybridApp.logToConsole("Got file entry");
 	    $scope.fileUri = fileEntry.toURL();
        SFHybridApp.logToConsole("Got file URI: " + $scope.fileUri);
        fileEntry.file($scope.gotMetadata, $scope.getFileFailed);
    }

    // Upload file when we got last modified date.
    $scope.gotMetadata = function(file) {
        SFHybridApp.logToConsole("Size: " + file.size + ", Last Modified: " + file.lastModifiedDate);
        $scope.fileObj = file;
        var reader = new FileReader();
        reader.onloadend = function (evt) {
            var tokens = evt.target.result.split(';');
            if (tokens.length > 1) {
                var prefix = tokens[0].split(':');
                if (prefix.length > 1) {
                    $scope.contentType = prefix[1];
                }
                tokens = tokens[1].split(',');
                if (tokens.length > 1) {
                    $scope.uploadFile(tokens[1]);
                }
            }
        };
        reader.readAsDataURL(file);
    }

    // Upload file with current timestamp if failed to get last modified date.
    $scope.getFileFailed = function() {
        SFHybridApp.logToConsole("Failed to get image metadata");
    }

    // Notify user if we can't resolve the image URI.
    $scope.getFileEntryFailed = function() {
        SFHybridApp.logToConsole("Failed to get file entry");
    }

    // Upload file in give base64 data
    $scope.uploadFile = function(base64Data) {
        var size = $scope.fileObj.size;
        var creationDate = $scope.fileObj.lastModifiedDate;
        SFHybridApp.logToConsole("Start to upload file " + $scope.fileUri +
            ', Last Modified: ' + creationDate + ', Size: ' + size);
        
        app.showLoading();
        if (size == 0 || size > 5 * 1024 * 1024) {
            app.hideLoading();
            $scope.errorDetail = {
                message: 'Validation Error',
                details: 'Image file size must be less than 5MB to upload. Please reduce the file size and try again.'
            };
            app.showPopup($('.errorPopup'));
            return;
        }
        // Post to server
        var fileData = {
            name : $scope.fileObj.name,
            body : base64Data,
            contentType : $scope.contentType
        };
        attachmentService.save($routeParams.id,fileData)
            .success(function(data){
                gaPlugin.trackEvent(null, null, "Attachment", "Created", "Id",
                    data ? data.id : null);
                app.hideLoading();
                app.showPopup($('.confirmPopup'));
                window.setTimeout(function(){
                    $scope.$apply(function() {
                        $window.history.go(-1);
                    })
                },3000);
            }).error(function(err){
                app.hideLoading();
                $scope.errorDetail = err.message;
                app.showPopup($('.errorPopup'));
            });
    }
}
])

/* create case controller */
cxControllers.controller('createEditCaseCtrl',['$scope','$routeParams', '$window', '$rootScope','caseService',
    'GlobalService','$route',
	function($scope,$routeParams, $window, $rootScope, caseService, GlobalService,$route){
		
		app.showLoading();
		$scope.globals = GlobalService;

		//check if jump
		if(angular.isDefined($routeParams.jump)){
			$scope.jump = true;
		}

		if(angular.isDefined($routeParams.line)){
			caseService.currentLine = $routeParams.line;
			$scope.viewLine = (parseInt(caseService.currentLine));
			// if new line create and set dirty
			if(caseService.caseData.items.length <= $routeParams.line){
				caseService.caseData.items[$routeParams.line] = {}
			}
			$scope.currentItem = caseService.caseData.items[$routeParams.line];
			caseService.setDirty(true);
		}

		//if routeparams id, fill
		if(caseService.isDirty()){
			$scope.caseData = caseService.caseData;
			app.hideLoading();
		} else if(angular.isDefined($routeParams.id)){
			// if not dirty and routeparams id, fill
			caseService.getCaseById($routeParams.id)
				.success(function(data){
					caseService.caseData = data;
					$scope.caseData = caseService.caseData;
					app.hideLoading();
				}).error(function(err){
					app.hideLoading();
		  		    $scope.errorDetail = err.message;
		  		    app.showPopup($('.errorPopup'));
				});
		} else {
			//init new
			app.hideLoading();
			caseService.caseData = {};
			$scope.caseData = caseService.caseData;
			if($route.current.loadedTemplateUrl.indexOf('technical') != -1){
				caseService.caseData.type='Technical_Service_Request';
			} else if ($route.current.loadedTemplateUrl.indexOf('software') != -1){
				caseService.caseData.type='Software_Download_Service_Request';
			} else {
				caseService.caseData.type='RMA_Service_Request';
				// init items array
				caseService.caseData.items=[];
			}
		}

		$scope.errorMessage ="";
		$scope.actionSaved = function(){
			app.showLoading();
			// validate
			var caseType = 'Case';
			if($route.current.loadedTemplateUrl.indexOf('technical') != -1){
				// technical validation
				caseType = 'Technical Service Case';
				if(undefinedAndNoValue($scope.caseData.currentPointRelease) 
					|| undefinedAndNoValue($scope.caseData.incidentCity)
					|| undefinedAndNoValue($scope.caseData.incidentCountry)
					|| undefinedAndNoValue($scope.caseData.problemDescription)
					|| undefinedAndNoValue($scope.caseData.problemSummary)
					|| undefinedAndNoValue($scope.caseData.productFamily)
					|| undefinedAndNoValue($scope.caseData.serviceActivity)
					|| undefinedAndNoValue($scope.caseData.specifySoftwareVersion)
					|| undefinedAndNoValue($scope.caseData.urgency)){
					$scope.errorMessage = "Error: Please fill all fields";
					app.hideLoading();
					return;
				}
					
			} else if ($route.current.loadedTemplateUrl.indexOf('software') != -1){
			    caseType = 'Software Case';
				// software validation
				if(undefinedAndNoValue($scope.caseData.currentPointRelease) 
					|| undefinedAndNoValue($scope.caseData.city)
					|| undefinedAndNoValue($scope.caseData.country)
					|| undefinedAndNoValue($scope.caseData.state)
					|| undefinedAndNoValue($scope.caseData.currentPointRelease)
					|| undefinedAndNoValue($scope.caseData.productFamily)
					|| undefinedAndNoValue($scope.caseData.specifySoftwareVersion)
					|| undefinedAndNoValue($scope.caseData.upgradePointRelease)){
					$scope.errorMessage = "Error: Please fill all fields";
					app.hideLoading();
					return;
				}
			}

			caseService
				.save().success(function(){
				    var actionType = angular.isDefined(caseService.caseData.id) ? 'Edited' : 'Created';
				    gaPlugin.trackEvent(null, null, caseType, actionType, "Case Number",
				        caseService.caseData.caseNumber);
					app.hideLoading();
					app.showPopup($('.confirmPopup'));
					window.setTimeout(function(){
						$scope.$apply(function() {
							$scope.resetService();
						  $rootScope.go('/home');
						})
					},3000);
				}).error(function(err){
						app.hideLoading();
						$scope.errorDetail = err.message;
						app.showPopup($('.errorPopup'));
				});
		}

		$scope.resetService = function() {
			caseService.caseData = {};
			caseService.caseData.items = [];
			caseService.setDirty(false);
			caseService.currentLine = 0;
  		caseService.currentItem = {};
		}

		function undefinedAndNoValue(value){
			return angular.isUndefined(value) || value == '';
		}

		$scope.setProperty = function(value,property){
			caseService.setCaseProperty(property,value);
	    $rootScope.go('back', 'slideRight');
		}

		$scope.setItemProperty = function(value,property){
			caseService.caseData.items[caseService.currentLine][property] = value;
	    $rootScope.go('back', 'slideRight');
		}
	
		$('.closePopup').off().on('click',function(){
			$('.con header .title').html(currentTitle); currentTitle="";
			$('.overlay').hide();
			$(this).closest('.popup').hide();
		})
		
		$scope.backAndClear = function(){
			$scope.resetService();
			$rootScope.go('back', 'slideRight');
		}

		$scope.confirmDelete = function(){
			app.showPopup($('.delConfirmPopup'));
		}
		
		$scope.rmaBarcode = _rmaBarcode;
		$scope.rmaLineBarcode = _rmaLineBarcode;

		$scope.barcodeSelect = function(e,item){
			$rootScope.go('/addPartNo', 'slideLeft','line='+caseService.currentLine);
		}

		$scope.addBarcode = function(){
		    var prefix = caseService.caseData.items[caseService.currentLine]['partPrefix'];
		    var suffix = caseService.caseData.items[caseService.currentLine]['partSuffix'];
			if(suffix && suffix.length == 5 && !isNaN(parseInt(suffix))
			    && prefix && !isNaN(parseInt(prefix))){		

			    $scope.errorMessage = null;
			    //TODO: This is hardcoded for the moment, refer to http://apps.topcoder.com/forums/?module=Thread&threadID=813695&start=0
			    var partNumberId = partNumber2Id[prefix + '-' + suffix];
			    if (!partNumberId) {
			        partNumberId = DEFAULT_PART_NUMBER_ID;
			    }
			    caseService.caseData.items[caseService.currentLine]['partNumberId'] = partNumberId;
			    $rootScope.go('back');
			} else {
			    $scope.errorMessage = 'Please enter a valid barcode';
			}
		}		
		
		$scope.pickBarPrefix = function(e){
			$scope.pick(e, ['100', '200', '300', '400', '500', '600', '700', '800']);
		}		
		
		$scope.scanBarcode = function(){
		    barcodeScanner.scan(
		        function (result) {
		            $scope.$apply(function() {
		                caseService.caseData.items[caseService.currentLine]['partPrefix'] = null;
		                caseService.caseData.items[caseService.currentLine]['partSuffix'] = "";
		            });
		            SFHybridApp.logToConsole("We got a barcode\n" +
		                "Result: " + result.text + "\n" +
		                "Format: " + result.format + "\n" +
                        "Cancelled: " + result.cancelled);
		                if (!result.cancelled) {
		                    // barcode must in format of "prefix-suffix revision"
		                    var isValid = true;
		                    var tokens, prefix, suffix;
		                    do {
		                        tokens = result.text.split('-');
		                        if (tokens.length != 2) {
                                SFHybridApp.logToConsole("Tokens.length !=2");
		                            isValid = false;
		                            break;
		                        }
		                        prefix = tokens[0];
                                suffix = tokens[1];
		                    /*    tokens = tokens[1].split(' ');
                                if (tokens.length != 2) {
		                            isValid = false;
		                            break;
		                        }
		                        suffix = tokens[0]; */
		                    } while (0);
		                 
		                    if (isValid) {
            		            $scope.$apply(function() {
            		                caseService.caseData.items[caseService.currentLine]['partPrefix'] = prefix;
            		                caseService.caseData.items[caseService.currentLine]['partSuffix'] = suffix;
            		            });
		                    } else {
                                SFHybridApp.logToConsole("Getting ready to popup message");
		                        navigator.notification.alert("The part number is invalid", null, "Message");
		                    }
		                }
		        }, 
		        function (error) {
		            SFHybridApp.logToConsole("Scanning failed: " + error);
		        },
		        1 // isPartNumber
		    );
		}

		$scope.scanSerialNumber = function(){
		    barcodeScanner.scan(
		        function (result) {
		            SFHybridApp.logToConsole("We got a barcode\n" +
		                "Result: " + result.text + "\n" +
		                "Format: " + result.format + "\n" +
		                "Cancelled: " + result.cancelled);
		                $scope.$apply(function() {
		                    caseService.caseData.items[caseService.currentLine]['serialNumber'] = '';
		                });
		                if (!result.cancelled) {
		                    if (/^\d+$/.test(result.text)) {
		                        $scope.$apply(function() {
		                            caseService.caseData.items[caseService.currentLine]['serialNumber'] = result.text;
		                        });
		                    } else {
		                        navigator.notification.alert("The serial number is invalid", null, "Message");
		                    }
		                }
		        }, 
		        function (error) {
		            SFHybridApp.logToConsole("Scanning failed: " + error);
		        }
		    );
		}

		if(activeEl.length>0 && activeEl.hasClass('rmaLineBarcode')){
			_rmaLineBarcode = activeEl.text();
			$('.rmaLineBarcode').removeClass('isPH').text(_rmaLineBarcode);
		}else if(activeEl.length>0 && activeEl.hasClass('rmaBarcode')){
			_rmaBarcode = activeEl.text();
			$('.rmaBarcode').removeClass('isPH').text(_rmaBarcode);
		}
		
		$scope.toLine = function(){
			//go to create first line, validate first
			if(undefinedAndNoValue($scope.caseData.city)
				|| undefinedAndNoValue($scope.caseData.country)
				|| undefinedAndNoValue($scope.caseData.state)
				|| undefinedAndNoValue($scope.caseData.addressLine1)
				|| undefinedAndNoValue($scope.caseData.addressLine2)
				|| undefinedAndNoValue($scope.caseData.serviceActivity)){
				$scope.errorMessage = "Error: Please fill all fields";
				return;
			}
			$rootScope.go('/createRMALine','','line=0');
		}
		
		/* add another line */
		$scope.newLine = function(){
			// validate line
			if(undefinedAndNoValue($scope.currentItem.partNumberId)
				|| undefinedAndNoValue($scope.currentItem.serialNumber)
				|| undefinedAndNoValue($scope.currentItem.failureCode)
				|| undefinedAndNoValue($scope.currentItem.descriptionOfFailure)){
				$scope.errorMessage = "Error: Please fill all fields";
				return;
			}
			caseService.caseData.items[$scope.viewLine] = $scope.currentItem;
			$rootScope.go('/createRMALine','','line='+(parseInt(caseService.currentLine) + 1));
		}

		$scope.cancel = function(){
			$scope.resetService();
			$rootScope.go('home', 'slideRight');
		}

		$scope.back = function(){
			if(caseService.currentLine == 0){
				//go back to the first screen
				$rootScope.go('/createRma');
			} else {
				//go back one item
				$rootScope.go('/createRMALine','','line='+(parseInt(caseService.currentLine) -1));
			}
		}
		
		/* increment line item n navigate next */
		$scope.review = function(){
			if(undefinedAndNoValue($scope.currentItem.partNumberId)
				|| undefinedAndNoValue($scope.currentItem.serialNumber)
				|| undefinedAndNoValue($scope.currentItem.failureCode)
				|| undefinedAndNoValue($scope.currentItem.descriptionOfFailure)){
				$scope.errorMessage = "Error: Please fill all fields";
				return;
			}
			$rootScope.go('/rmaReview');
		}
	}
])

/* verify Rma Case Ctrl */
cxControllers.controller('verifyRmaCaseCtrl',['$scope','$routeParams', '$window', '$rootScope','caseService',
	function($scope,$routeParams, $window, $rootScope,caseService){
		$scope.caseData = caseService.caseData;
		$scope.actionSaved = function(){
			app.showLoading();
			caseService.save()
				.success(function(){
				    var actionType = angular.isDefined(caseService.caseData.id) ? 'Edited' : 'Created';
				    gaPlugin.trackEvent(null, null, 'RMA Case', actionType, "Case Number",
				        caseService.caseData.caseNumber);
					app.hideLoading();
					app.showPopup($('.confirmPopup'));
					window.setTimeout(function(){
						$scope.$apply(function() {
							$scope.resetService();
						  $rootScope.go('/home');
						})
					},3000);
				}).error(function(err){
					app.hideLoading();
					$scope.errorDetail = err;
					app.showPopup($('.errorPopup'));
				})
		}

		$scope.cancel = function(){
			$scope.resetService();
			$rootScope.go('home', 'slideRight');
		}

		$scope.resetService = function() {
			caseService.caseData = {};
			caseService.caseData.items = [];
			caseService.setDirty(false);
			caseService.currentLine = 0;
  		caseService.currentItem = {};
		}
	
		$('.closePopup').off().on('click',function(){
			$('.con header .title').html(currentTitle); currentTitle="";
			$('.overlay').hide();
			$(this).closest('.popup').hide();
		})
		
		$scope.confirmDelete = function(){
			app.showPopup($('.delConfirmPopup'));
		}
		
		/* increment line item n navigate next */
		$scope.incLineNgo=function(){
			lineItmes+=1;
			$rootScope.go('/rmaReview');
		}
		
		if(lineItmes>1){
			$('.rmaLineItem.item2').show();
		}
	}
])


var lineItmes=0;
var activeEl = "";
var _rmaBarcode = "Add Part Number";
var _rmaLineBarcode = "Add Part Number";
