var app = {};
var forcetkClient;
var debugMode = true;
var isAppStarted = false;
var openUrl;

// The Force.com API version
var apiVersion;


app.init = function(){
	// splash demo
	if($('#splash:visible').length>0){
		window.setTimeout(function(){window.location = 'index.html';},3000);
	}
	
	document.addEventListener("deviceready", onDeviceReady, false);
}

function handleOpenURL(url) {
    openUrl = url;
    if (isAppStarted) {
        processOpenUrl();
    }
}

function processOpenUrl() {
    var stackOpenUrl = openUrl;
    openUrl = null;
    var rootScope = angular.element('html').scope();
    rootScope.$apply(function() {
        rootScope.handleOpenUrl(stackOpenUrl);
    });
}

function onDeviceReady() {
    SFHybridApp.logToConsole("onDeviceReady: Cordova ready");
    gaPlugin.init(function (result) {
                      SFHybridApp.logToConsole("GAPlugin initialized: " + result);
                  },
                  function (error) {
                      SFHybridApp.logToConsole("Failed to initialize GAPlugin: " + error);
                  }, gaTrackingId, gaDispatchInterval);
    // Get auth information
    SalesforceOAuthPlugin.getAuthCredentials(app.salesforceSessionRefreshed, app.getAuthCredentialsError);
    document.addEventListener("salesforceSessionRefresh", app.salesforceSessionRefreshed, false);
}

app.salesforceSessionRefreshed = function(creds) {
        SFHybridApp.logToConsole("salesforceSessionRefreshed");
		
        // Depending on how we come into this method, `creds` may be callback data from the auth
        // plugin, or an event fired from the plugin.  The data is different between the two.
        var credsData = creds;
        if (creds.data)  // Event sets the `data` object with the auth data.
            credsData = creds.data;

        forcetkClient = new forcetk.Client(credsData.clientId, credsData.loginUrl, null,
            cordova.require("salesforce/plugin/oauth").forcetkRefresh);
        forcetkClient.setSessionToken(credsData.accessToken, apiVersion, credsData.instanceUrl);
        forcetkClient.setRefreshToken(credsData.refreshToken);
        forcetkClient.setUserAgentString(credsData.userAgent);
        
        if (!isAppStarted) {
            angular.bootstrap(document.getElementById('cxApp'), ["cxApp"]);
            isAppStarted = true;
            processOpenUrl();
        }
}
 
 
app.getAuthCredentialsError = function(error) {
    SFHybridApp.logToConsole("getAuthCredentialsError: " + error);
}

app.delayedInit = function(){
	$(document).on('webkitTransitionEnd','.sidebarMenu',function(){
		window.setTimeout( function(){
			$('body').toggleClass('fix');
			},200);
	});
	
	
	$(document).on('click','.eotEnabled .ellipsis', function(){
		var cCmnt = $(this).closest('.postedComment');
		var fht = $('span',cCmnt).height()+5;
		cCmnt.css('max-height',fht+'px');
		cCmnt.removeClass('eotEnabled');
		return false;
	});
	$('article.screen .con').css('min-height',$('#wrapper').height()+'px');
	
}

currentTitle="";
app.showPopup = function(el){
    if (el.hasClass('errorPopup')) {
        // Save title for restore if this is an error popup
	    currentTitle = $('.con header .title').html();
    } else {
        currentTitle = null;
    }
	var newT = el.attr('title');
	if($.trim(newT)!=""){
		$('.con header .title').html(newT);
	}
	$('.overlay').css('min-height', $('article.screen').height()+'px');
	$('.overlay').show();
	el.show();
	// Auto hide if it's message only dialog
	if (el.hasClass('msgOnly')) {
	    window.setTimeout(function() {
	        app.hidePopup(el);
	    },5000);
	}
}

app.hidePopup = function(el){
	$('.overlay').hide();
	el.hide();
	if (currentTitle) {
	    $('.con header .title').html(currentTitle);
	}
}

app.showLoading = function(el){
	$('.overlay').css('min-height', $('article.screen').height()+'px');
	$('.overlay').show();
	$('.loading').show();
}

app.hideLoading = function(el){
	$('.overlay').hide();
	$('.loading').hide();
}
// bootstraping
$(document).ready(function() {
	app.init();
	window.setTimeout(app.delayedInit,100);
});
$('.sidebarMenu .title').on('click',function(){
	$scope.$apply(function(){
		$scope.myAngularVariable = !$scope.myAngularVariable;
	})
})