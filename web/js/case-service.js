cxApp.service('caseService', function($http) {

  this.caseData = {};
  this.currentLine = 0;
  this.currentItem = {};
  var dirty = false;

  function getQueryString(query){
    if(angular.isUndefined(query)){
      return "";
    }
    var queryString = "?";
    if(query.type != ''){
      queryString += 'type='+query.type + '&';
    }
    if(query.sort != ''){
      queryString += 'sort='+query.sort +'&';
    }
    if(query.status != ''){
      queryString += 'status='+query.status;
    }
    return queryString;
  }

  var caseService = {
    getAll: function(query) {
      // $http returns a promise, which has a then function, which also returns a promise
      var promise = $http({
        method: 'GET',
        url: serviceEndpoint + 'caseServices'+getQueryString(query),
        headers: {'Authorization':'OAuth '+forcetkClient.sessionId}
      });
      return promise;
    },
    getCaseById: function(id) {
      var promise = $http({
        method: 'GET',
        url: serviceEndpoint + 'caseServices/' + id,
        headers: {'Authorization':'OAuth '+forcetkClient.sessionId}
      });
      return promise;
    },
    closeById: function(id){
      var promise = $http({
        method: 'PUT',
        url: serviceEndpoint + 'caseServices/' + id+'/close',
        headers: {'Authorization':'OAuth '+forcetkClient.sessionId}
      });
      return promise;
    },
    save: function () {

      if(angular.isDefined(this.caseData.id)){
        // filter fields depending on type
        if(this.caseData.type === 'Technical_Service_Request'){
          delete this.caseData.items;
        } else if (this.caseData.type === 'Software_Download_Service_Request') {
          delete this.caseData.city;
          delete this.caseData.state;
          delete this.caseData.country;
          delete this.caseData.items;
        } else{
          this.caseData.serviceActivity = null;
        } 
        //remove id
        var _id = this.caseData.id;
        delete this.caseData.id;
        // status cannot be assigned
        delete this.caseData.status;
        // Subscribers Affected is not editable
        delete this.caseData.numberOfSubscribersAffected;
        // reported severity is not editable
        delete this.caseData.reportedSeverity;
        // $http returns a promise, which has a then function, which also returns a promise
        var promise = $http({
          method: 'PUT',
          url: serviceEndpoint + 'caseServices/' + _id,
          headers: {'Authorization':'OAuth '+forcetkClient.sessionId},
          data: this.caseData
        });
        
        return promise;
      } else {
        if(this.caseData.type === 'Technical_Service_Request'){
          delete this.caseData.items;
        } else if (this.caseData.type === 'Software_Download_Service_Request') {
          delete this.caseData.items;
        } else{
          
        } 
        // insert new
        var promise = $http({
          method: 'POST',
          url: serviceEndpoint + 'caseServices',
          headers: {'Authorization':'OAuth '+forcetkClient.sessionId},
          data: this.caseData
        });
        
        // Return the promise to the controller
        return promise;
      }
    },
    setCurrentItem: function (data) {
        dirty = true;
        this.currentItem = data;
    },
    setCase: function (data) {
        dirty = true;
        caseData = data;
    },
    setCaseProperty: function (property,value) {
      dirty = true;
      this.caseData[property] = value;
    },
    setCaseItemProperty: function (property,value,index) {
      dirty = true;
      this.caseData.items[index][property] = value;
    },
    getCase: function () {
        return caseData;
    },
    isDirty: function () {
        return dirty;
    },
    setDirty: function (data) {
        dirty = data;
    },
    clear: function (){
      reset();
    }
  };
  return caseService;
});

cxApp.service('commentService', function($http) {

  var commentsService = {
    getAllByCaseId: function(id) {
      // $http returns a promise, which has a then function, which also returns a promise
      var promise = $http({
        method: 'GET',
        url: serviceEndpoint + 'caseServices/'+id+'/comments',
        headers: {'Authorization':'OAuth '+forcetkClient.sessionId}
      });
      // Return the promise to the controller
      return promise;
    },
    save: function (id,data) {
      // insert new
      var promise = $http({
        method: 'POST',
        url: serviceEndpoint + 'caseServices/'+id+'/comments',
        headers: {'Authorization':'OAuth '+forcetkClient.sessionId},
        data: data
      });
      // Return the promise to the controller
      return promise;
    }
  };
  return commentsService;
});

cxApp.service('attachmentService', function($http) {

  var attachmentsService = {
    getAllByCaseId: function(id) {
      // $http returns a promise, which has a then function, which also returns a promise
      var promise = $http({
        method: 'GET',
        url: serviceEndpoint + 'caseServices/'+id+'/attachments',
        headers: {'Authorization':'OAuth '+forcetkClient.sessionId}
      }).then(function (response) {
        return response.data;
      });
      // Return the promise to the controller
      return promise;
    },
    getByCaseIdAttachmentId: function(id, attachmentId) {
      var promise = $http({
        method: 'GET',
        url: serviceEndpoint + 'caseServices/'+id+'/attachments/'+attachmentId,
        headers: {'Authorization':'OAuth '+forcetkClient.sessionId}
      }).then(function (response) {
        return response.data;
      });
      return promise;
    },
    save: function (id,data) {
      // insert new
      var promise = $http({
        method: 'POST',
        url: serviceEndpoint + 'caseServices/'+id+'/attachments',
        headers: {'Authorization':'OAuth '+forcetkClient.sessionId},
        data: data
      });
      return promise;
    }
  };
  return attachmentsService;
});

cxApp.service('queryService', function() {

  this.query = {
      status: 'unassigned',
      type: 'rma,sw,tech',
      sort: 'dcd'
    };

  this.enabledTypes = {
    rma: true,
    sw: true,
    tech: true
  };

  this.enabledStatus = {
    unassigned: true,
    closed: false
  };


  this.toggleType = function(type) {
    this.enabledTypes[type] = !this.enabledTypes[type];
    this.query.type = "";
    if(this.enabledTypes.tech){
      this.query.type += 'tech,'
    }
    if(this.enabledTypes.rma){
      this.query.type += 'rma,'
    }
    if(this.enabledTypes.sw){
      this.query.type += 'sw'
    }
  }

  this.toggleStatus = function(status) {
    this.enabledStatus[status] = !this.enabledStatus[status];
    this.query.status = "";
    if(this.enabledStatus.unassigned){
      this.query.status += 'unassigned,'
    }
    if(this.enabledStatus.closed){
      this.query.status += 'closed'
    }
  }

});

