<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <content>CM_CreateRMARequestLine</content>
        <skipRecordTypeSelect>false</skipRecordTypeSelect>
        <type>Visualforce</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <deploymentStatus>Deployed</deploymentStatus>
    <enableActivities>true</enableActivities>
    <enableEnhancedLookup>false</enableEnhancedLookup>
    <enableFeeds>false</enableFeeds>
    <enableHistory>false</enableHistory>
    <enableReports>true</enableReports>
    <fields>
        <fullName>AID__c</fullName>
        <externalId>false</externalId>
        <label>AID (NX-X-X)</label>
        <length>80</length>
        <required>false</required>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Account_Name__c</fullName>
        <externalId>false</externalId>
        <formula>Calix_Service_Request_Number__r.Account.Name</formula>
        <label>Account Name</label>
        <required>false</required>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Affected_Ports__c</fullName>
        <externalId>false</externalId>
        <label>Affected Ports</label>
        <length>80</length>
        <required>false</required>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Calix_Service_Request_Number__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <externalId>false</externalId>
        <label>Calix Case Number</label>
        <referenceTo>Case</referenceTo>
        <relationshipLabel>RMA Request Lines</relationshipLabel>
        <relationshipName>Request_Lines</relationshipName>
        <required>false</required>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Card_was_software_reset_manually_reset__c</fullName>
        <defaultValue>false</defaultValue>
        <externalId>false</externalId>
        <label>Card was software reset/manually reset?</label>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Case_Record_Type__c</fullName>
        <externalId>false</externalId>
        <formula>IF(OR(Calix_Service_Request_Number__r.RecordTypeId =&apos;01270000000E19V&apos;,  ISPICKVAL(Calix_Service_Request_Number__r.Status, &quot;Closed&quot;)), &apos;RMA SR&apos;, null)</formula>
        <label>Case Record Type</label>
        <required>false</required>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Description__c</fullName>
        <externalId>false</externalId>
        <label>Description of Failure</label>
        <length>32768</length>
        <type>LongTextArea</type>
        <visibleLines>3</visibleLines>
    </fields>
    <fields>
        <fullName>Environmental_Influence__c</fullName>
        <externalId>false</externalId>
        <label>Environmental Influence</label>
        <length>32768</length>
        <type>LongTextArea</type>
        <visibleLines>3</visibleLines>
    </fields>
    <fields>
        <fullName>Failure_Analysis_Requested__c</fullName>
        <externalId>false</externalId>
        <label>Failure Analysis Requested</label>
        <picklist>
            <picklistValues>
                <fullName>No</fullName>
                <default>true</default>
            </picklistValues>
            <picklistValues>
                <fullName>Yes</fullName>
                <default>false</default>
            </picklistValues>
            <sorted>false</sorted>
        </picklist>
        <type>Picklist</type>
    </fields>
    <fields>
        <fullName>Failure_Code__c</fullName>
        <externalId>false</externalId>
        <label>Failure Code</label>
        <picklist>
            <picklistValues>
                <fullName>Card Failure</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Boot Failure</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Degraded Performance</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Port Failure</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Software Failure</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>ONT Rogue Detected</fullName>
                <default>false</default>
            </picklistValues>
            <sorted>false</sorted>
        </picklist>
        <type>Picklist</type>
    </fields>
    <fields>
        <fullName>Market_Revenue__c</fullName>
        <externalId>false</externalId>
        <label>Market Revenue</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>Modified_provisioning__c</fullName>
        <defaultValue>false</defaultValue>
        <externalId>false</externalId>
        <label>Modified provisioning?</label>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Network_Name__c</fullName>
        <externalId>false</externalId>
        <label>Network Name</label>
        <length>80</length>
        <required>false</required>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Network_Ring__c</fullName>
        <externalId>false</externalId>
        <label>Network/ Ring</label>
        <length>80</length>
        <required>false</required>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Number_of_Subscribers_Affected__c</fullName>
        <externalId>false</externalId>
        <label>Number of Subscribers Affected</label>
        <precision>18</precision>
        <required>false</required>
        <scale>0</scale>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Onsite_Date__c</fullName>
        <externalId>false</externalId>
        <label>Onsite Date</label>
        <picklist>
            <picklistValues>
                <fullName>STANDARD – 3 DAY</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>PRIORITY – Next Business Day</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Other – See Special Instructions</fullName>
                <default>false</default>
            </picklistValues>
            <sorted>false</sorted>
        </picklist>
        <type>Picklist</type>
    </fields>
    <fields>
        <fullName>Oracle_Part_Number__c</fullName>
        <externalId>false</externalId>
        <label>Oracle Part Number</label>
        <length>10</length>
        <required>false</required>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Oracle_Software_Release__c</fullName>
        <externalId>false</externalId>
        <label>Oracle Software Release</label>
        <length>80</length>
        <required>false</required>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Other_specify__c</fullName>
        <externalId>false</externalId>
        <label>Other (specify)</label>
        <length>80</length>
        <required>false</required>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>PartNumber__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <description>Part Number lookup to Product2 object</description>
        <externalId>false</externalId>
        <label>Part Number</label>
        <referenceTo>Product2</referenceTo>
        <relationshipLabel>RMA Request Line</relationshipLabel>
        <relationshipName>RMA_Request_Line</relationshipName>
        <required>false</required>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Part_Name__c</fullName>
        <externalId>false</externalId>
        <label>Part Name</label>
        <picklist>
            <picklistValues>
                <fullName>C7 Shelf-20</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>C7 Shelf-20c</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>B6-001 Power Cable Kit</fullName>
                <default>false</default>
            </picklistValues>
            <sorted>false</sorted>
        </picklist>
        <type>Picklist</type>
    </fields>
    <fields>
        <fullName>Part_Number_Lookup__c</fullName>
        <description>This field is used to display the part number without the hyperlink to customer portal user</description>
        <externalId>false</externalId>
        <formula>PartNumber__r.Name</formula>
        <label>Part Number</label>
        <required>false</required>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Part_Number__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <externalId>false</externalId>
        <label>Part Number</label>
        <referenceTo>Product__c</referenceTo>
        <relationshipLabel>RMA Request Line</relationshipLabel>
        <relationshipName>RMA_Request_Line</relationshipName>
        <required>false</required>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Product_Line__c</fullName>
        <externalId>false</externalId>
        <label>Product Family</label>
        <picklist>
            <picklistValues>
                <fullName>B- Series</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>C-Series</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>E-Series</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>F-Series</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>P-Series (ONTs)</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Software Products</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Other</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>BLM-Series</fullName>
                <default>false</default>
            </picklistValues>
            <sorted>false</sorted>
        </picklist>
        <type>Picklist</type>
    </fields>
    <fields>
        <fullName>Quantity__c</fullName>
        <externalId>false</externalId>
        <label>Quantity</label>
        <length>80</length>
        <required>false</required>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>RMA_Request_Line_ID__c</fullName>
        <caseSensitive>false</caseSensitive>
        <externalId>true</externalId>
        <label>RMA Request Line ID</label>
        <length>10</length>
        <required>false</required>
        <type>Text</type>
        <unique>true</unique>
    </fields>
    <fields>
        <fullName>Reason_Code__c</fullName>
        <externalId>false</externalId>
        <label>Reason Code</label>
        <picklist>
            <picklistValues>
                <fullName>In Service Failure</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Out of Box Failure</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Recall</fullName>
                <default>false</default>
            </picklistValues>
            <sorted>false</sorted>
        </picklist>
        <type>Picklist</type>
    </fields>
    <fields>
        <fullName>Return_Reason__c</fullName>
        <externalId>false</externalId>
        <label>Return Reason</label>
        <length>32768</length>
        <type>LongTextArea</type>
        <visibleLines>2</visibleLines>
    </fields>
    <fields>
        <fullName>Revision__c</fullName>
        <externalId>false</externalId>
        <label>Revision</label>
        <length>255</length>
        <required>false</required>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Serial_Number__c</fullName>
        <externalId>false</externalId>
        <label>Serial Number</label>
        <length>20</length>
        <required>false</required>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Service_Activity__c</fullName>
        <externalId>false</externalId>
        <label>Service Activity</label>
        <picklist>
            <picklistValues>
                <fullName>In Service Failure</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>In-Service Failure-Maintenance</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>In-Service Failure-Upgrade</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Out of Box Failure</fullName>
                <default>false</default>
            </picklistValues>
            <sorted>false</sorted>
        </picklist>
        <type>Picklist</type>
    </fields>
    <fields>
        <fullName>Shelf_Serial_Number__c</fullName>
        <externalId>false</externalId>
        <label>Shelf Serial Number</label>
        <length>80</length>
        <required>false</required>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Software_Release_Lookup__c</fullName>
        <description>This field is used to display the software release without the hyperlink to customer portal user</description>
        <externalId>false</externalId>
        <formula>Software_Releases__r.Name</formula>
        <label>Software Release</label>
        <required>false</required>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Software_Releases__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <externalId>false</externalId>
        <label>Software Release</label>
        <referenceTo>Software_Center_Listings__c</referenceTo>
        <relationshipLabel>RMA Request Line</relationshipLabel>
        <relationshipName>RMA_Request_Line</relationshipName>
        <required>false</required>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Special_Instructions__c</fullName>
        <externalId>false</externalId>
        <label>Special Instructions</label>
        <length>255</length>
        <required>false</required>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Switched_protection__c</fullName>
        <defaultValue>false</defaultValue>
        <externalId>false</externalId>
        <label>Switched protection?</label>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Total_Outage_Minutes__c</fullName>
        <externalId>false</externalId>
        <label>Total Outage Minutes</label>
        <precision>18</precision>
        <required>false</required>
        <scale>0</scale>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Tried_in_another_slot__c</fullName>
        <defaultValue>false</defaultValue>
        <externalId>false</externalId>
        <label>Tried in another slot?</label>
        <type>Checkbox</type>
    </fields>
    <label>RMA Request Line</label>
    <listViews>
        <fullName>All</fullName>
        <filterScope>Everything</filterScope>
        <label>All</label>
    </listViews>
    <nameField>
        <displayFormat>R-{0000}</displayFormat>
        <label>Request Line Number</label>
        <type>AutoNumber</type>
    </nameField>
    <pluralLabel>RMA Request Line</pluralLabel>
    <searchLayouts>
        <searchResultsAdditionalFields>Part_Number_Lookup__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>Part_Name__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>Product_Line__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>Service_Activity__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>Serial_Number__c</searchResultsAdditionalFields>
    </searchLayouts>
    <sharingModel>ReadWrite</sharingModel>
    <validationRules>
        <fullName>RMACaseMandatory</fullName>
        <active>false</active>
        <description>RMA Case Mandatory to Create RMA Request Line</description>
        <errorConditionFormula>OR(ISBLANK(Calix_Service_Request_Number__c), NOT(OR( Calix_Service_Request_Number__r.RecordType.DeveloperName = &apos;RMA_Service_Request&apos;,Calix_Service_Request_Number__r.RecordType.DeveloperName = &apos;RMA_SR&apos;)))</errorConditionFormula>
        <errorDisplayField>Calix_Service_Request_Number__c</errorDisplayField>
        <errorMessage>Please select RMA case to create RMA Request Line</errorMessage>
    </validationRules>
    <validationRules>
        <fullName>RequestLineNotEditable</fullName>
        <active>true</active>
        <errorConditionFormula>IF(AND(Id!=null, Calix_Service_Request_Number__r.RecordTypeId =&apos;01270000000E19V&apos;), TRUE, FALSE)</errorConditionFormula>
        <errorMessage>This RMA Request has already been submitted. Please contact Calix RMA Management if you have any questions.</errorMessage>
    </validationRules>
    <validationRules>
        <fullName>RestrictSpaceChar</fullName>
        <active>true</active>
        <description>Restrict Space Character in Serial Number</description>
        <errorConditionFormula>CONTAINS(  Serial_Number__c  , &quot; &quot;)</errorConditionFormula>
        <errorDisplayField>Serial_Number__c</errorDisplayField>
        <errorMessage>Spaces are not allowed in serial number, please remove spaces</errorMessage>
    </validationRules>
    <validationRules>
        <fullName>SoftwareReleaseFilterCriteria</fullName>
        <active>true</active>
        <errorConditionFormula>AND(OR( $Profile.Name &lt;&gt; &apos;Calix HVCP Manager&apos;, $Profile.Name &lt;&gt; &apos;Calix HVCP User&apos;, $Profile.Name &lt;&gt; &apos;Calix HVCP Partner Manager&apos;, $Profile.Name &lt;&gt; &apos;Calix HVCP Partner User&apos;),AND(TEXT(Software_Releases__r.Product_Family__c ) &lt;&gt; NULL , IF(PartNumber__r.Product_Family__r.Name &lt;&gt; TEXT(Software_Releases__r.Product_Family__c ), TRUE, FALSE)))</errorConditionFormula>
        <errorDisplayField>Software_Releases__c</errorDisplayField>
        <errorMessage>Please select the software related to the selected part number</errorMessage>
    </validationRules>
    <validationRules>
        <fullName>SoftwareReleaseRequired</fullName>
        <active>true</active>
        <description>On the RMA Service Request, if this part is chosen then the Software Load is required field if SWC Sensitive field is set to Yes</description>
        <errorConditionFormula>IF(AND($Profile.Name!= &apos;Calix HVCP User&apos;, $Profile.Name!= &apos;Calix HVCP Manager&apos;, $Profile.Name!= &apos;Calix HVCP Partner User&apos;, $Profile.Name!= &apos;Calix HVCP Partner Manager&apos;, ISPICKVAL(PartNumber__r.SWC_Sensitive__c, &quot;Yes&quot;)), ISBLANK(Software_Releases__c) ,false)</errorConditionFormula>
        <errorDisplayField>Software_Releases__c</errorDisplayField>
        <errorMessage>Software Release is required  if SWC Sensitive field is set to Yes on Part Number field.</errorMessage>
    </validationRules>
    <webLinks>
        <fullName>Add_Line_Item</fullName>
        <availability>online</availability>
        <displayType>massActionButton</displayType>
        <linkType>javascript</linkType>
        <masterLabel>Add Line Item</masterLabel>
        <openType>onClickJavaScript</openType>
        <protected>false</protected>
        <requireRowSelection>true</requireRowSelection>
        <url>sforce.connection.sessionId = &apos;{!$Api.Session_ID}&apos;;

var caseId = &quot;{!Case.Id}&quot;;
//alert(&quot;The Case ID is &quot;+caseId);
var cases= sforce.connection.query (&quot;select Id from RMA_Request_Line__c where Calix_Service_Request_Number__c=&apos;&quot;+ caseId + &quot;&apos;&quot; );
 var casesRecords = cases.getArray(&quot;records&quot;);
//alert(&quot;The Total records available are &quot;+casesRecords.length);
if(casesRecords!=null &amp;&amp; casesRecords.length&gt;=25){
alert(&quot;The limit of 25 lines has been reached. Please click on Submit Request to submit this RMA Request.  If you have additional failed units, please submit them on a new Case.&quot;);
}else{
window.parent.location.href = &quot;/apex/CM_createRMArequestline?caseid=&quot;+caseId;
}</url>
    </webLinks>
    <webLinks>
        <fullName>Add_RMA_Item</fullName>
        <availability>online</availability>
        <displayType>massActionButton</displayType>
        <linkType>url</linkType>
        <masterLabel>Add RMA Item</masterLabel>
        <openType>replace</openType>
        <protected>false</protected>
        <requireRowSelection>false</requireRowSelection>
        <url>/a0T/e?CF00N70000002cRha={!Case.CaseNumber}&amp;CF00N70000002cRha_lkid={!Case.Id}&amp;retURL=/{!Case.Id}&amp;saveURL=/{!Case.Id}</url>
    </webLinks>
    <webLinks>
        <fullName>Back_to_Case</fullName>
        <availability>online</availability>
        <description>https://cs3.salesforce.com/{!RMA_Request_Line__c.Calix_Service_Request_NumberId__c}</description>
        <displayType>button</displayType>
        <linkType>url</linkType>
        <masterLabel>Back to Case</masterLabel>
        <openType>replace</openType>
        <protected>false</protected>
        <url>/{!RMA_Request_Line__c.Calix_Service_Request_NumberId__c}</url>
    </webLinks>
    <webLinks>
        <fullName>Copy</fullName>
        <availability>online</availability>
        <displayType>button</displayType>
        <linkType>javascript</linkType>
        <masterLabel>Copy</masterLabel>
        <openType>onClickJavaScript</openType>
        <protected>false</protected>
        <url>{!REQUIRESCRIPT(&quot;/soap/ajax/12.0/connection.js&quot;)};
sforce.connection.session = &quot;{!$Api.Session_ID}&quot;;
var caseId = &quot;{!RMA_Request_Line__c.Calix_Service_Request_NumberId__c}&quot;;
var cases= sforce.connection.query (&quot;select Id from RMA_Request_Line__c where Calix_Service_Request_Number__c=&apos;&quot;+ caseId + &quot;&apos;&quot; );
var casesRecords = cases.getArray(&quot;records&quot;);
//alert(&quot;The Total records available are &quot;+casesRecords.length);
if(casesRecords!=null &amp;&amp; casesRecords.length&gt;=25){
alert(&quot;The limit of 25 lines has been reached. Please click on Submit Request to submit this RMA Request. If you have additional failed units, please submit them on a new Case.&quot;);
}else{
//window.parent.location.href = &quot;/apex/CM_createRMArequestline?caseid=&quot;+caseId;
var rmaLineId = &quot;{!RMA_Request_Line__c.Id}&quot;;
window.parent.location.href = &quot;/apex/CM_CreateRMARequestLine?id=&quot;+rmaLineId+&quot;&amp;clone=1&quot;;
}</url>
    </webLinks>
</CustomObject>
