/**
 * SVST_AppNotificationServices REST APIs class. Handles GET/POST requests for notification setting.
 *
 * @author A.I
 * @version 1.0
 */
@RestResource(urlMapping='/notificationServices/*') 
global class SVST_AppNotificationServices {
    public static final String E000 = 'E000';
    public static final String E000_MESSAGE = 'Internal error';

    @HttpGet
    global static void getAction(){
    	getNotificatioSettings();
    }
    @HttpPost
    global static void postAction(){
    	handleNotificatioSettingsRequest();
    }

	public static void testNotification(){
		NotificationSettings nts = new NotificationSettings();
		nts.newComments = true;
		nts.statusChanges = true;
		NotificationSettings ns = setNotifications(nts);
		System.debug('JSON: '+JSON.serializePretty(ns));
	}
	
    private static void handleNotificatioSettingsRequest(){
     	
        RestRequest req = RestContext.request;
        RestResponse res = RestContext.response;
                
        try{
 
 			SVST_AppNotificationServices.NotificationSettings parsed = 
             (SVST_AppNotificationServices.NotificationSettings)JSON.deserialize(req.requestBody.toString(), SVST_AppNotificationServices.NotificationSettings.class);
        	
        	NotificationSettings met = setNotifications(parsed);
			returnResponse(res, met, 200);
			        	
        }catch(Exception e){
            returnResponse(res, new CaseServicesParser.RestError(E000, E000_MESSAGE, 
                                                                    CaseServicesParser.getStackTrace(e)
                                                                    +e.getMessage()),400);
        }            	
    }

    private static void getNotificatioSettings(){
     	
        RestRequest req = RestContext.request;
        RestResponse res = RestContext.response;
                
        try{
        	
        	NotificationSettings met = getNotifications();
			returnResponse(res, met, 200);
			        	
        }catch(Exception e){
            returnResponse(res, new CaseServicesParser.RestError(E000, E000_MESSAGE, 
                                                                    CaseServicesParser.getStackTrace(e)
                                                                    +e.getMessage()),400);
        }            	
    }
    
    private static NotificationSettings setNotifications(NotificationSettings nts){
    	// Save settings to Setting object for each user
    	SVST_Notification__c nfObj = getNotificationsSettings();
    	nfObj.User_Settings__c = UserInfo.getUserId();
            	
    	nfObj.New_Comments__c = nts.newComments;
    	nfObj.Status_Change__c = nts.statusChanges;
    	upsert nfObj;
    	   	
    	return nts;
    }

    private static NotificationSettings getNotifications(){
    	NotificationSettings nts = new NotificationSettings();
    	// Query settings to Setting object for each user
    	SVST_Notification__c nfObj = getNotificationsSettings();
   		nts.newComments = nfObj.New_Comments__c;
   		nts.statusChanges = nfObj.Status_Change__c;
    	
    	return nts;
    }
    
    public static SVST_Notification__c getNotificationsSettings(){
    	String usrId = UserInfo.getUserId();
    	List<SVST_Notification__c> nfObj = [Select User_Settings__c, Status_Change__c, New_Comments__c, Name, Id From SVST_Notification__c where User_Settings__c =: usrId];
    	if (nfObj!=null && nfObj.size()>0){
    		return nfObj[0];
    	}else{ 
    		// Set default S-object   
    		SVST_Notification__c nf = new SVST_Notification__c();
    		nf.User_Settings__c = usrId;
  			nf.New_Comments__c = true;
    		nf.Status_Change__c = true;      			
    		return nf;
    	}
    }
    
    public static void returnResponse(RestResponse res, Object body, Integer status){
        res.statusCode = status;
        res.headers.put('content-type','application/json');
        res.responseBody = Blob.valueOf(JSON.serialize(body));
    }
    
    public class NotificationSettings  {
    	public Boolean newComments{get; set;}
    	public Boolean statusChanges{get;set;}
    	public NotificationSettings(){}
    	public NotificationSettings(SVST_Notification__c n){
    		this.newComments = n.New_Comments__c;
    		this.statusChanges = n.Status_Change__c;	
    	}
    	
    }
}