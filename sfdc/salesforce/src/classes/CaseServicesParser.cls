/*
 * Copyright (C) 2014 TopCoder Inc., All Rights Reserved.
 */
/**
 * This is the helper class used to parse request bodies on 
 * CaseServices REST APIs.
 *
 * @author TCASSEMBLER
 * @version 1.0
 */
public class CaseServicesParser {
    /* Sensible profiles names*/
    public static final String PROFILE_CALIX_PARTNER_USER = 'Calix HVCP Partner User';
    public static final String PROFILE_CALIX_PARTNER_Manager = 'Calix HVCP Partner Manager';
    
    /* Case RecordType.DeveloperName*/
    public static final String CASE_RTYPE_TECHSUPPORT = 'Technical_Service_Request';
    public static final String CASE_RTYPE_RMA = 'RMA_Service_Request';
    public static final String CASE_RTYPE_RMASR = 'RMA_Read_Only';
    public static final String CASE_RTYPE_SOFTWAREREQ = 'Software_Download_Service_Request';
    
    /* Fields used throughout the CaseServices implementation */
    public static final String CASE_FIELDS = 'Id,CaseNumber,CreatedDate,lastModifiedDate,RecordType.DeveloperName,'
                                                +'RecordType.Name,Account.Name,'
                                                +'Contact.Name,Urgency__c,Product_Family__c,Service_Activity__c,'
                                                +'current_point_release__c,Other_Upg_to_Software_Release__c,'
                                                +'problem_summary__c,Description__c,resolution_summary__c,City__c,'
                                                +'Country__c,reported_severity__c,of_Subscribers_Affected__c,status,'
                                                +'ShipTo_Address_Line_1__c,ShipTo_Address_Line_2__c,'
                                                +'ShipTo_City__c,ShipTo_Country__c,ShipTo_state__c,ShipTo_Postal_Code__c,'
                                                +'Expediate__c,upgrade_point_release__c,'
                                                +'latest_available__c,network_environment__c,'
                                                +'Grant_To_Company__c,Severity__c,Customer_Tracking_Number__c, Owner.Name';
    
    public static final String RMA_LINEITEM_FIELDS = 'partnumber__c,serial_number__c,revision__c,Software_Release_Lookup__c,'
                                                    +'failure_code__c,Description__c,Software_Releases__c,PartNumber__r.Name,Service_Activity__c';
    
    public static final String CASECOMMENT_FIELDS = 'id,createddate,parentid,CommentBody,CreatedBy.Name';
    
    public static final String ATTACHMENT_FIELDS = 'Id,Name,ContentType,BodyLength,CreatedDate,Body,CreatedBy.Name';
    
    /* 
        Debug utility (if TRUE the errors are stacktraced)
    */
    public static Boolean DEBUG = true;
    public static String getStackTrace(Exception e){
        if(!DEBUG) { return ''; }
        return e.getStackTraceString().replace('\n',' ')+' ';
    }
    
    /*
        Interface used to provide basic serializing/deserializing functionalities 
    */
    public interface JsonObject{
        SObject toSObject();
        //isUpdate: can handle 2 states: update and insert
        String getRequired(Boolean isUpdate);
    }
    
    /*
        object used to parse the initial POST of a case (only the type is required to know which parser use)
    */
    public class BaseCaseFields{
        public String type{get;set;}
    }
    
    /*
        Parser for Software Request Case
    */
    public class SoftwareRequestCase implements JsonObject{
        public String caseNumber{get;set;}
        public String id{get;Set;}
        public String type{get;set;}
        public String accountName{get;Set;}
        public String createdDate{get;set;}
        public String lastModifiedDate{get;Set;}
        public String contactName{get;set;}
        public String productFamily{get;set;}
        public String currentPointRelease{get;set;}
        public String specifySoftwareVersion{get;set;}
        public String status{get;set;}
        public String city{get;set;}
        public String country{get;set;}
        public String state{get;set;}
        public String upgradePointRelease{get;set;}
        public String latestAvailable{get;set;}
        public String networkEnvironment{get;set;}
        public String grantAccessToAllUsers{get;set;}
        public String ownerName{get;set;}
        public String severity{get;set;}        
        public SoftwareRequestCase(){}
        public SoftwareRequestCase(Case c){
            id = c.Id;
            caseNumber = c.CaseNumber;
            createdDate = String.valueOf(c.CreatedDate);
            lastModifiedDate = String.valueOf(c.lastModifiedDate);
            type = c.RecordType.DeveloperName;
            accountName = c.Account.Name;
            contactName = c.Contact.Name;
            productFamily = c.Product_Family__c;
            currentPointRelease = c.current_point_release__c;
            specifySoftwareVersion = c.Other_Upg_to_Software_Release__c;
            status = c.status;
            city = c.ShipTo_City__c;
            country = c.ShipTo_Country__c;
            state = c.ShipTo_state__c;
            upgradePointRelease = c.upgrade_point_release__c;
            latestAvailable = String.valueOf(c.latest_available__c);
            networkEnvironment = c.network_environment__c;
            grantAccessToAllUsers =  String.valueOf(c.Grant_To_Company__c);
            ownerName = c.Owner.Name;
            severity = c.Severity__c;
            
        }
        public Case toSObject(){
            Case c = new Case();
            if(id != null) { c.Id = id;}
            if(productFamily != null) { c.Product_Family__c = productFamily; }
            if(currentPointRelease != null) { c.current_point_release__c = currentPointRelease; }
            if(specifySoftwareVersion != null) { c.Other_Upg_to_Software_Release__c = specifySoftwareVersion; }
            if(status != null) { c.status = status; }
            if(city != null) { c.ShipTo_City__c = city; }
            if(country != null) { c.ShipTo_Country__c = country; }
            if(state != null) { c.ShipTo_state__c = state; }
            if(upgradePointRelease != null) { c.upgrade_point_release__c = upgradePointRelease; }
            if(latestAvailable != null) { c.latest_available__c = Boolean.valueOf(latestAvailable); }
            if(networkEnvironment != null) { c.network_environment__c = networkEnvironment; }
            if(grantAccessToAllUsers != null) { c.Grant_To_Company__c = Boolean.valueOf(grantAccessToAllUsers); }
            return c;
        }
        public String getRequired(Boolean isUpdate){
            if(isUpdate){
                if(city != null) { return 'city cannot be updated'; }
                if(country != null) { return 'country cannot be updated'; }
                if(state != null) { return 'state cannot be updated'; }
            }
            if(String.isBlank(id)==false) { return 'id field cannot be updated'; }
            if(status != null) { return 'status cannot be updated'; }
            
            return null;
        }

    }
    
    /*
        Tech Support Case Parser
    */
    public class TechSupportCase implements JsonObject{
        public String caseNumber{get;set;}
        public String id{get;Set;}
        public String type{get;set;}
        public String accountName{get;Set;}
        public String createdDate{get;set;}
        public String lastModifiedDate{get;Set;}
        public String contactName{get;set;}
        public String urgency{get;set;}
        public String productFamily{get;set;}
        public String serviceActivity{get;set;}
        public String currentPointRelease{get;set;}
        public String specifySoftwareVersion{get;set;}
        public String problemSummary{get;set;}
        public String problemDescription{get;set;}
        public String resolutionSummary{get;set;}
        public String incidentCity{get;set;}
        public String incidentCountry{get;set;}
        public String reportedSeverity{get;set;}
        public String numberOfSubscribersAffected{get;set;}
        public String status{get;set;}
        public String advanceExchange{get;set;}
        public String customerTrackingNumber{get;set;}
        public String ownerName{get;set;}
        public String severity{get;set;}
        public TechSupportCase(){}
        public TechSupportCase(Case c){
            id = c.Id;
            caseNumber = c.CaseNumber;
            createdDate = String.valueOf(c.CreatedDate);
            lastModifiedDate = String.valueOf(c.lastModifiedDate);
            type = c.RecordTYpe.DeveloperName;
            accountName = c.Account.Name;
            contactName = c.Contact.Name;
            urgency = c.Urgency__c;
            productFamily = c.Product_Family__c;
            serviceActivity = c.Service_Activity__c;
            currentPointRelease = c.current_point_release__c;
            specifySoftwareVersion = c.Other_Upg_to_Software_Release__c;
            problemSummary = c.problem_summary__c;
            problemDescription = c.Description__c;
            resolutionSummary = c.resolution_summary__c;
            incidentCity = c.City__c;
            incidentCountry = c.Country__c;
            reportedSeverity = c.reported_severity__c;
            numberOfSubscribersAffected = c.of_Subscribers_Affected__c+'';
            status = c.status;
            customerTrackingNumber = c.Customer_Tracking_Number__c;
            advanceExchange = String.valueOf(c.Expediate__c);
            ownerName = c.Owner.Name;
            severity = c.Severity__c;
        }
        public Case toSObject(){
            Case c = new Case();
            if(id!=null) { c.Id = id; }
            if(urgency!=null) { c.Urgency__c = urgency; }
            if(productFamily!=null) { c.Product_Family__c = productFamily; }
            if(serviceActivity!=null) { c.Service_Activity__c = serviceActivity; }
            if(currentPointRelease!=null) { c.current_point_release__c = currentPointRelease; }
            if(specifySoftwareVersion!=null) { c.Other_Upg_to_Software_Release__c = specifySoftwareVersion; }
            if(problemSummary!=null) { c.problem_summary__c = problemSummary; }
            if(problemDescription!=null) { c.Description__c = problemDescription; }
            if(resolutionSummary!=null) { c.resolution_summary__c = resolutionSummary; }
            if(incidentCity!=null) { c.City__c = incidentCity; }
            if(incidentCountry!=null) { c.Country__c = incidentCountry; }
            if(reportedSeverity!=null) { c.reported_severity__c = reportedSeverity; }
            if(String.isBlank(numberOfSubscribersAffected)==false && numberOfSubscribersAffected != 'null') { 
                c.of_Subscribers_Affected__c = Double.valueOf(numberOfSubscribersAffected); 
            }
            if(status!=null) { c.status = status; }
            if(advanceExchange!=null) { c.Expediate__c = Boolean.valueOf(advanceExchange); }
            if(customerTrackingNumber!=null) { c.Customer_Tracking_Number__c = customerTrackingNumber; }            
            return c;
        }
        
        public String getRequired(Boolean isUpdate){
            if(isUpdate){
                if(urgency != null && String.isBlank(urgency)) { return 'urgency is required'; }
                if(productFamily != null && String.isBlank(productFamily)) { return 'productFamily is required'; }
                if(serviceActivity != null && String.isBlank(serviceActivity)) { return 'serviceActivity is required'; }
                if(problemSummary != null && String.isBlank(problemSummary)) { return 'problemSummary is required'; }
                if(incidentCity != null && String.isBlank(incidentCity)) { return 'incidentCity is required'; }
                if(incidentCountry != null && String.isBlank(incidentCountry)) { return 'incidentCountry is required'; }
            }else{
                if(String.isBlank(urgency)) { return 'urgency is required'; }
                if(String.isBlank(productFamily)) { return 'productFamily is required'; }
                if(String.isBlank(serviceActivity)) { return 'serviceActivity is required'; }
                if(String.isBlank(problemSummary)) { return 'problemSummary is required'; }
                if(String.isBlank(incidentCity)) { return 'incidentCity is required'; }
                if(String.isBlank(incidentCountry)) { return 'incidentCountry is required'; }
                //if(String.isBlank(customerTrackingNumber)) { return 'customerTrackingNumber is required'; }
            }
            
            if(resolutionSummary != null) { return 'resolutionSummary cannot be updated'; }
            if(status != null) { return 'status cannot be updated'; }
            if(String.isBlank(id)==false) { return 'id field cannot be updated'; }
            return null;
        }

    }
    
    /*
        RMA Case Parser
    */
    public class RMACase implements JsonObject{
        public String caseNumber{get;set;}
        public String id{get;Set;}
        public String type{get;set;}
        public String accountName{get;Set;}
        public String createdDate{get;set;}
        public String lastModifiedDate{get;Set;}
        public String contactName{get;set;}
        //public String serviceActivity{get;set;}
        public String status{get;set;}
        public String addressLine1{get;set;}
        public String addressLine2{get;set;}
        public String city{get;set;}
        public String country{get;set;}
        public String state{get;set;}
        public String postalCode{get;set;}
        public String advanceExchange{get;set;}
        public List<RMALineItem> items{get;set;}
        public String ownerName{get;set;}
        public String severity{get;set;}
                
        public RMACase(){}
        public RMACase(Case c){
            id = c.Id;
            caseNumber = c.CaseNumber;
            createdDate = String.valueOf(c.CreatedDate);
            lastModifiedDate = String.valueOf(c.lastModifiedDate);
            type = c.RecordTYpe.DeveloperName;
            accountName = c.Account.Name;
            contactName = c.Contact.Name;
            //serviceActivity = c.Service_Activity__c;
            status = c.status;
            addressLine1 = c.ShipTo_Address_Line_1__c;
            addressLine2 = c.ShipTo_Address_Line_2__c;
            city = c.ShipTo_City__c;
            country = c.ShipTo_Country__c;
            state = c.ShipTo_state__c;
            postalCode = c.ShipTo_Postal_Code__c;
            advanceExchange = String.valueOf(c.Expediate__c);
            items = new List<RMALineItem>();
            if(c.Request_Lines__r != null){
                for(RMA_Request_Line__c r : c.Request_Lines__r){
                    items.add(new RMALineItem(r));    
                }
            }
            ownerName = c.Owner.Name;
            severity = c.Severity__c;            
        }
        public Case toSObject(){
            Case c = new Case();
            if(id != null) { c.Id = id; }
            if(status != null) { c.status = status; }
            //if(serviceActivity != null) { c.Service_Activity__c = serviceActivity; }
            if(addressLine1 != null) { c.ShipTo_Address_Line_1__c = addressLine1; }
            if(addressLine2 != null) { c.ShipTo_Address_Line_2__c = addressLine2; }
            if(city != null) { c.ShipTo_City__c = city; }
            if(country != null) { c.ShipTo_Country__c = country; }
            if(state != null) { c.ShipTo_state__c = state; }
            if(postalCode != null) { c.ShipTo_Postal_Code__c = postalCode; }
            if(advanceExchange!=null) { c.Expediate__c = Boolean.valueOf(advanceExchange); }
            return c;
        }
        
        public String getRequired(Boolean isUpdate){
            if(isUpdate){
                //by requirement none can update RMA via APIs
                /*
                if(serviceActivity!=null && String.isBlank(serviceActivity)==false) return 'serviceActivity is required';    
                if(addressLine1 != null) return 'cannot update addressLine1';
                if(addressLine2 != null) return 'cannot update addressLine2';
                if(country != null) return 'cannot update country';
                if(state != null) return 'cannot update state';
                if(city != null) return 'cannot update city';
                if(postalCode != null) return 'cannot update postalCode';
                if(advanceExchange != null) return 'cannot update advanceExchange';
                */
            }else{
                //if(String.isBlank(serviceActivity)) { return 'serviceActivity is required'; }
                if(String.isBlank(id)==false) { return 'cannot use id when creating case'; }
            }
            
            if(String.isBlank(status)==false) { return 'status cannot be updated'; }
            if(this.items != null){
                for(RMALineItem r : this.items){
                    String req = r.getRequired(isUpdate);
                    if(!String.isBlank(req)) { return req; }
                }
            }
            return null;
        }

    }
    
    /*
        RMA Line Item Parser
    */
    public class RMALineItem implements JsonObject{
        public String id{get;set;}
        public String serialNumber{get;set;}
        public String revision{get;set;}
        public String softwareRelease{get;set;} // Name for display
        public String softwareReleaseId{get; set;} // ID Value of operation
        public String failureCode{get;set;}
        public String descriptionOfFailure{get;set;}
        public String partNumberId{get;set;}
        public String partNumberName{get;set;}
        public String serviceActivity{get;set;}
        public RMALineItem(){}
        public RMALineItem(RMA_Request_Line__c r){
            id = r.Id;
            serialNumber = r.serial_number__c;
            revision =  r.revision__c;
            softwareRelease = r.Software_Release_Lookup__c;
            softwareReleaseId = r.Software_Releases__c;
            failureCode = r.failure_code__c;
            descriptionOfFailure = r.Description__c;
            partNumberId = r.PartNumber__c;
            partNumberName = r.PartNumber__r.Name;
            serviceActivity = r.Service_Activity__c;
        }
        public RMA_Request_Line__c toSObject(){
            RMA_Request_Line__c r = new RMA_Request_Line__c();
            if(id != null) { r.Id = id; }
            if(serialNumber != null) { r.serial_number__c = serialNumber; }
            if(revision != null) { r.revision__c = revision; }
            if(failureCode != null) { r.failure_code__c = failureCode; }
            if(descriptionOfFailure != null) { r.Description__c = descriptionOfFailure; }
            if(partNumberId != null) { r.PartNumber__c = partNumberId; }
            if(serviceActivity != null) { r.Service_Activity__c = serviceActivity; }
            if(softwareReleaseId != null) { r.Software_Releases__c = softwareReleaseId; }
            return r;
        }
        public String getRequired(Boolean isUpdate){
            if(isUpdate) { return null; }
            if(String.isBlank(partNumberName)) { return 'partNumberName is required'; }
            if(String.isBlank(serialNumber)) { return 'serialNumber is required'; }
            if(String.isBlank(failureCode)) { return 'failureCode is required'; }
            if(String.isBlank(descriptionOfFailure)) { return 'descriptionOfFailure is required'; }
            if(String.isBlank(serviceActivity)) { return 'Service Activity is required'; }
            if(revision != null) { return 'revision is not updatable'; }
            return null;
        }
        
        
    }
    
    /*
        Case "summary" data (for the search action)
    */
    public class SummaryCase{
        
        public String id{get;Set;}
        public String caseNumber{get;Set;}
        public String summary{get;Set;}
        public String status{get;Set;}
        public String type{get;set;}
        public String createdDate{get;set;}
        public String lastModifiedDate{get;set;}
        public String severity{get;set;}
        public String ownerName{get;set;}
        //public SummaryCase(){}
        public SummaryCase(Case c){
            this.id = c.Id;
            this.summary = c.Problem_summary__c;
            this.status = c.Status;
            this.type = c.RecordType.DeveloperName;
            this.createdDate = String.valueOf(c.CreatedDate);
            this.caseNumber = c.CaseNumber;
            this.severity = c.Severity__c;
            this.ownerName = c.Owner.Name;
            this.lastModifiedDate = String.valueOf(c.lastModifiedDate);
        }
        
    }
    
    /*
        Comment parser 
    */
    public class Comment implements JsonObject{
        public String createdDate{get;set;}
        public String body{get;Set;}
        public String id{get;Set;}
        public String createdBy{get;Set;}
        public Comment(){}
        public Comment(CaseComment c){
            this.id = c.Id;
            this.body = c.CommentBody;
            this.createdDate = String.valueOf(c.CreatedDate);
            this.createdBy = c.CreatedBy.Name;
        }
        
        public CaseComment toSObject(){
            CaseComment cc = new CaseComment();
            cc.id = id;
            cc.CommentBody = body;
            return cc;
        }
        
        public String getRequired(Boolean isUpdate){
            if(String.isBlank(this.body)) { return 'Body is required'; }
            return null;
        }

    }
    
    /*
        Attachment parser 
    */
    public class FileAttachment implements JsonObject{
        public String createdDate{get;set;}
        public String body{get;Set;}
        public String id{get;Set;}
        public String name{get;Set;}
        public String contentType{get;Set;}
        public String bodyLength{get;Set;}
        public String createdBy{get;Set;}
        public FileAttachment(){}
        public FileAttachment(Attachment c){
            this.id = c.Id;
            if(c.Body != null) { this.body = Encodingutil.base64Encode(c.Body); }
            this.createdDate = String.valueOf(c.CreatedDate);
            this.name = c.Name;
            this.contentType = c.ContentType;
            this.bodyLength = String.valueOf(c.BodyLength);
            this.createdBy = c.CreatedBy.Name;
        }
        
        public Attachment toSObject(){
            Attachment a = new Attachment();
            a.id = id;
            if(body == null) { a.Body = null; }
            else a.Body = Encodingutil.base64Decode(body);
            a.Name = name;
            a.ContentType = contentType;
            return a;
        }
        
        public String getRequired(Boolean isUpdate){
            if(String.isBlank(this.name)) { return 'name is required'; }
            if(String.isBlank(this.body)) { return 'body is required'; }
            try{
                Blob b = EncodingUtil.base64Decode(this.body);
            }catch(Exception e){
                return 'Invalid body format';
            }
             if(String.isBlank(id)==false) { return 'id field cannot be updated'; }
            return null;
        }

    }

    /*
        User data parser 
    */
    public class PartnerUser implements JsonObject{
        public String id{get;Set;}
        public String profileName{get;Set;}
        public String isPartner{get;Set;}
		public String userName{get;set;}
		public String profileId{get;set;}
        
        public PartnerUser(User u){
            this.id = u.Id;
            this.userName = u.UserName;
            this.profileId = u.ProfileId;
            if(u.Profile.Name != CaseServicesParser.PROFILE_CALIX_PARTNER_USER &&
                u.Profile.Name != CaseServicesParser.PROFILE_CALIX_PARTNER_Manager){
                this.profileName = '';
                this.isPartner = 'false';
            }else{
            	this.profileName = u.Profile.Name;
            	this.isPartner = 'true';
            }
        }

        public User toSObject(){
            User u = new User();
            u.id = id;
            u.UserName = userName;
            u.ProfileId = profileId;
            return u;
        }

        public String getRequired(Boolean isUpdate){
            if(String.isBlank(this.userName)) { return 'userName is required'; }
            //if(String.isBlank(this.id)) { return 'id is required'; }
             //if(String.isBlank(id)==false) { return 'id field cannot be updated'; }
            return null;
        }

    }
     
    /*
        ID parser
    */
    public static ID parseId(String value, SObjectType otype){
        ID valId = null;
        try{
            valId = ID.valueOf(value);    
            if(valId.getSObjectType() != otype) { valId = null; }
        }catch(Exception e){ valId = null;}
        return valId;
    }
    
    /*
        Custom errors
    */
    public class RestError{
        public String message{get;set;}
        public String code{get;set;}
        public String details{get;set;}
        public RestError(String c, String m, String d){
                        system.debug('TONYP: '+c+m+d);
            this.message = m;
            this.code = c; 
            this.details = d;
        }
    }
    
    public class RestException extends Exception{ 
        public RestError restError{get;set;}
        public RestException(RestError err){
            this.restError = err;
        }
    }
    
    public class CustomException extends Exception{}
}