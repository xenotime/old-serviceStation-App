/**
 * Test class for Service Station Metadata Services REST API.
 *
 * @author A.I.
 * @version 1.0
 */
@isTest
private class SVST_AppMetadataServices_Test {

	static testMethod void test_get_metadata_rest() {
        
        Test.startTest();
        
        RestRequest request = new RestRequest();  
        request.httpMethod = 'GET';
        RestResponse response = new RestResponse();  
        RestContext.request = request;
        RestContext.response = response;
 
        request.requestURI = '/metadataServices';
        SVST_AppMetadataServices.getMetadata();
        System.assert(response.statusCode == 200,'Response with status '+response.statusCode+' and body "'+response.responseBody.toString()+'". Expected 200.');
           
        Test.stopTest();
    }

	static testMethod void test_get_metadata() {
        
        Test.startTest();
        
        SVST_AppMetadataServices.testMetadata();
        
        Test.stopTest();
    }
        
}