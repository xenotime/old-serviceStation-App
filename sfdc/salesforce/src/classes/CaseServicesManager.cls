/*
 * Copyright (C) 2014 TopCoder Inc., All Rights Reserved.
 */

/**
 * Apex Class that handles the logic behind the CaseServices REST APIs.
 * Almost each method handles a particular REST action.
 *
 * @author TCASSEMBLER
 * @version 1.0
 */
public class CaseServicesManager {   
    /*
        Query for a given case id
    */ 
    private static Case querySingleCase(String id){
        ID cid = CaseServicesParser.parseId(id, Case.getSObjectType()); 
        if(cid == null) { 
            throw new CaseServicesParser.RestException(new CaseServicesParser.RestError(CaseServices.E002, 
                                                                                                    CaseServices.E002_MESSAGE,
                                                                                                    id));
        }
         
        String query = 'Select '+CaseServicesParser.CASE_FIELDS
                                            +', (Select '+CaseServicesParser.RMA_LINEITEM_FIELDS
                                            +' From Request_Lines__r order by CreatedDate)'
                                            + ' From Case Where Id = \''+cid+'\'';
        System.debug('## Query on Case: '+query);
        List<Case> cList = Database.query(query); 
        if(cList.size()==0)  { 
            throw new CaseServicesParser.RestException(new CaseServicesParser.RestError(CaseServices.E003, 
                                                                                        CaseServices.E003_MESSAGE,
                                                                                        id)); 
        }
        
        return cList[0];
    }
     
    /*
        Query for a given attachment id
    */
    private static Attachment querySingleAttachment(String id){
        ID cid = CaseServicesParser.parseId(id, Attachment.getSObjectType());
        if(cid == null) { 
            throw new CaseServicesParser.RestException(new CaseServicesParser.RestError(CaseServices.E004, 
                                                                                            CaseServices.E004_MESSAGE,
                                                                                            id)); 
        }
         
        List<Attachment> cList = Database.query('Select '+CaseServicesParser.ATTACHMENT_FIELDS
                                                +' From Attachment Where Id = \''+cid+'\'');
        if(cList.size()==0) { 
            throw new CaseServicesParser.RestException(new CaseServicesParser.RestError(CaseServices.E005, 
                                                                                        CaseServices.E005_MESSAGE,
                                                                                        id)); 
        }
        
        return cList[0];
    }
    
    /*
        Manage query for a case by ID
    */
    public static CaseServicesParser.JsonObject getCase(String id){
        Case c = querySingleCase(id);
        
        CaseServicesParser.JsonObject rslt = null; 
        
        if(c.RecordType!= null && c.RecordType.DeveloperName == CaseServicesParser.CASE_RTYPE_TECHSUPPORT){
            rslt = new CaseServicesParser.TechSupportCase(c);
            
            //in case of not User/Manager profiles, hides these fields (requirement)
            User currentUser = [Select Id, Profile.Name, Username, ProfileId From User Where Id = :UserInfo.getUserId()];
            if(currentUser.Profile.Name != CaseServicesParser.PROFILE_CALIX_PARTNER_USER &&
                currentUser.Profile.Name != CaseServicesParser.PROFILE_CALIX_PARTNER_Manager){
                    ((CaseServicesParser.TechSupportCase)rslt).reportedSeverity = null;
                    ((CaseServicesParser.TechSupportCase)rslt).numberOfSubscribersAffected = null;
                }
        }else if(c.RecordType!= null && c.RecordType.DeveloperName == CaseServicesParser.CASE_RTYPE_RMA){
            rslt = new CaseServicesParser.RMACase(c);
        }else if(c.RecordType!= null && c.RecordType.DeveloperName == CaseServicesParser.CASE_RTYPE_RMASR){ // RMA Comment
            rslt = new CaseServicesParser.RMACase(c);            
        }else if(c.RecordType!= null && c.RecordType.DeveloperName == CaseServicesParser.CASE_RTYPE_SOFTWAREREQ){
            rslt = new CaseServicesParser.SoftwareRequestCase(c);
        }
        
        if(rslt==null) {
            throw new CaseServicesParser.RestException(new CaseServicesParser.RestError(CaseServices.E006, 
                                                                                        CaseServices.E006_MESSAGE,
                                                                                        id));
        }
        
        return rslt;
    }
    
    /*
        Query for Partner User type based on Partner Profile
     */
     public static CaseServicesParser.JsonObject getPartnerProfile(){
     	
     	CaseServicesParser.JsonObject rslt = null;
     	
        //in case of not User/Manager profiles, hides these fields (requirement)
        User currentUser = [Select Id, Username, ProfileId, Profile.Name From User Where Id = :UserInfo.getUserId()];
		rslt = new CaseServicesParser.PartnerUser(currentUser);
		
        if(rslt==null) {
            throw new CaseServicesParser.RestException(new CaseServicesParser.RestError(CaseServices.E000, 
                                                                                        CaseServices.E000_MESSAGE,
                                                                                        UserInfo.getUserId()));
        }
        
        return rslt;
     }
    
    /*
        Manage query for all comments for a given case id
    */
    public static List<CaseServicesParser.Comment> getComments(String id){
        Case c = querySingleCase(id);
        
        List<CaseComment> ccList = Database.query('Select '+ CaseServicesParser.CASECOMMENT_FIELDS
                                                    +' From CaseComment Where ParentId = \''
                                                    +c.Id+'\' order by CreatedDate DESC');
        List<CaseServicesParser.Comment> rslt = new List<CaseServicesParser.Comment>();
        for(CaseComment cc : ccList){
            rslt.add(new CaseServicesParser.Comment(cc));
        }
        return rslt;
    }
    
    /*
        Manage the creation of a comment for a given case id
    */
    public static CaseServicesParser.Comment postComments(String id, String reqBody){
        Case  callingCase = querySingleCase(id);
        
        CaseServicesParser.Comment parsed = null;
        try{
            parsed = (CaseServicesParser.Comment)JSON.deserialize(reqBody, CaseServicesParser.Comment.class);

        }catch(Exception e){
            throw new CaseServicesParser.RestException(new CaseServicesParser.RestError(CaseServices.E007, 
                                                                                        CaseServices.E007_MESSAGE, 
                                                                                        e.getMessage()));
        }
        
        String requiredMsg = parsed.getRequired(false);
        if(String.isBlank(requiredMsg) == false) { 
            throw new CaseServicesParser.RestException(new CaseServicesParser.RestError(CaseServices.E008, 
                                                                                        CaseServices.E008_MESSAGE,
                                                                                        'Validating fields: '+requiredMsg)); 
        }
        
        //creates the Case from the JSON object
        CaseComment c = (CaseComment)parsed.toSObject();
        c.ParentId = callingCase.Id;
        
        try{
            insert c;
        }catch(DMLException e){
            throw new CaseServicesParser.RestException(new CaseServicesParser.RestError(CaseServices.E008, 
                                                                                        CaseServices.E008_MESSAGE,
                                                                                        e.getDMLMessage(0)));
        }
        
        c = Database.query('Select '+ CaseServicesParser.CASECOMMENT_FIELDS
                            +' From CaseComment Where Id = \''+c.Id+'\'');
        return (new CaseServicesParser.Comment(c));
    }
    
    /*
        Manage query for the list of all attachments (no body sent)
    */
    public static List<CaseServicesParser.FileAttachment> getAttachments(String id){
        Case c = querySingleCase(id); 
        List<CaseServicesParser.FileAttachment> faList = new List<CaseServicesParser.FileAttachment>(); 
        for(Attachment a : Database.query('Select '+CaseServicesParser.ATTACHMENT_FIELDS
                                            +' From Attachment Where ParentId = \''
                                            +c.Id+'\' ORder by Name, CreatedDate DESC')){
            a.Body = null;    //the body isn't sent
            faList.add(new CaseServicesParser.FileAttachment(a));
        }
        return faList;
    }
    
    /*
        Manage the creation of an attachment for a given case id (body encoded in base64)
    */
    public static CaseServicesParser.FileAttachment postAttachment(String id, String reqBody){
        Case c = querySingleCase(id);
        //parse attachment object
        
        CaseServicesParser.FileAttachment parsed = (CaseServicesParser.FileAttachment)JSON.deserialize(reqBody, 
                                                                                        CaseServicesParser.FileAttachment.class);
        
        String requiredMsg = parsed.getRequired(false);
        if(String.isBlank(requiredMsg) == false) { 
            throw new CaseServicesParser.RestException(new CaseServicesParser.RestError(CaseServices.E008, 
                                                                                        CaseServices.E008_MESSAGE,
                                                                                        'Validating fields: '+requiredMsg)); 
        }
        
        Attachment a = parsed.toSObject();
        try{
            a.ParentId = c.Id;
            insert a;
        }catch(DMLException e){
            throw new CaseServicesParser.RestException(new CaseServicesParser.RestError(CaseServices.E008, 
                                                                                        CaseServices.E008_MESSAGE,
                                                                                        e.getDMLMessage(0)));
        }
        
        CaseServicesParser.FileAttachment fa = getAttachment(c.Id, a.Id);
        fa.body = null;
        return fa;
    }
    
    /*
        Manage the query of a given attachment id
    */
    public static CaseServicesParser.FileAttachment getAttachment(String cid, String aid){
        Case c = querySingleCase(cid);
        Attachment a = querySingleAttachment(aid);
        CaseServicesParser.FileAttachment fa = new CaseServicesParser.FileAttachment(a);
        return fa;
    }
    
    /*
       Handle Delete Attachment requires for single attachment id
     */
    public static void deleteAttachment(String aid){
    	Attachment a = querySingleAttachment(aid);
    	String uname = UserInfo.getUserName();
    	if (uname.equals(a.CreatedBy.Name)){
    		// Allow delete this attachment created by same user
    		delete a;
    	}else{
    		// User Did not match Delete not allowed throw REST error
            throw new CaseServicesParser.RestException(new CaseServicesParser.RestError(CaseServices.E012, 
                                                                                        CaseServices.E012_MESSAGE,
                                                                                        CaseServices.E009_MESSAGE));    		
    	}
    }
    
    
    /*
        Manage the close of the Tech Support case
    */
    public static CaseServicesParser.TechSupportCase closeCase(String cid){
        Case c = querySingleCase(cid);
        
        //recordtype not supported
        if(c.RecordType.DeveloperName != CaseServicesParser.CASE_RTYPE_TECHSUPPORT) {
            throw new CaseServicesParser.RestException(new CaseServicesParser.RestError(CaseServices.E006, 
                                                                                        CaseServices.E006_MESSAGE,
                                                                                        'Cannot close this case'));
        }
        
        if(c.Status == 'Closed') {
            throw new CaseServicesParser.RestException(new CaseServicesParser.RestError(CaseServices.E009, 
                                                                                        CaseServices.E009_MESSAGE,
                                                                                        'Case already closed'));
        }
        
        try{
            c.Status = 'Closed';
            update c;
        }catch(DMLException e){
            throw new CaseServicesParser.RestException(new CaseServicesParser.RestError(CaseServices.E008, 
                                                                                        CaseServices.E008_MESSAGE,
                                                                                        e.getDMLMessage(0)));
        }
        return new CaseServicesParser.TechSupportCase(c);
    } 
    
    /*
        Manage query on case with filters
    */
    public static List<CaseServicesParser.SummaryCase> searchCases(Map<String,String> params){
        String status = params.get('status');
        String caseType = params.get('type');
        String sortBy = params.get('sort');
        Boolean closedStatus = false;
        String offsetStr = params.get('offset');
        String limitStr = params.get('limit');
        System.debug('## Query Param offset: '+offsetStr+' limit: '+limitStr);
        
        //SORT SET
        Set<String> sortBySet = new Set<String>();
        if(String.isbLank(sortBy)==false){
            sortBySet.addAll(sortBy.split(','));
        }
        
        //STATUS SET
        Set<String> statusSet = new Set<String>();
        if(String.isbLank(status)==false){
            system.debug('TEST: Filter Statuses: '+status);
            statusSet.addAll(status.split(','));
        }
        List<String> actualStatus = new List<String>();
        for(String t : new List<String>(statusSet)){
            if(String.escapeSingleQuotes(t) != 'closed'){
                actualStatus.add(String.escapeSingleQuotes(t));
            }else{
                closedStatus = true;
            }
        }
        
        
        //CASE TYPE SET
        Set<String> typeSet = new Set<String>();
        if(String.isbLank(caseType)==false){
            typeSet.addAll(caseType.split(','));
        }
        List<String> actualCaseTypes = new List<String>();
        for(String t : new List<String>(typeSet)){
            if(t == 'tech') actualCaseTypes.add(CaseServicesParser.CASE_RTYPE_TECHSUPPORT);
            else if(t == 'sw') actualCaseTypes.add(CaseServicesParser.CASE_RTYPE_SOFTWAREREQ);
            else if(t == 'rma'){ 
                actualCaseTypes.add(CaseServicesParser.CASE_RTYPE_RMA);
                actualCaseTypes.add(CaseServicesParser.CASE_RTYPE_RMASR);
            }
            else throw new CaseServicesParser.RestException('Invalid case type filter: "'+t+'" not supported');
        }
        //search for all case types if none set
        if(actualCaseTypes.size()==0){
            actualCaseTypes.add(CaseServicesParser.CASE_RTYPE_TECHSUPPORT);
            actualCaseTypes.add(CaseServicesParser.CASE_RTYPE_SOFTWAREREQ);
            actualCaseTypes.add(CaseServicesParser.CASE_RTYPE_RMA);
            actualCaseTypes.add(CaseServicesParser.CASE_RTYPE_RMASR);
        }
        
        String query = 'Select '+CaseServicesParser.CASE_FIELDS+' From Case Where Id != null ';
        if(actualCaseTypes.size()>0) { 
            query += ' AND RecordType.DeveloperName IN(\''+ String.join(actualCaseTypes,'\',\'') +'\')'; 
        }
        
        /*
        if(actualStatus.size()>0) { query += ' AND Status IN(\''+ String.join(actualStatus,'\',\'') +'\')'; }
        else { query += ' AND Status != \'Closed\''; }
        */
        system.debug('TONY: status size: '+actualStatus.size());
        system.debug('TONY: closed status: '+closedStatus);
        if(actualStatus.size()>0){ 
            if(closedStatus != true){ 
                query += ' AND Status != \'Closed\''; 
            }
        }else if(closedStatus == true){
            query += ' AND Status = \'Closed\'';
        }
                
        query += ' ORDER BY ';
        
        List<String> sortingOptions = new List<String>();
        
        if(sortBySet.contains('dcn') || sortBySet.contains('cn')) { sortingOptions.add('CaseNumber DESC'); }
        else if(sortBySet.contains('acn')) { sortingOptions.add('CaseNumber ASC'); }
        
        if(sortBySet.contains('dcd')  || sortBySet.contains('cd')) { sortingOptions.add('CreatedDate DESC'); }
        else if(sortBySet.contains('acd')) { sortingOptions.add('CreatedDate ASC'); }
        
        if(sortBySet.contains('dlmd')  || sortBySet.contains('lmd')) { sortingOptions.add('LastModifiedDate DESC'); }
        else if(sortBySet.contains('almd')) { sortingOptions.add('LastModifiedDate ASC'); }
        
        if(sortBySet.contains('dse')  || sortBySet.contains('se')) { sortingOptions.add('Severity__c DESC'); }
        else if(sortBySet.contains('ase')) { sortingOptions.add('Severity__c ASC'); }
        
        if(sortBySet.contains('dst')  || sortBySet.contains('st')) { sortingOptions.add('Status DESC'); }
        else if(sortBySet.contains('ast')) { sortingOptions.add('Status ASC'); }
        
        if(sortingOptions.size()==0) { sortingOptions.add('CaseNumber DESC'); }
        
        query += String.join(sortingOptions,',');

        Integer lim = Calix_Settings__c.getValues('Default').Cases_Per_Page__c.intValue();
        if (String.isBlank(limitStr) == false) {
        	try {
        		lim = Integer.valueOf(limitStr);
        	} catch (System.TypeException e) {
        		throw new CaseServicesParser.RestException(new CaseServicesParser.RestError(CaseServices.E011,
        		                                                                            CaseServices.E011_MESSAGE,
        		                                                                            'Limit: ' + limitStr));
        	}

        	if (lim <= 0) {
        	   throw new CaseServicesParser.RestException(new CaseServicesParser.RestError(CaseServices.E011,
                                                                                           CaseServices.E011_MESSAGE,
                                                                                           'Limit: ' + limitStr));
        	}
        }   

        if (String.isBlank(offsetStr) == false) {
        	Integer offset = -1;
        	try {
                offset = Integer.valueOf(offsetStr);
            } catch (System.TypeException e) {
                throw new CaseServicesParser.RestException(new CaseServicesParser.RestError(CaseServices.E010,
                                                                                            CaseServices.E010_MESSAGE,
                                                                                            'Offset: ' + offsetStr));
            }

            if (offset < 0) {
                throw new CaseServicesParser.RestException(new CaseServicesParser.RestError(CaseServices.E010,
                                                                                            CaseServices.E010_MESSAGE,
                                                                                            'Offset: ' + offsetStr));
            }

            query += ' LIMIT ' + String.valueOf(lim);
            query += ' OFFSET ' + offsetStr;
        }else{
            query += ' LIMIT 40 ';
        	
        }
        System.debug('## Query: '+query);
        List<Case> cList = Database.query(query);
        List<CaseServicesParser.SummaryCase> rslt = new List<CaseServicesParser.SummaryCase>();
        for(Case c : cList){
            rslt.add(new CaseServicesParser.SummaryCase(c));
        }
        return rslt;
    }
    
    /*
        Manage the creation of a new case
    */
    public static CaseServicesParser.JsonObject postCase(String reqBody){
        
        //searches for case type inside the request
        String caseType = null;
        try{
            CaseServicesParser.BaseCaseFields bcf = (CaseServicesParser.BaseCaseFields)JSON.deserialize(reqBody, 
                                                                                        CaseServicesParser.BaseCaseFields.class);
            caseType = bcf.type;
        }catch(Exception e){
            throw new CaseServicesParser.RestException(new CaseServicesParser.RestError(CaseServices.E007, 
                                                                                        CaseServices.E007_MESSAGE, 
                                                                                        e.getMessage()));
        }
        
        if(String.isBlank(caseType)){
            throw new CaseServicesParser.RestException(new CaseServicesParser.RestError(CaseServices.E007, 
                                                                                        CaseServices.E007_MESSAGE, 
                                                                                        'No case type set'));
        }
        
        CaseServicesParser.JsonObject parsed = null;
        
        //tries to get Account and Contract from current user
        User currentUser = [Select AccountId, ContactId, Contact.MailingStreet,
                            Contact.MailingCity,Contact.MailingCountry, Contact.MailingState,
                            Contact.MailingPostalCode
                             From User Where Id = :UserInfo.getUserId()];
        
        //try to parse the request bases upon the case type
        Case c = null;
        try{
            
            if(caseType == CaseServicesParser.CASE_RTYPE_TECHSUPPORT){
                parsed = (CaseServicesParser.TechSupportCase)JSON.deserialize(reqBody, 
                                                                CaseServicesParser.TechSupportCase.class);
                
            }else if(caseType == CaseServicesParser.CASE_RTYPE_RMA){
                parsed = (CaseServicesParser.RMACase)JSON.deserialize(reqBody, CaseServicesParser.RMACase.class);
                
                //copies address from User.Contact if null
                CaseServicesParser.RMACase tc = (CaseServicesParser.RMACase)parsed;
                if(String.isBlank(tc.city)) tc.city = currentUser.Contact.MailingCity;
                if(String.isBlank(tc.country)) tc.country = currentUser.Contact.MailingCountry;
                if(String.isBlank(tc.postalCode)) tc.postalCode = currentUser.Contact.MailingPostalCode;
                if(String.isBlank(tc.state)) tc.state = currentUser.Contact.MailingState;
                if(String.isBlank(tc.addressLine1)) tc.addressLine1 = currentUser.Contact.MailingStreet;
                
            }else if(caseType == CaseServicesParser.CASE_RTYPE_SOFTWAREREQ){
                parsed = (CaseServicesParser.SoftwareRequestCase)JSON.deserialize(reqBody, CaseServicesParser.SoftwareRequestCase.class);
                
                //copies address from User.Contact if null
                CaseServicesParser.SoftwareRequestCase sc = (CaseServicesParser.SoftwareRequestCase)parsed;
                if(String.isBlank(sc.city)) sc.city = currentUser.Contact.MailingCity;
                if(String.isBlank(sc.country)) sc.country = currentUser.Contact.MailingCountry;
                if(String.isBlank(sc.state)) sc.state = currentUser.Contact.MailingState;
            }

        }catch(Exception e){
            throw new CaseServicesParser.RestException(new CaseServicesParser.RestError(CaseServices.E007, 
                                                                                        CaseServices.E007_MESSAGE, 
                                                                                        e.getMessage()));
        }
        
        if(parsed==null) { 
            throw new CaseServicesParser.RestException(new CaseServicesParser.RestError(CaseServices.E006, 
                                                                                            CaseServices.E006_MESSAGE,'')); 
        }
        
        String requiredMsg = parsed.getRequired(false);
        if(String.isBlank(requiredMsg) == false) { 
            throw new CaseServicesParser.RestException(new CaseServicesParser.RestError(CaseServices.E008, 
                                                                                        CaseServices.E008_MESSAGE,
                                                                                        'Validating fields: '
                                                                                        +requiredMsg)); 
        }
        
        //creates the Case from the JSON object
        c = (Case)parsed.toSObject();
        //assigns the recordtype ID
        c.RecordTypeId = [Select Id From RecordType Where DeveloperName = :caseType and SObjectType='Case'].Id;
        c.AccountId = currentUser.AccountId;
        c.ContactId = currentUser.ContactId;
        
        //in case of RMA created the line items objects
        List<RMA_Request_Line__c> rmaLineItems = new List<RMA_Request_Line__c>();
        if(caseType == CaseServicesParser.CASE_RTYPE_RMA){
            //gets all the products by "name"
            Map<String,ID> products = new Map<String,ID>();
            for(CaseServicesParser.RMALineItem r : ((CaseServicesParser.RMACase)parsed).items){
                if(String.isBlank(r.partNumberName)==false){
                    products.put(r.partNumberName,null);
                }
            }
            //queries product by names
            List<Product2> pList = [Select Id, Name From Product2 Where Name IN :products.keySet()];
            for(Product2 p : pList){
                products.put(p.Name,p.Id);
            }
            for(CaseServicesParser.RMALineItem r : ((CaseServicesParser.RMACase)parsed).items){
                //sets the product part name
                r.partNumberId = products.get(r.partNumberName);
                if(String.isBlank(r.partNumberId)){
                    throw new CaseServicesParser.RestException(new CaseServicesParser.RestError(CaseServices.E008, 
                                                                                        CaseServices.E008_MESSAGE,
                                                                                        'Part number not found:'
                                                                                        +r.partNumberName));
                }
                rmaLineItems.add((RMA_Request_Line__c)r.toSObject());
                
            }
        }
        
        Savepoint sp = Database.setSavePoint();
        Id myCaseId;
        try{
            /*
            RecordType rt = [select Id, Name from RecordType where Name = 'RMA SR' AND SobjectType = 'Case' LIMIT 1];
            User usr = [Select Id, Email, Name from User where Name = 'Integration User' limit 1];
             c.OwnerId =  usr.id;
             c.RecordTypeId = rt.id;
  
             c.Status = 'Work in progress';
             c.Sub_Status__c ='Ready To Sync';
            */

			// set Mobile case flag for all new created cases
			c.Cases_Create_through_Mobile__c = TRUE;

            insert c;
            
            if(caseType == CaseServicesParser.CASE_RTYPE_RMA){
                if(rmaLineItems.size()>0){
                    for(RMA_Request_Line__c r : rmaLineItems){
                        r.Calix_Service_Request_Number__c = c.Id;
                    }
                    insert rmaLineItems;
                    
                    //CaseUtil.submitCase(c);
                }
                //S-183805
                CM_RMASubmitRequestButton.CaseFutureUpdate(c.Id);
            }
            
        }catch(DMLException e){
            Database.rollback(sp);
            system.debug('TONYP: '+CaseServices.E008+CaseServices.E008_MESSAGE+e.getDMLMessage(0));        
            throw new CaseServicesParser.RestException(new CaseServicesParser.RestError(CaseServices.E008, 
                                                                                        CaseServices.E008_MESSAGE,
                                                                                        e.getDMLMessage(0)));
        }
        
        return getCase(c.Id);
    }
    
    /*
        Manage the update of a given case id
    */
    public static CaseServicesParser.JsonObject updateCase(String id, String reqBody){
        Case callingCase = querySingleCase(id);
        String caseType = callingCase.RecordType.DeveloperName;
        
        CaseServicesParser.JsonObject parsed = null;
        
        String excDetails = '';
        try{
            
            if(caseType == CaseServicesParser.CASE_RTYPE_TECHSUPPORT){
                parsed = (CaseServicesParser.TechSupportCase)JSON.deserialize(reqBody, CaseServicesParser.TechSupportCase.class);
            }else if(caseType == CaseServicesParser.CASE_RTYPE_RMA){
                excDetails = 'RMA cases are not editable';
            }else if(caseType == CaseServicesParser.CASE_RTYPE_SOFTWAREREQ){
                parsed = (CaseServicesParser.SoftwareRequestCase)JSON.deserialize(reqBody, CaseServicesParser.SoftwareRequestCase.class);
            }
        }catch(Exception e){
            throw new CaseServicesParser.RestException(new CaseServicesParser.RestError(CaseServices.E007, 
                                                                                        CaseServices.E007_MESSAGE, 
                                                                                        e.getMessage()));
        }
        
        if(parsed==null) { throw new CaseServicesParser.RestException(new CaseServicesParser.RestError(CaseServices.E006, 
                                                                                                        CaseServices.E006_MESSAGE,
                                                                                                        excDetails)); }
        
        String requiredMsg = parsed.getRequired(true);
        if(String.isBlank(requiredMsg) == false) { 
            throw new CaseServicesParser.RestException(new CaseServicesParser.RestError(CaseServices.E008, 
                                                                                        CaseServices.E008_MESSAGE,
                                                                                        'Validating fields: '+requiredMsg)); 
        }
        
        //creates the Case from the JSON object
        Case c = (Case)parsed.toSObject();
        c.Id = callingCase.Id;
        
        try{ 
            update c;
        }catch(DMLException e){
            throw new CaseServicesParser.RestException(new CaseServicesParser.RestError(CaseServices.E008, 
                                                                                        CaseServices.E008_MESSAGE,
                                                                                        e.getDMLMessage(0)));
        }
        
        return getCase(c.Id);
    }
    

}