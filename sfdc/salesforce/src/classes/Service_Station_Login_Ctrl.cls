public with sharing class Service_Station_Login_Ctrl {

    public String authProviderURL { get; set; }
    public String username {get; set;}
    public String password {get; set;}
    public String siteURL  {get; set;}
    public String startURL {get; set;}
 
    public Boolean passOKShow {get; set;}
    public Boolean passFailedShow {get; set;}
    
    public Service_Station_Login_Ctrl () {
        // Auth Provider URL hard coded here for clarity; better to load from a custom setting! this is used only for external provider like Facebook example
        authProviderURL = 'https://login.salesforce.com/services/auth/sso/00Dd0000000e1KYEAY/FacebookProvider';
        siteURL         = Site.getBaseUrl(); // replaced as of 29.0 deprecated getCurrentSiteUrl();
        startURL        = System.currentPageReference().getParameters().get('startURL');
        passOKShow = true;
        passFailedShow = false;
    }
    
    public PageReference login() {
        PageReference pr = Site.login(username, password, startUrl); 
        if (pr != null){
            passOKShow = true;
            passFailedShow = false;
        }else{
            passOKShow = false;
            passFailedShow = true;
        }
        return pr;
    }
}