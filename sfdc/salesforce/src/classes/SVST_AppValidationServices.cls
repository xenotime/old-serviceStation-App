/**
 * SVST_AppValidationServices REST APIs class. Handles GET requests for validation.
 *
 * @author A.I
 * @version 1.0
 */
@RestResource(urlMapping='/validationServices/*') 
global class SVST_AppValidationServices {

    public static final String V000 = 'V000';
    public static final String V001 = 'V001';

	/* Validation messages */
    public static final String PART_VALIDATION_MSG_OK = 'Part number found';
    public static final String PART_VALIDATION_MSG_ERROR = 'Part number not found';

    @HttpGet
    global static void getAction(){
        RestRequest req = RestContext.request;
        RestResponse res = RestContext.response;
                
        try{
        	String part = req.params.get('partid');
        	ValidationResult vr = partValidation(part);
			CaseServices.returnResponse(res, vr, 200);
			        	
        }catch(Exception e){
            CaseServices.returnResponse(res, new CaseServicesParser.RestError(CaseServices.E000, CaseServices.E000_MESSAGE, 
                                                                    CaseServicesParser.getStackTrace(e)
                                                                    +e.getMessage()),400);
        }            	

    }

	// Validate if part number exists
	public static ValidationResult partValidation(String part_number) {
		ValidationResult err = new ValidationResult(V000,'NOOP');
		
		List<Product__c> prodLst = [Select Name, Id From Product__c where Name =: part_number]; 
		if (prodLst != null && prodLst.size() > 0){
			err.message = PART_VALIDATION_MSG_OK;
		}else{
			err.code = V001;
			err.message = PART_VALIDATION_MSG_ERROR;
		}
		return err;
	}
    
    /*
        Interface used to provide basic serializing/deserializing functionalities 
    */
    public interface JsonObject{
        SObject toSObject();
    }
    
    /*
        Custom errors
    */
    public class ValidationResult {
        public String message{get;set;}
        public String code{get;set;}
        public ValidationResult(String c, String m){
            this.message = m;
            this.code = c; 
        }        
    }

}