/**
 * Helper Test class for Case Services REST API.
 *
 * @author A.I
 * @version 1.0
 */
@isTest (seeAllData=true)
private class CaseServicesEX_Test {

    private static Product2 createRMAProduct(){
        Product2 r = new Product2 (Name = '000-000-000', RMA_Form_Viewable__c = 'Yes');
        insert r;
        return r;
    }

    static testMethod void test_post_new_case_pt3() {
        //creates an RMA product
        Product2 partNumber = createRMAProduct();
        
        //POST RMA REQUEST
        Test.startTest();
        RestRequest request = new RestRequest();  
        request.httpMethod = 'POST';
        RestResponse response = new RestResponse();  
        RestContext.request = request;
        RestContext.response = response;
        
        request.requestURI = '/caseServices';
        
        //creating tech support case without setting the recordtype
        CaseServicesParser.RMACase reqCase = new CaseServicesParser.RMACase();
        request.requestBody = Blob.valueOf(JSON.serialize(reqCase));
        CaseServices.postAction();
        System.assert(response.statusCode == 400,'Response with status '+response.statusCode+' and body "'+response.responseBody.toString()+'". Expected 400.');
        
        //creating rma case without required fields
        reqCase = new CaseServicesParser.RMACase();
        reqCase.type = CaseServicesParser.CASE_RTYPE_RMA;
        request.requestBody = Blob.valueOf(JSON.serialize(reqCase));
        CaseServices.postAction();
        System.assert(response.statusCode == 400,'Response with status '+response.statusCode+' and body "'+response.responseBody.toString()+'". Expected 400.');
        
        //creating a valid ticket but rma line items need required fields

        CaseServicesParser.RMALineItem rlitem = new CaseServicesParser.RMALineItem();
        reqCase.items = new List<CaseServicesParser.RMALineItem>();
        reqCase.items.add(rlitem);
        request.requestBody = Blob.valueOf(JSON.serialize(reqCase));
        CaseServices.postAction();
        System.assert(response.statusCode == 400,'Response with status '+response.statusCode+' and body "'+response.responseBody.toString()+'". Expected 400.');
        
        
        reqCase.items[0].partNumberName = partNumber.Name;
        reqCase.items[0].serviceActivity = 'ACTIVITY';
        request.requestBody = Blob.valueOf(JSON.serialize(reqCase));
        CaseServices.postAction();
        System.assert(response.statusCode == 400,'Response with status '+response.statusCode+' and body "'+response.responseBody.toString()+'". Expected 400.');
        
        reqCase.items[0].serialNumber = '1234567890'; //'SERIAL NUMBER';
        request.requestBody = Blob.valueOf(JSON.serialize(reqCase));
        CaseServices.postAction();
        System.assert(response.statusCode == 400,'Response with status '+response.statusCode+' and body "'+response.responseBody.toString()+'". Expected 400.');
        
        reqCase.items[0].failureCode = 'CODE';
        request.requestBody = Blob.valueOf(JSON.serialize(reqCase));
        CaseServices.postAction();
        System.assert(response.statusCode == 400,'Response with status '+response.statusCode+' and body "'+response.responseBody.toString()+'". Expected 400.');
        
        //creating a valid ticket (rma line items are populated as required)
        reqCase.items[0].descriptionOfFailure = 'DESCRIPTION';
        request.requestBody = Blob.valueOf(JSON.serialize(reqCase));
        CaseServices.postAction();
        System.assert(response.statusCode == 200,'Response with status '+response.statusCode+' and body "'+response.responseBody.toString()+'". Expected 200.');
        
        CaseServicesParser.RMACase rslt = null;
        try{
               rslt = (CaseServicesParser.RMACase)JSON.deserialize(response.responseBody.toString(), CaseServicesParser.RMACase.class);
           }catch(Exception e){System.assert(false,'Unable to parse: '+response.responseBody.toString()+'. Cause: '+e.getMessage());}
           System.assert(rslt != null, 'Null result');
           System.assert(String.isBlank(rslt.id)==false,'Null id: '+rslt);
           System.assert([Select count() From Case Where Id = :rslt.id and RecordType.DeveloperName = :CaseServicesParser.CASE_RTYPE_RMA] == 1, 
                                       'No case found for recordtype '+CaseServicesParser.CASE_RTYPE_RMA+': '+rslt);
           System.assert([Select count() From RMA_Request_Line__c Where Calix_Service_Request_Number__c = :rslt.id ] == 1, 
                                       'No RMA line item found for recordtype '+CaseServicesParser.CASE_RTYPE_RMA+': '+rslt);
        
        Test.stopTest();
    }

}