//Owner: Murali
// used in Submit Request button on RMA service request
// this class is used to assign the case to queue member and send an email to queue from case object 


global without sharing Class CM_RMASubmitRequestButton{

webservice static String findRMALinesExist(String CaseId) {
    List<RMA_Request_Line__c> foundRecs=[Select id from RMA_Request_Line__c where Calix_Service_Request_Number__c = :CaseId];
    if(foundRecs!=null && foundRecs.size()>0){
        return '1';
    }else{
        return '0';
    }
}

@future
public static void CaseFutureUpdate(String CaseId) {
    String result = CaseOwnerUpdate(CaseId);
}

webservice static String CaseOwnerUpdate(String CaseId) {
 String result = '';
 String cId = null;
 
 if(caseId != null && caseId.length()>0)                 
  cId = caseId.trim();
 
 RecordType rt = [select Id, Name from RecordType where Name = 'RMA SR' AND SobjectType = 'Case' LIMIT 1]; 
 // query case      
 Case caseRec = [Select Id, RecordTypeId, OwnerId, ContactId, AccountId From Case Where Id=:cId];
 User usr = [Select Id, Email, Name from User where Name = 'Integration User' limit 1];
 QueueSobject que = [Select Id, queueId, Queue.Name, Queue.Email from QueueSobject where SobjectType = 'Case' AND Queue.Name = 'Service Admin' limit 1];
 system.debug('-----------que:'+que);
 caseRec.OwnerId =  usr.id;
// caseRec.OwnerId =  que.queueId;
 system.debug('---------queue.email:'+ que.queue.email);    
 caseRec.RecordTypeId = rt.id;
 caseRec.Status = 'Work in progress';
 caseRec.Sub_Status__c ='Ready To Sync';
 
 system.debug('TEST: '+usr.id+' : '+rt.id+' : '+caseRec.Status+' : '+caseRec.Sub_Status__c);
 Update caseRec;
 
 /*
 Messaging.SingleEmailMessage[] email = new Messaging.SingleEmailMessage[] {};
     
            //Email send to Case created User.
            Messaging.SingleEmailMessage emailqueue = new Messaging.SingleEmailMessage();       
            String[] toAddresses = new String[] {};
            toAddresses.add(usr.Email);
            EmailTemplate et = [SELECT id FROM EmailTemplate WHERE developerName = 'CaseQueueAssignmentRules'];
            emailqueue.setTargetObjectId(caseRec.ContactId); // Specify who the email should be sent to.
            emailqueue.setToAddresses(toAddresses);                                        
            emailqueue.SaveAsActivity= false; 
            emailqueue.setTemplateId(et.id); 
            emailqueue.setWhatId(caseRec.Id);  
            email.add(emailqueue);
            system.debug('--------toAddresses:'+toAddresses);
            
            if(!email.isEmpty()){
                 Messaging.sendEmail(email);
                 system.debug('--------SendEmail:'+Email); 
            }
   */
        
   return 'result';
  }
}