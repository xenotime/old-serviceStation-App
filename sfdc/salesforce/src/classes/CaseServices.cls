/*
 * Copyright (C) 2014 TopCoder Inc., All Rights Reserved.
 */

/**
 * CaseServices REST APIs class. Handles GET, POST and PUT requests.
 *
 * Version 2.0 changes: added error codes E010 and E011 and their corresponding
 *                      messages to handle pagination errors.
 *
 * @author TCASSEMBLER
 * @version 2.0
 */
@RestResource(urlMapping='/caseServices/*')
global with sharing class CaseServices {
 
    public static final String E000 = 'E000';
    public static final String E000_MESSAGE = 'Internal error';
    public static final String E001 = 'E001';
    public static final String E001_MESSAGE = 'Invalid action';
    public static final String E002 = 'E002';
    public static final String E002_MESSAGE = 'Invalid case id';
    public static final String E003 = 'E003';
    public static final String E003_MESSAGE = 'Case not found';
    public static final String E004 = 'E004';
    public static final String E004_MESSAGE = 'Invalid attachment id';
    public static final String E005 = 'E005';
    public static final String E005_MESSAGE = 'Attachment not found';
    public static final String E006 = 'E006';
    public static final String E006_MESSAGE = 'Type of case not supported';
    public static final String E007 = 'E007';
    public static final String E007_MESSAGE = 'Invalid request body';
    public static final String E008 = 'E008';
    public static final String E008_MESSAGE = 'Case validation error';
    public static final String E009 = 'E009';
    public static final String E009_MESSAGE = 'Unable to perform action';
    // Invalid offset parameter.
    public static final String E010 = 'E010';
    public static final String E010_MESSAGE = 'Invalid offset parameter. Should be non-negative integer.';
    // Invalid limit parameter.
    public static final String E011 = 'E011';
    public static final String E011_MESSAGE = 'Invalid limit parameter. Should be positive integer.';
	// Atachmet Delete Validation    
    public static final String E012 = 'E012';
    public static final String E012_MESSAGE = 'Unable to dlete Attachment. User No this record owner.';
    
    private static final String REGEX_ID = '([0-9a-zA-Z]{15,15}([0-9A-Z]{3,3})*)';
    private static final String REGEX_COMMENTS = REGEX_ID+'/comments';
    private static final String REGEX_ATTACHMENTS = REGEX_ID+'/attachments';
    private static final String REGEX_CLOSE = REGEX_ID+'/close';
    private static final String REGEX_PARTNER_USER = '/partner';
    
    //Action: /{id}
    private static final Pattern PTN_CASEID = Pattern.compile('^'+REGEX_ID+'$');
    //Action: /{id}/comments
    private static final Pattern PTN_COMMENTS = Pattern.compile('^'+REGEX_COMMENTS+'$');
    //Action: /{id}/attachments
    private static final Pattern PTN_ATTACHMENTS = Pattern.compile('^'+REGEX_ATTACHMENTS+'$');
    //Action: /{id}/attachments/{id}
    private static final Pattern PTN_ATTACHMENTID = Pattern.compile('^'+REGEX_ATTACHMENTS+'/'+REGEX_ID+'$');
    //Action: /{id}/close
    private static final Pattern PTN_CASECLOSE = Pattern.compile('^'+REGEX_CLOSE+'$');
    //Action: /{id}/partner
    private static final Pattern PTN_USER = Pattern.compile('^'+REGEX_PARTNER_USER+'$');
    
    @HttpGet
    global static void getAction(){
        handleRequest();
    }
    @HttpPost
    global static void postAction(){
        handleRequest();
    }
    @HttpPut
    global static void putAction(){
        handleRequest();
    }
    
    
    private static void handleRequest(){
        RestRequest req = RestContext.request;
        RestResponse res = RestContext.response;
        String method = req.httpMethod;
        String requestURI = req.requestURI;
        
        //polish request URI from base service
        requestURI = requestURI.replace('/caseServices/','').replace('/caseServices','').trim();
        
        try{
            
        
            if(String.isBlank(requestURI) ){
                if(method == 'GET'){
                     
                    //get filtered list of cases
                    List<CaseServicesParser.SummaryCase> caseList = CaseServicesManager.searchCases(req.params);
                    returnResponse(res, caseList, 200);
                    
                    //String s = JSON.serialize(caseList);
                    //System.debug('### JSON: '+s);
                    return;
                    
                }else if(method == 'POST'){
                    //post case
                    Object cs = CaseServicesManager.postCase(req.requestBody.toString()); 
                    returnResponse(res, cs, 200);
                    return;
                }
                
            }
            
            Matcher match = PTN_CASEID.matcher(requestURI);
            if(match.matches()){
                if(method == 'GET'){
                    //get case by ID
                    Object cs = CaseServicesManager.getCase(match.group(1)); 
                    returnResponse(res, cs, 200);
                    return;
                }else if(method == 'PUT'){
                    //update case
                    Object cs = CaseServicesManager.updateCase(match.group(1), req.requestBody.toString()); 
                    returnResponse(res, cs, 200);
                    return;
                }  
            }

            if(requestURI.contains('/partner')){	
                if(method == 'GET'){
                    //get partner user
                    Object usrList = CaseServicesManager.getPartnerProfile(); 
                    returnResponse(res, usrList, 200);
                    return;
                }
                return;
            }
            
            match = PTN_COMMENTS.matcher(requestURI);
            if(match.matches()){
                if(method == 'GET'){
                    //get comments
                    Object cmtList = CaseServicesManager.getComments(match.group(1)); 
                    returnResponse(res, cmtList, 200);
                    return;
                }else if(method == 'POST'){
                    //post comment
                    Object cmt = CaseServicesManager.postComments(match.group(1),req.requestBody.toString());
                    returnResponse(res, cmt, 200);
                    return;
                }
            }
            
            match = PTN_ATTACHMENTS.matcher(requestURI);
            if(match.matches()){
                if(method == 'GET'){
                    //get attachments
                    Object attList = CaseServicesManager.getAttachments(match.group(1));
                    returnResponse(res, attList, 200); 
                    return;
                }else if(method == 'POST'){
                    Object att = CaseServicesManager.postAttachment(match.group(1), req.requestBody.toString());
                    returnResponse(res, att, 200);
                    return;
                }else if(method == 'DELETE'){
                	Attachment att = new Attachment(); // Send empty object
                	CaseServicesManager.deleteAttachment(match.group(1));
                    returnResponse(res, att, 200);
                    return;
                }
            }
            
            match = PTN_ATTACHMENTID.matcher(requestURI);
            if(match.matches()){
                if(method == 'GET'){
                    //get attachment
                    Object att = CaseServicesManager.getAttachment(match.group(1),match.group(3));
                    returnResponse(res, att, 200); 
                    return;
                }
            }
            
            match = PTN_CASECLOSE.matcher(requestURI);
            if(match.matches()){
                if(method == 'PUT'){
                    //close case
                    Object cs = CaseServicesManager.closeCase(match.group(1)); 
                    returnResponse(res, cs, 200);
                    return;
                } 
            } 
            
            //no action available
            returnResponse(res, new CaseServicesParser.RestError(E001, E001_MESSAGE, requestURI),400);
        }catch(CaseServicesParser.RestException exc){
            returnResponse(res, exc.restError,400);
        }catch(Exception e){
            returnResponse(res, new CaseServicesParser.RestError(E000, E000_MESSAGE, 
                                                                    CaseServicesParser.getStackTrace(e)
                                                                    +e.getMessage()),400);
        }
    } 
    
    public static void returnResponse(RestResponse res, Object body, Integer status){
        res.statusCode = status;
        res.headers.put('content-type','application/json');
        res.responseBody = Blob.valueOf(JSON.serialize(body));
    }
    
    
}