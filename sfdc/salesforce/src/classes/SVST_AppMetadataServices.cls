/**
 * SVST_AppMetadataServices REST APIs class. Handles GET requests for metadata mobile app initialization.
 *
 * @author A.I
 * @version 1.0
 */
@RestResource(urlMapping='/metadataServices/*') 
global class SVST_AppMetadataServices {
    public static final String E000 = 'E000';
    public static final String E000_MESSAGE = 'Internal error';

	@HttpGet
    global static void getMetadata(){
        handleMetadataRequest();
    }
    
    // This methods temporray for testing only relomve for prod
    public static void testMetadata() {
    	AppMetadata met = getAppMetadata();
    	System.debug('JSON: '+JSON.serializePretty(met));
    }
    
    private static void handleMetadataRequest(){
     	
        RestRequest req = RestContext.request;
        RestResponse res = RestContext.response;
        //String method = req.httpMethod;
        //String requestURI = req.requestURI;
                
        try{
        	
        	AppMetadata met = getAppMetadata();
			returnResponse(res, met, 200);
			        	
        }catch(Exception e){
            returnResponse(res, new CaseServicesParser.RestError(E000, E000_MESSAGE, 
                                                                    CaseServicesParser.getStackTrace(e)
                                                                    +e.getMessage()),400);
        }            	
    }
    
    private static AppMetadata getAppMetadata(){

        	AppMetadata met = new AppMetadata();
        	// User Data
        	met.userInfo = getUserData();
        	
			// Metadata Picklist derived
			met.reportedSeverityList = CaseServicesUtil.getReportedSeverityData();
			met.urgencyList = CaseServicesUtil.getUrgencyData();
			met.serviceActivityList = CaseServicesUtil.getServiceActivityData();
			met.networkEnvironmentList = CaseServicesUtil.getNetworkEnvironmentData();
			met.failureCodeList = CaseServicesUtil.getFailureCodeData();
			met.productFamilyList = CaseServicesUtil.getProductFamilyData();
			met.productCurrentPointReleaseList = CaseServicesUtil.getProductCurrentPointReleaseData();
			met.productUpgradePointReleaseList = CaseServicesUtil.getProductUpgradePointReleaseData();
			
        	// Statis data
        	met.rmaServiceActivityList = CaseServicesUtil.getRMAServiceActivityData();
        	met.rmaFailureCodeList = CaseServicesUtil.getRMAFailureCodeData();
        	met.codePrefixList = CaseServicesUtil.getCodePrefixData();
			
			met.softwareReleaseList = CaseServicesUtil.getProductSoftwareReleaseList();
			
			return met;    	
    } 
    
    public static void returnResponse(RestResponse res, Object body, Integer status){
        res.statusCode = status;
        res.headers.put('content-type','application/json');
        res.responseBody = Blob.valueOf(JSON.serialize(body));
    }
    
    public static UserData getUserData(){
    	String result = UserInfo.getUserId();
        User usr = [Select Id, Name, Contact.Name, Contact.MailingStreet, Contact.MailingCity, Contact.MailingState, Contact.MailingPostalCode, Contact.MailingCountry from User WHERE Id =: result];
    	//User usr = [SELECT Id,Name,Street,City,State,PostalCode,Country,Email,Phone,ContactId FROM User WHERE Id =: result ];
    	UserData uInfo = new UserData();
    	uInfo.userId = usr.Id;
    	uInfo.userName = usr.Name;

    	uInfo.state = usr.Contact.MailingState;
    	uInfo.address1 = usr.Contact.MailingStreet;
    	uInfo.city = usr.Contact.MailingCity;
    	uInfo.zip = usr.Contact.MailingPostalCode;
    	uInfo.country = usr.Contact.MailingCountry;
/*    	
    	uInfo.state = usr.State;
    	uInfo.address1 = usr.Street;
    	uInfo.city = usr.City;
    	uInfo.zip = usr.PostalCode;
    	uInfo.country = usr.Country;
*/  
    	return uInfo;
    } 
    
    public class AppMetadata  {
    	// User Contact Data
    	UserData userInfo {get; set;}
    	
    	// SFDC Picklists metadata
    	List<CaseServicesUtil.PickList> reportedSeverityList {get;set;}
    	List<CaseServicesUtil.PickList> urgencyList {get;set;}
    	List<CaseServicesUtil.PickList> serviceActivityList {get;set;}
    	List<CaseServicesUtil.PickList> networkEnvironmentList {get;set;}
    	List<CaseServicesUtil.PickList> failureCodeList {get;set;}
    	List<CaseServicesUtil.PickList> productFamilyList {get;set;}
    	Map<String,List<CaseServicesUtil.PickList>> productCurrentPointReleaseList {get;set;}
    	Map<String,List<CaseServicesUtil.PickList>> productUpgradePointReleaseList {get;set;}
    	// Static data
    	List<CaseServicesUtil.PickList> rmaServiceActivityList {get;set;}
    	List<CaseServicesUtil.PickList> rmaFailureCodeList {get;set;}
    	List<CaseServicesUtil.PickList> codePrefixList {get;set;}
    	
    	// Product software release list for limited set of products
    	Map<String,List<CaseServicesUtil.PickList>> softwareReleaseList {get;set;}
    } 
    
    public class UserData {
    	public String userId {get; set;}
    	public String userName {get; set;}
    	public String address1 {get; set;}
    	public String address2 {get; set;}
    	public String city {get; set;}
    	public String state {get; set;}
    	public String zip {get; set;}
    	public String country {get; set;}    	
    } 
}