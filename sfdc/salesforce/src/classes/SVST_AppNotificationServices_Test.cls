/**
 * Test class for Service Station Notification Services REST API.
 *
 * @author A.I.
 * @version 1.0
 */
@isTest
private class SVST_AppNotificationServices_Test {

	static testMethod void test_set_notification_rest() {
        Test.startTest();

		SVST_AppNotificationServices.NotificationSettings nts = new SVST_AppNotificationServices.NotificationSettings();
		nts.newComments = true;
		nts.statusChanges = true;
		        
        RestRequest request = new RestRequest();  
        request.httpMethod = 'POST';
        request.addParameter('newComments', 'true');
        request.addParameter('statusChanges', 'true');
        request.requestURI = '/notificationServices';
        
        //request.requestBody = JSON.serializePretty(nts);
        RestResponse response = new RestResponse();  
        RestContext.request = request;
        RestContext.response = response;
 
        SVST_AppNotificationServices.postAction();
        System.assert(response.statusCode == 400,'Response with status '+response.statusCode+' and body "'+response.responseBody.toString()+'". Expected 400.');
           
        Test.stopTest();	
	}

	static testMethod void test_get_notification_rest() {
        Test.startTest();
        
        RestRequest request = new RestRequest();  
        request.httpMethod = 'GET';
        RestResponse response = new RestResponse();  
        RestContext.request = request;
        RestContext.response = response;
 
        request.requestURI = '/notificationServices';
        SVST_AppNotificationServices.getAction();
        System.assert(response.statusCode == 200,'Response with status '+response.statusCode+' and body "'+response.responseBody.toString()+'". Expected 200.');
           
        Test.stopTest();	
	}

	static testMethod void test_notification() {
        
        Test.startTest();
        
        SVST_AppNotificationServices.testNotification();
        
        Test.stopTest();
    }

}