/**
 * Test class for Service Station Notification Services REST API.
 *
 * @author A.I.
 * @version 1.0
 */
@isTest (seeAllData=true)
private class SVST_PushNotificationUtil_Test {

	static testMethod void test_push_notification() {
		List<User> usr = [select Id, Name from User limit 3];
		Set<String> uSet = new Set<String>();
		for (User u : usr){
			uSet.add(u.Id);
		}
		List<Case> cLst = [select Id, CaseNumber, RecordType.DeveloperName from Case where RecordType.DeveloperName = 'RMA_Service_Request' limit 10];
        Test.startTest();
		SVST_PushNotificationUtil.sendNotification('Test notiication', uSet, cLst[0]);
        Test.stopTest();	
	}

	static testMethod void test_notification_logic() {

        Test.startTest();
		// Passthrough case
		SVST_PushNotificationUtil.validateNotification('Test notiication');
		// ORACLE mesage do not send notify
		SVST_PushNotificationUtil.validateNotification('Test Outstanding RMA Return Reminder, RMA Number');		
        Test.stopTest();	
		
	}
	
	static testMethod void test_casecomment_notification() {

		List<CaseComment> cmtList = [SELECT ParentId, IsPublished, Id, CreatedBy.Name, CreatedById, CommentBody From CaseComment limit 3];

        Test.startTest();
        // Check CaseComent notification
        if(cmtList != null && cmtList.size()>0){
			SVST_PushNotificationUtil.sendCommentNotification(cmtList[0]);
        }
        Test.stopTest();	
		
	}
	
	static testMethod void test_case_status_notification() {

		List<Case> caseList = [SELECT Id, Contact.Name, CreatedById, RecordTypeId, RecordType.DeveloperName, ContactId, Case_Number__c, CaseNumber, Status FROM Case limit 3];
	    	
        Test.startTest();
        // Check Case status notification
		if (caseList != null && caseList.size() > 0){
			SVST_PushNotificationUtil.sendCaseStatusNotification(caseList[0]);
		}
        Test.stopTest();	
		
	}
	
}