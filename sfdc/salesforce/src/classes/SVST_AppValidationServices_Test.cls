/**
 * Helper Test class for Part Validation Services REST API.
 *
 * @author A.I
 * @version 1.0
 */
@isTest (seeAllData=true)
private class SVST_AppValidationServices_Test {

    static testMethod void test_part_validation() {

        Test.startTest();
        // Validate incorrect porduct
		SVST_AppValidationServices.ValidationResult res = SVST_AppValidationServices.partValidation('001-0001');
		System.assert(res.code == 'V001','Part do not exists'+res.message+'". Expected V001.');
		
		//Validate existing part
		SVST_AppValidationServices.ValidationResult res1 = SVST_AppValidationServices.partValidation('100-00016');
     	System.assert(res1.code == 'V000','Part do not exists'+res.message+'". Expected V000.');
		
		
        RestRequest request = new RestRequest();  
        request.httpMethod = 'GET';
        RestResponse response = new RestResponse();  
        RestContext.request = request;
        RestContext.response = response;
        
        request.requestURI = '/validationServices';
        
        SVST_AppValidationServices.getAction();
        System.assert(response.statusCode == 200,'Response with status '+response.statusCode+' and body "'+response.responseBody.toString()+'". Expected 200.');
		
        Test.stopTest();

    }
}