/**
 * SVST_PushNotificationUtil utility class helps to send PushNotifications
 *
 * @author A.I
 * @version 1.0
 */
public class SVST_PushNotificationUtil {

	/**
	 Test code
	 Set<String> u = new Set<String>();
	 u.add('005n0000000KBUT'); //Mobiel demo
	 u.add('005n0000000LfMb'); //Mobile demo2
	 u.add('005n0000000Lfwy'); //Mobile demo4
	 u.add('005n0000000Lfwt'); //Mobile demo3
	 u.add('005n0000000Lfx3'); //Mobile demo5
	 List<Case> c = [SELECT Id, CaseNumber, RecordType.DeveloperName from Case where RecordType.DeveloperName = 'RMA_Service_Request' limit 5];
	 SVST_PushNotificationUtil.sendNotification('Test notification app', u, c[0]);
	*/
	// Send notifictaion to both iOS and Android no way to know what model user
	public static void sendNotification(String txt, Set<String> users, Case c){
		try{
			// iOS
			sendNotificationApple(txt, users, c);
			
			// Android 
			sendNotificationDroid(txt, users, c);
		}catch(Exception e){
			System.debug('ERROR: Notification '+e.getMessage());
		}
	}
	
	/**
	 * send list of recipient users and text message to Apple Notification service
	 */
	public static void sendNotificationApple(String txt, Set<String> users, Case c){
		
		// Instantiating a notification
        Messaging.PushNotification msg = new Messaging.PushNotification();

        // Assembling the necessary payload parameters for Apple.
        // Apple params are: 
        // (<alert text>,<alert sound>,<badge count>,
        // <free-form data>)
        // This example doesn't use badge count or free-form data.
        // The number of notifications that haven't been acted
        // upon by the intended recipient is best calculated
        // at the time of the push. This timing helps
        // ensure accuracy across multiple target devices.
        
		Map<String, String> notificationMsgData = new Map<String, String>();
		if (c != null){
			notificationMsgData.put('CaseNumber',c.CaseNumber);
			notificationMsgData.put('CaseId',c.Id);	
			notificationMsgData.put('CaseType',c.RecordType.DeveloperName);		
		}

        Map<String, Object> payload = 
            Messaging.PushNotificationPayload.apple(txt, '', 1, notificationMsgData);

		for(String s : payload.keySet()){
			Object o = payload.get(s);
			System.debug('### key:'+s+' - '+o);
		}
		
		
        // Adding the assembled payload to the notification
        msg.setPayload(payload);

        // Sending the notification to the specified app and users.
        // Here we specify the API name of the connected app.  
        msg.send('Mobile_Service_Station', users);		
	}

	/**
	 * send list of recipient users and text message to Android Notification service
	 */
	public static void sendNotificationDroid(String txt, Set<String> users, Case c){

		// Instantiating a notification
        Messaging.PushNotification msg = new Messaging.PushNotification();

        Map<String,String> notificationMsg = new Map<String,String>();
        notificationMsg.put('Title','A New Alert is Available');
        notificationMsg.put('Text',txt);
		if (c != null){
			notificationMsg.put('CaseNumber',c.CaseNumber);
			notificationMsg.put('CaseId',c.Id);
			notificationMsg.put('CaseType',c.RecordType.DeveloperName);			
		}
        // Adding the assembled payload to the notification
        msg.setPayload(notificationMsg);
        
        // Sending the notification to the specified app and users.
        // Here we specify the API name of the connected app.  
        msg.send('Mobile_Service_Station_Android', users);		
	
	}
		
	// Validate CaseComment notification
	//public static Boolean validateNotification(String cmtName, String txt_comment_body, String caseId) {
	public static Boolean validateNotification(String txt_comment_body) {		
		Boolean rc = true;

		String strExchange = 'Outstanding RMA Return Reminder, RMA Number'; 
        String strOutstanding = 'Calix Product Exchange Information, RMA'; 
        String strRetro = 'CLXSRRMA Alert'; 
        String strShipment = 'Calix Shipment Notification RMA Order#'; 		

        // RMA Logic details validate how to filter RMA Cases for Oracle source
        //
        //
		if (txt_comment_body != null && txt_comment_body.length() > 0
			&& (txt_comment_body.contains(strExchange) || txt_comment_body.contains(strOutstanding) ||
			txt_comment_body.contains(strRetro) || txt_comment_body.contains(strShipment))){
			rc = false;
		}      
		return rc;
	}
	//
	// Trigger Method for notification logic
	// Send notification for single comment
	public static void sendCommentNotification(CaseComment cmnt){
		// Check if user optout of this notification type
		//if(!optInCaseCommentNotification()){
		//	return;
		//}
		// Get Case Coment info list avoid Exception on nil
		List<Case> caseList = [SELECT Id, Contact.Name, CreatedById, RecordTypeId, RecordType.DeveloperName, ContactId, Case_Number__c, CaseNumber FROM Case WHERE Id =: cmnt.ParentId];
		List<CaseComment> cmtList = [SELECT ParentId, IsPublished, Id, CreatedBy.Name, CreatedById, CommentBody From CaseComment WHERE Id =: cmnt.Id];
		System.debug('### Coment Notification '+ cmnt.Id +''+cmnt.CommentBody);
		// Check if the comment, Case lists are valid with data
		if (caseList != null && cmtList != null && caseList.size() > 0 && cmtList.size() > 0){
			Case c = caseList[0];
			CaseComment cmt = cmtList[0];
			// Check if the comment Craete Name is NOT SAME as Case Name owner that valid fire notification
			if (cmt.CreatedBy.Name != null && !cmt.CreatedBy.Name.equals(c.Contact.Name) ){
				// Send to a user who originally created the case (Ownd=er may be reassigned)
				Set<String> usr = getUsers(c);
				System.debug('### Coment Notification Prep '+cmt.CreatedBy.Name);
				// Check if RMA Case Type validate more filtering
				if (c.RecordType != null && validateNotification(cmt.CommentBody) &&
					(c.RecordType.DeveloperName == CaseServicesParser.CASE_RTYPE_RMA ||	c.RecordType.DeveloperName == CaseServicesParser.CASE_RTYPE_RMASR)) {
					System.debug('### Coment Notification RMA sent '+c.CreatedById);	
					SVST_PushNotificationUtil.sendNotification('New Comment for case '+c.CaseNumber, usr, c);
				
				}else{ // NON RMA Case send notification					
					System.debug('### Coment Notification Regular sent '+c.CreatedById);
					SVST_PushNotificationUtil.sendNotification('New Comment for case '+c.CaseNumber, usr, c);
				
				}
			}		
		}
	}
	//
	//Send notification for Case status change
	public static void sendCaseStatusNotification(Case c) {
		if(!optInCaseStatusNotification()){
			return;
		}
		//List<Case> caseList = [SELECT Id, Contact.Name, CreatedById, RecordTypeId, RecordType.DeveloperName, ContactId, Case_Number__c, CaseNumber, Status FROM Case WHERE Id =: c.Id];
		if (c != null){
			Set<String> usr = getUsers(c);
			System.debug('### Case Notification status change sent '+c.CreatedById);
			SVST_PushNotificationUtil.sendNotification('Case:'+c.CaseNumber+' status - '+c.Status, usr, c);
			
		}
	}
	
	// Based on Case get USer to send notification to
	private static Set<String> getUsers(Case c) {
		Set<String> usr = new Set<String>();
		usr.add(c.CreatedById);
		return usr;		
	}
	
	// User options method
	private static Boolean optInCaseCommentNotification(){
		Boolean rc = true;
		SVST_Notification__c nfy = SVST_AppNotificationServices.getNotificationsSettings();
		if(nfy != null  && !nfy.New_Comments__c){
		   rc = false;	
		}
		return rc;
	}
	
	private static Boolean optInCaseStatusNotification(){
		Boolean rc = true;
		SVST_Notification__c nfy = SVST_AppNotificationServices.getNotificationsSettings();
		if(nfy != null  && !nfy.Status_Change__c){
		   rc = false;	
		}
		return rc;		
	}
	
}