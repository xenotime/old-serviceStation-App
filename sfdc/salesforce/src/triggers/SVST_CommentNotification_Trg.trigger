trigger SVST_CommentNotification_Trg on CaseComment (after insert) {

	for(CaseComment cmnt: Trigger.New){
		SVST_PushNotificationUtil.sendCommentNotification(cmnt);
    }

}