trigger SVST_CaseStatusNotification_Trg on Case (after insert, after update) {
	// Query this to handle BULK data for this trigger logic
	List<Case> caseList = [SELECT Id, Contact.Name, CreatedById, RecordTypeId, RecordType.DeveloperName, ContactId, Case_Number__c, CaseNumber, Status FROM Case WHERE Id in: Trigger.New];
	for(Case c: caseList){
		if (c.Status == 'Pending customer input' || c.Status == 'Closed' ){
			SVST_PushNotificationUtil.sendCaseStatusNotification(c);
		}
	}
	
}