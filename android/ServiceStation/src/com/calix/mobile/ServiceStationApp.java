/*
 * Copyright (C) 2014 TopCoder Inc., All Rights Reserved.
 */
/*
 * Copyright (c) 2011, salesforce.com, inc.
 * All rights reserved.
 * Redistribution and use of this software in source and binary forms, with or
 * without modification, are permitted provided that the following conditions
 * are met:
 * - Redistributions of source code must retain the above copyright notice, this
 * list of conditions and the following disclaimer.
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 * - Neither the name of salesforce.com, inc. nor the names of its contributors
 * may be used to endorse or promote products derived from this software without
 * specific prior written permission of salesforce.com, inc.
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package com.calix.mobile;

import android.app.Application;

import com.appirio.mobile.AMPushNotification;
import com.salesforce.androidsdk.push.PushNotificationInterface;
import com.salesforce.androidsdk.app.SalesforceSDKManager;
/**
 * This class is the entry of this application.
 * <p>
 * It initializes this hybrid app via Saleforce Mobile SDK.
 * <p>
 * <b>Thread Safety:</b> This class is not thread safe but will be used in a
 * thread safe manner.
 * 
 * @author TCSASSEMBLER
 * @version 1.0
 * 
 */
public class ServiceStationApp extends Application {
	/**
	 * Creates the hybrid application.
	 */
    @Override
	public void onCreate() {
		super.onCreate();
		SalesforceSDKManager.initHybrid(getApplicationContext(), new KeyImpl());
		PushNotificationInterface amp = new AMPushNotification();
    SalesforceSDKManager.getInstance().setPushNotificationReceiver(amp);
	}
}
