package com.appirio.mobile;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;

import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.util.Log;

import com.salesforce.androidsdk.app.SalesforceSDKManager;
import com.salesforce.androidsdk.rest.ClientManager;
import com.salesforce.androidsdk.rest.ClientManager.AccountInfoNotFoundException;
import com.salesforce.androidsdk.rest.RestClient;

public class APIProxy {
	
	private Context ctx;
	private static String authToken = null;
	private static String instanceUrl = null;
	private static String clientId = null;
	private static String refreshToken = null;

	public APIProxy(Context context) {
		this.ctx = context;
	}
	
	private void ensureToken() throws AccountInfoNotFoundException, AMException, JSONException {
		ClientManager mgr = new ClientManager(ctx, SalesforceSDKManager.getInstance().getAccountType(), SalesforceSDKManager.getInstance().getLoginOptions(), false);
		RestClient cli = mgr.peekRestClient();

		if (authToken == null || refreshToken == null || !refreshToken.equals(cli.getRefreshToken())) {

			authToken = cli.getAuthToken();
			refreshToken = cli.getRefreshToken();
			instanceUrl = cli.getClientInfo().instanceUrl.toString();
			clientId = cli.getClientInfo().clientId;
		}
	}
	
	public RequestResult makeSFRequest(String uri, String method, String data, boolean retry) throws AMException {
		
		try {
		  
			ensureToken();

	    RequestResult result = new RequestResult();

	    String line = null;

			URL url = new URL(uri);

			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			conn.addRequestProperty("Authorization", "OAuth " + authToken); 
			conn.setRequestMethod(method);

			if(data != null && data.length() > 0) {
        conn.setDoOutput(true);
        conn.setRequestProperty("Content-Type", 
            "application/json");
        OutputStreamWriter out = new OutputStreamWriter(conn.getOutputStream());
        out.write(data);
        out.flush();
        out.close();
      }

			StringBuilder responseBody = new StringBuilder();

			BufferedReader reader = null;

			try {
				reader = new BufferedReader(new InputStreamReader(
						conn.getInputStream()));
			} catch (IOException ex) {
				result.setSuccess(false);;
			}
			
			result.setStatus(conn.getResponseCode());
			
			if(result.getStatus() != 200) {
				result.setSuccess(false);
        reader = new BufferedReader(new InputStreamReader(
            conn.getErrorStream()));
			} else {
			  result.setSuccess(true);
			}
			
			while ((line = reader.readLine()) != null) {
				responseBody.append(line);
			}
			
			result.setResponse(responseBody.toString());
			
			try {

			  JSONObject response = new JSONObject(responseBody.toString());

				if(response.has("errorCode")) {
					result.setSuccess(false);
				}
			} catch (Exception e) {
				// not a json return, can ignore
			  // commented this out : the logic here is difficult to understand, trying to explain the reason to comment:
			  // it uses the exception to control the logic.
			  // 1. if the result is a failure, it is a JSONObject, so the code tries to parse it as JSONObject first
			  //    if parsing succeeds, but the response has a key of errorCode, the operation fails and success = false
			  // 2. if the result is for data, it is a JSONArray, the code still tries to parse it as JSONObject
			  //    but the parse will fail, in this case, success = false is not set, so the result is returned directly
			  // as the result, the exception here isn't always indicating an error. in most cases, it succeeds but it still
			  // prints the stacktrace - which is huge and causes the system to run slow.
				// e.printStackTrace();
			  Log.d("APIProxy", "Can't parse the result as JSONArray");
			}

			if (!result.isSuccess()) {
				if (retry) {
					url = new URL(instanceUrl + "/services/oauth2/token");

					conn = (HttpURLConnection) url.openConnection();

					conn.setRequestMethod("POST");
					conn.setRequestProperty("ContentType",
							"application/x-www-form-urlencoded");
					conn.setRequestProperty("Accept", "application/json");

					conn.setDoOutput(true);

					conn.getOutputStream().write(
							("grant_type=refresh_token&client_id=" + clientId
									+ "&refresh_token=" + refreshToken).getBytes());

					reader = new BufferedReader(new InputStreamReader(
							conn.getInputStream()));
					
					responseBody = new StringBuilder();
					
					while ((line = reader.readLine()) != null) {
						responseBody.append(line);
					}

					authToken = new JSONObject(responseBody.toString()).getString("access_token"); 
					
					return makeSFRequest(uri, method, data, false);

				} else {
					return result;
				}
			} else {
				return result;
			}
		} catch (Exception e) {
			e.printStackTrace();
			
			throw new AMException(e);
		}

	}
	
}