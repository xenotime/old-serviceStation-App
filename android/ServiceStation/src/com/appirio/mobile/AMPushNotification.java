package com.appirio.mobile;
import android.app.Activity;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.TaskStackBuilder;
import android.os.Bundle;
import android.util.Log;
import android.content.*;
import com.calix.mobile.R;

import com.salesforce.androidsdk.app.SalesforceSDKManager;
import com.salesforce.androidsdk.push.PushNotificationInterface;

public class AMPushNotification implements PushNotificationInterface {

  @Override
	public void onPushMessageReceived(Bundle message) {
		String notificationTitle = "test";
		String notificationText = "test case";
		if(message!=null){
			notificationTitle = message.getString("Title");
			notificationText = message.getString("Text");
		}  else {
			Log.d("onPushMessageReceivedError","message was null");
		}
		
		Context currContext = SalesforceSDKManager.getInstance().getAppContext();
		Class<? extends Activity> activityClass = SalesforceSDKManager.getInstance().getMainActivityClass();
		Intent currIntent = new Intent( currContext, SalesforceSDKManager.getInstance().getMainActivityClass());
		
		TaskStackBuilder stackBuilder = TaskStackBuilder.create(currContext);
		// Adds the back stack
		stackBuilder.addParentStack(activityClass);
		// Adds the Intent to the top of the stack
		stackBuilder.addNextIntent(currIntent);
		// Gets a PendingIntent containing the entire back stack
		PendingIntent resultPendingIntent =
		        stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);
		
		Notification noti = new Notification.Builder(currContext)
        .setContentTitle(notificationTitle)
        .setContentText(notificationText)
        .setSmallIcon(R.drawable.launcher_icon)
        .setContentIntent(resultPendingIntent)
        .build();
		
		NotificationManager notificationManager = 
				  (NotificationManager) currContext.getSystemService(Context.NOTIFICATION_SERVICE);
		
		notificationManager.notify(0,noti);
	}

	
}
