package com.appirio.mobile.plugins;

import org.apache.cordova.api.CallbackContext;
import org.apache.cordova.api.CordovaPlugin;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.appirio.mobile.APIProxy;
import com.appirio.mobile.RequestResult;

public class AMWebService extends CordovaPlugin {

	private APIProxy proxy;

	private APIProxy getProxy() {
		if(proxy == null && this.webView != null && this.webView.getContext() != null) {
			proxy = new APIProxy(this.webView.getContext());
		}
		
		return proxy;
	}
	
	public AMWebService() {
	}
	
	@Override
	public boolean execute(String action, JSONArray args,
			CallbackContext callbackContext) throws JSONException {
		
		return execute(action, args, true, callbackContext);
	}

	public boolean execute(String action, JSONArray params, boolean retry, CallbackContext callbackContext) {
		try {
		  String uri = null;
		  String method = "GET";
		  String data = null;
		  
		  if(params.length() > 0) {
		    uri = params.get(0).toString();
		  } else {
		    callbackContext.error("First parameter: uri is required");
		    return false;
		  }
			
      if(params.length() > 1) {
        method = params.get(1).toString();
      } 
      
      if(params.length() > 2) {
        data = params.get(2).toString();
      }
      
      RequestResult result = getProxy().makeSFRequest(uri, method, data, true);

      JSONObject resultData = new JSONObject();
      resultData.put("response", result.getResponse());
      resultData.put("status", result.getStatus());
      
      if(result.isSuccess()) {
        callbackContext.success(resultData.toString());
      } else {
        callbackContext.error(resultData.toString());
      }

			return result.isSuccess();
		} catch (Exception e) {
			e.printStackTrace();
			
			if(retry) {
				return execute(action, params, false, callbackContext);
			}
			
			callbackContext.error(e.getMessage());

			return false;
		}
	}
	
}
