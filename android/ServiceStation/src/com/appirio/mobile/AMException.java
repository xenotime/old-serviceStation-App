package com.appirio.mobile;

public class AMException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public AMException() {
	}

	public AMException(String detailMessage) {
		super(detailMessage);
	}

	public AMException(Throwable throwable) {
		super(throwable);
	}

	public AMException(String detailMessage, Throwable throwable) {
		super(detailMessage, throwable);
	}

}
