/*
 * Copyright (C) 2014 TopCoder Inc., All Rights Reserved.
 */
package nl.xservices.plugins;

import android.content.Intent;
import org.apache.cordova.DroidGap;
import org.apache.cordova.api.CallbackContext;
import org.apache.cordova.api.CordovaPlugin;
import org.apache.cordova.api.PluginResult;
import org.json.JSONArray;
import org.json.JSONException;

/**
 * This class gets the URL if the application is launched by custom scheme.
 * <p>
 * <b>Thread Safety:</b> This class is not thread safe but will be used in a
 * thread safe manner.
 * 
 * @author TCSASSEMBLER
 * @version 1.0
 * 
 */
public class LaunchMyApp extends CordovaPlugin {

	/**
	 * The check action.
	 */
	private static final String ACTION_CHECKINTENT = "checkIntent";

	/**
	 * Checks whether this application is launched by custom URL schema.
	 * @param action the command
	 * @param args the arguments
	 * @param callbackContext the callback context
	 */
	@Override
	public boolean execute(String action, JSONArray args,
			CallbackContext callbackContext) {
		if (ACTION_CHECKINTENT.equalsIgnoreCase(action)) {
			final Intent intent = ((DroidGap) this.webView.getContext())
					.getIntent();
			if (intent.getDataString() != null) {
				callbackContext.sendPluginResult(new PluginResult(
						PluginResult.Status.OK, intent.getDataString()));
				return true;
			} else {
				callbackContext
						.error("App was not started via the launchmyapp URL scheme. "
								+ "Ignoring this errorcallback is the best approach.");
				return false;
			}
		} else {
			callbackContext.error("This plugin only responds to the "
					+ ACTION_CHECKINTENT + " action.");
			return false;
		}
	}
}