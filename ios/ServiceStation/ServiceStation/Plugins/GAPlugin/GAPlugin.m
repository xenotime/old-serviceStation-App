//
//  GAPlugin.m
//  Calix Mobile iOS Native Module Assembly
//
//  Modified by TCSASSEMBLER on 03/08/14.
//  Copyright (c) 2014 TopCoder Inc., All rights reserved.
//

#import "GAPlugin.h"
#import "AppDelegate.h"
#import "GAIFields.h"
#import "GAIDictionaryBuilder.h"

@implementation GAPlugin
/**
 * Initializes the Google Analysis library, use tracking ID configured in configuration.plist
 * instead the one passed from JS.
 * @param command: The command context
*/
- (void) initGA:(CDVInvokedUrlCommand*)command
{
    NSString    *callbackId = command.callbackId;
    NSString    *accountID = [command.arguments objectAtIndex:0];
    NSInteger   dispatchPeriod = [[command.arguments objectAtIndex:1] intValue];

    [GAI sharedInstance].trackUncaughtExceptions = YES;
    // Optional: set Google Analytics dispatch interval to e.g. 20 seconds.
    [GAI sharedInstance].dispatchInterval = dispatchPeriod;
    // Optional: set debug to YES for extra debugging information.
    #ifdef DEBUG
    [[GAI sharedInstance].logger setLogLevel:kGAILogLevelVerbose];
    #endif
    
    // Uncomment to use dry run which won't send hits to the server
    //[GAI sharedInstance].dryRun = YES;
    
    // Create tracker instance.
    id tracker = [[GAI sharedInstance] trackerWithTrackingId:accountID];
    
    // Start Session
    [tracker set:kGAISessionControl value:@"start"];
    
    inited = YES;

    [self successWithMessage:[NSString stringWithFormat:@"initGA: accountID = %@; Interval = %d seconds",accountID, dispatchPeriod] toID:callbackId];
}

-(void) exitGA:(CDVInvokedUrlCommand*)command
{
    NSString *callbackId = command.callbackId;

    if (inited) {
        [[GAI sharedInstance] dispatch];
        // End Session
        [[[GAI sharedInstance] defaultTracker] set:kGAISessionControl value:@"end"];
    }

    [self successWithMessage:@"exitGA" toID:callbackId];
}

- (void) trackEvent:(CDVInvokedUrlCommand*)command
{
    NSString        *callbackId = command.callbackId;
    NSString        *category = [command.arguments objectAtIndex:0];
    NSString        *eventAction = [command.arguments objectAtIndex:1];
    NSString        *eventLabel = [command.arguments objectAtIndex:2];
    NSString        *eventValueStr = [command.arguments objectAtIndex:3];
    
    // Updated in Calix Mobile iOS Native Module Assembly
    // Use Google Analytics v3
    BOOL hasValue = eventValueStr != (id)[NSNull null];
    NSInteger eventValue = 0;
    if (hasValue) {
        eventValue = [eventValueStr intValue];
    }

    if (inited)
    {
        [[[GAI sharedInstance] defaultTracker] send:[[GAIDictionaryBuilder
                                                      createEventWithCategory:category
                                                      action:eventAction
                                                      label:eventLabel
                                                      value:hasValue ? [NSNumber numberWithInt:eventValue] : nil] build]];
        [self successWithMessage:[NSString stringWithFormat:@"trackEvent: category = %@; action = %@; label = %@; value = %d", category, eventAction, eventLabel, eventValue] toID:callbackId];
    }
    else
        [self failWithMessage:@"trackEvent failed - not initialized" toID:callbackId withError:nil];
}

- (void) trackPage:(CDVInvokedUrlCommand*)command
{
    NSString            *callbackId = command.callbackId;
    NSString            *pageURL = [command.arguments objectAtIndex:0];

    if (inited)
    {
        // Updated in Calix Mobile iOS Native Module Assembly
        // Use Google Analytics v3
        id tracker = [[GAI sharedInstance] defaultTracker];
        [tracker set:kGAIScreenName
               value:pageURL];
        [tracker send:[[GAIDictionaryBuilder createAppView] build]];

        [self successWithMessage:[NSString stringWithFormat:@"trackPage: url = %@", pageURL] toID:callbackId];
    }
    else
        [self failWithMessage:@"trackPage failed - not initialized" toID:callbackId withError:nil];
}

- (void) setVariable:(CDVInvokedUrlCommand*)command
{
    NSString            *callbackId = command.callbackId;
    NSInteger           index = [[command.arguments objectAtIndex:0] intValue];
    NSString            *value = [command.arguments objectAtIndex:1];

    if (inited)
    {
        // Updated in Calix Mobile iOS Native Module Assembly
        // Use Google Analytics v3
        [[[GAI sharedInstance] defaultTracker] set:[GAIFields customMetricForIndex:index]
               value:value];

        [self successWithMessage:[NSString stringWithFormat:@"setVariable: index = %d, value = %@;", index, value] toID:callbackId];
    }
    else
        [self failWithMessage:@"setVariable failed - not initialized" toID:callbackId withError:nil];
}

-(void)successWithMessage:(NSString *)message toID:(NSString *)callbackID
{
    CDVPluginResult *commandResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:message];

    [self writeJavascript:[commandResult toSuccessCallbackString:callbackID]];
}

-(void)failWithMessage:(NSString *)message toID:(NSString *)callbackID withError:(NSError *)error
{
    NSString        *errorMessage = (error) ? [NSString stringWithFormat:@"%@ - %@", message, [error localizedDescription]] : message;
    CDVPluginResult *commandResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR messageAsString:errorMessage];

    [self writeJavascript:[commandResult toErrorCallbackString:callbackID]];
}

-(void)dealloc
{
    [[GAI sharedInstance] dispatch];
    // End Session
    [[[GAI sharedInstance] defaultTracker] set:kGAISessionControl value:@"end"];
   // [super dealloc];
}

@end