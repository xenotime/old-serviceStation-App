//
//  CustomSFHybridViewController.h
//  Calix Mobile iOS Native Module Assembly
//
//  Created by Zulander on 3/09/2014.
//  Copyright (c) 2014 TopCoder Inc., All rights reserved.

#import "SFHybridViewController.h"

@interface CustomSFHybridViewController : SFHybridViewController<UIScrollViewDelegate>
/**
 * Property representing the open URL. If it's not nil, the corresponding
 * handleOpenURL() JS function will be called on the next page finished load event.
 */
@property (nonatomic, strong) NSURL *openUrl;

@end
