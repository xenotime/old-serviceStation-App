//
//  CustomSFHybridViewController.m
//  Calix Mobile iOS Native Module Assembly
//
//  Created by Zulander on 3/09/2014.
//  Copyright (c) 2014 TopCoder Inc., All rights reserved.


#import "CustomSFHybridViewController.h"

@interface CustomSFHybridViewController ()

@end

@implementation CustomSFHybridViewController {
    id delegate;
    CGPoint currentOffset;
}

@synthesize openUrl = _openUrl;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    
    return self;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void)webViewDidFinishLoad:(UIWebView *)theWebView
{
    _openUrl = [NSURL URLWithString:  @"web/index.html"];
    [super webViewDidFinishLoad:theWebView];
    if ([theWebView isEqual:self.webView] && _openUrl != nil) {
        NSString* jsString = [NSString stringWithFormat:@"handleOpenURL(\"%@\");", _openUrl];
        [self.webView stringByEvaluatingJavaScriptFromString:jsString];
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShowOrHide:)
                                                 name:UIKeyboardWillShowNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShowOrHide:)
                                                 name:UIKeyboardWillHideNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardDidShowOrHide:)
                                                 name:UIKeyboardDidShowNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardDidShowOrHide:)
                                                 name:UIKeyboardDidHideNotification object:nil];
    
    delegate = self.webView.scrollView.delegate;
}

-(void)keyboardWillShowOrHide:(NSNotification*)aNotification {
    delegate = self.webView.scrollView.delegate;
    currentOffset = self.webView.scrollView.contentOffset;
    self.webView.scrollView.delegate = self;
}

-(void)keyboardDidShowOrHide:(NSNotification*)aNotification {
    self.webView.scrollView.delegate = delegate;
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    scrollView.contentOffset = currentOffset;
}

@end
