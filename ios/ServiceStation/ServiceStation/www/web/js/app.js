/*
 * Copyright (C) 2014 TopCoder Inc., All Rights Reserved.
 *
 * Calix Mobile App UI Updates and Bug Fixes Assembly
 *
 * Author: snowone, TCSASSEMBLER
 * Version: 2.1
 *
 * Changes in 2.0:
 *    1. Fixed various bugs as required on http://community.topcoder.com/tc?module=ProjectDetail&pj=30044578.
 *    2. Added new route rules for notifications/help pages.
 *    3. Modified for v2 UI.
 *
 * Changes in 2.1
 *    1. Updated backbutton event listener to let comments/attachments related views
 *       to ignore the overridden behaviour.
 */

/*angular functions*/
var cxApp = angular.module('cxApp', [
  'ngRoute',  
  'ngAnimate',
  'ngTouch',
  'cxControllers',
  'LocalStorageModule',
  'infinite-scroll'
]).directive('listLoadedDirective', function() {
    return function(scope, element, attrs) {
        app.pickClicked = false;
        if (scope.$last){
            window.setTimeout(function () {
                try {
                    var top = $('.picklistOptions a.selected').offset().top;
                    top = top - 60;
                    $('body').scrollTop(top < 0 ? 0 : top);
                } catch (e) {
                    $('body').scrollTop(0);
                }
            }, 500);
        }
    };
});
// SFUAT Host: sfuat-calixstore.cs3.force.com/servicestation
// DEV1 Host: sfdev1-calixstore.cs30.force.com/servicestation

cordova.define("PauseApp", function (require, exports, module) {
    var exec = require("cordova/exec");
    module.exports = {
        pause: function (win, fail) {
            exec(win, fail, "PauseApp", "pause", []);
        }
    };
});

// Initialize the main module
cxApp.run(['$rootScope', '$location', '$window', '$http', 'metadataService', 'queryService',
           function ($rootScope, $location, $window, $http, metadataService, queryService) {
  
  // add backbutton event listener to override the default behavior
  // this is only needed for android because iOS will not trigger this event
  document.addEventListener("backbutton", function(e) {
      var path = $location.path();
      // ignore the back button handler if the current screen is for new comment or comments list
      if (path == '/comments' || path == '/commentsNone' || path == '/newcomment') {
          $window.history.back();
          return;
      }
      // ignore the back button handler if the current screen is for new attachment or attachments list
      if (path == '/attachments' || path == '/newAttachments' || path == '/editAttachment') {
          $window.history.back();
          return;
      }
      if (path != '/home') {
          $rootScope.$apply(function() {
              $location.path("/home");
          });
      } else {
          queryService.persist();
          var pauseApp = cordova.require("PauseApp");
          pauseApp.pause(
        	        function () {
                    // this will be called after the app is paused so it needs to do nothing
        	        },
        	        function () {
                    // this will not be called
        	        }
        	    );
      }
      e.stopPropagation();
  }, false);


  // add pause/resume event listeners to store/reload the query
  document.addEventListener("pause", function() {
      queryService.persist();
  }, false);

  document.addEventListener("resume", function() {
      queryService.refresh();
  }, false);
  // start the query refresh in background so the UI is not blocked
  setTimeout(function() { queryService.refresh() }, 100);

  $rootScope.closePopup = function (popup){
    app.hidePopup($(popup));  
  };

  $rootScope.go = function (path, pageAnimationClass, search) {
          window.clearTimeout();

            if (typeof(pageAnimationClass) === 'undefined') { // Use a default, your choice
                $rootScope.pageAnimationClass = 'crossFade';
            }
            
            else { // Use the specified animation
            pageAnimationClass+= " inMotion";
                $rootScope.pageAnimationClass = pageAnimationClass;
                $window.setTimeout(function(){
                  $rootScope.pageAnimationClass = '';
                  $('.pageView').removeClass('inMotion').removeClass('slideLeft').removeClass('slideRight');
                  $('.fixedActions .caseActions').css('bottom','0.1px');
                  $window.setTimeout(function(){
                    $('article.screen .con').css('min-height',$('#wrapper').height()+'px');                  
                  },10);
                },700);
                
            }
            window.setTimeout(function(){
              app.hideLoading();
              $('article.screen .con').css('min-height',$('#wrapper').height()+'px');                  
            },100);
    
            if (path === 'back') { // Allow a 'back' keyword to go to previous page              
              $window.history.back();              
            }
            
            else { // Go to the specified path
                $location.path(path);
                if(angular.isDefined(search)){
                  $location.search(search);
                }else{
                  //reset search
                  $location.search('');
                }
            };


    };
    
    $rootScope.handleOpenUrl = function(openUrl) {
        if (!openUrl) return;
        app.showLoading();
        SFHybridApp.logToConsole("process open url: " + openUrl);
        // Parse calix://view?case={id} to view case
        if (openUrl.indexOf("calix://view?") == 0) {
            var tokens = openUrl.split('?');
            if (tokens.length == 2) {
                var query = tokens[1];
                tokens = query.split('=');
                if (tokens.length == 2) {
                    var param = tokens[0];
                    var caseId = tokens[1];
                    if (param == 'case' && caseId) {
                        $http({
                            method: 'GET',
                            url: forcetkClient.instanceUrl + serviceEndpoint + 'caseServices/' + caseId,
                            headers: {'Authorization':'OAuth '+forcetkClient.sessionId}
                        }).success(function(data) {
                            var page = null;
                            if(data.type === 'Technical_Servce_Request'){
                                page = 'technical-service';
                            } else if (data.type === 'Software_Download_Service_Request') {
                                page = 'software'
                            } else if (data.type === 'RMA_Service_Request') {
                                page = 'rma';
                            } else {
                                // unknown type
                                navigator.notification.alert("Unknown case type: " + data.type, null, "Message");
                                return;
                            }
                            $rootScope.caseData = data;
                            $rootScope.go('/case-' + page,'slideLeft','id='+caseId+'&loaded=true');
                        }).error(function(err){
                            navigator.notification.alert("Cannot load given case", null, "Message");
                        });
                    }
                }
            }
        }
    }

    $rootScope.$on("$locationChangeStart", function(event, next, current) {
        var pathes = next.split('#/');
        if (pathes.length > 1) {
            var screenName = pathes[1];
               gaPlugin.trackPage(null, null, screenName);
        } else {
            SFHybridApp.logToConsole("Cannot track page, invalid location: " + next);
        }
    });
    // sets page title and page type
    $rootScope.$on('$routeChangeSuccess', function(ev, data) {
        if (data.title) {
            $rootScope.pageTitle = data.title;
        } else {
            $rootScope.pageTitle = "";
        }
        if(snapper){
            snapper.close('right');
        }
        // init navs
        initSlidingNavs($rootScope, $location);
    })
}]);


cxApp.factory('authHttpResponseInterceptor',['$q','$location','$rootScope',function($q,$location,$rootScope){
    return {
        response: function(response){
            if (response.status === 401) {
            }
            return response || $q.when(response);
        },
        responseError: function(rejection) {
            if (rejection.status === 401) {
                // Force logout on 401 error
                var sfOAuthPlugin = cordova.require("salesforce/plugin/oauth");
                sfOAuthPlugin.logout();
            }
            return $q.reject(rejection);
        }
    }
}]);

//navs
var snapper = "";
var snEna = true;
function initSlidingNavs(sc, location) {
    sc.currentpage = location.path();
    sc.currentFooter = false;
    window.setTimeout(function() {
        snapper = new Snap({
            element : $('.pageContainer')[0],
            dragger : $('.handle')[0],
            disable : 'right',
            maxPosition : 46,
            minPosition : -46,
            transitionSpeed: 0.2
        });

        $('header .menuLink').on('click', function() {
            if($(this).hasClass('backLink')){
                return false;
            }
            if (snEna === true) {
                snEna = false;
                if (snapper.state().state == "left") {
                    snapper.close();
                } else {
                    snapper.open('left');
                }
                window.setTimeout(function() {
                    snEna = true;
                }, 500)
            }
        })
        snapper.on('open', function() {
            window.setTimeout(function() {
                $('.page-main').scrollTop(0);
            }, 100)
        })
        $('.rightSidebar li').on('click', function() {
            snapper.close('right');
        })
    }, 100)

}

/* page routing */
cxApp.config(['$routeProvider','$httpProvider', function($routeProvider,$httpProvider) {

    // cors
    $httpProvider.defaults.useXDomain = true;
    delete $httpProvider.defaults.headers.common['X-Requested-With'];

    // 401 Unauthorized handling
    $httpProvider.interceptors.push('authHttpResponseInterceptor');

    $routeProvider.
      when('/home',{
        templateUrl: 'partials/home.html'
      }).
      when('/case-technical-service',{
        templateUrl: 'partials/case-technical-service.html'
      }).
      when('/case-software',{
        templateUrl: 'partials/case-software.html'
      }).
      when('/case-rma',{
        templateUrl: 'partials/case-rma.html'
      }).
      when('/homeSearch',{
        templateUrl: 'partials/home-search.html'
      }).
      when('/comments',{
        templateUrl: 'partials/comments.html'
      }).
      when('/commentsNone',{
        templateUrl: 'partials/comment-none.html'
      }).
      when('/attachments',{
        templateUrl: 'partials/attachments.html'
      }).      
      when('/edit-case-software',{
        templateUrl: 'partials/case-software-edit.html'
      }).
      when('/edit-case-technical-service',{
        templateUrl: 'partials/case-technical-service-edit.html'
      }).
      when('/edit-case-rma',{
        templateUrl: 'partials/case-rma-edit.html'
      }).
      when('/newcomment',{
        templateUrl: 'partials/comment-new.html',
        controller: 'newCommentCtrl'
      }).
      when('/newAttachments',{
        templateUrl: 'partials/attachment-new.html'
      }).
      when('/editAttachment',{
        templateUrl: 'partials/attachment-edit.html'
      }).
      when('/createTechnicalService',{
        templateUrl: 'partials/case-technical-service-create.html'
      }).      
      when('/createSoftware',{
        templateUrl: 'partials/case-software-create.html'
      }).      
      when('/createRma',{
        templateUrl: 'partials/case-rma-create.html',
        reloadOnSearch: true
      }).       
      when('/createRmaJump',{
        templateUrl: 'partials/case-rma-create-jump.html'
      }).    
      when('/createRMALine',{
        templateUrl: 'partials/case-rma-create-line.html',
        reloadOnSearch: true
      }).   
      when('/addPartNo',{
        templateUrl: 'partials/addPartNo.html'
      }).    
      when('/createRMAVerify',{
        templateUrl: 'partials/case-rma-create-verify.html'
      }).
      when('/rmaReview',{
        templateUrl: 'partials/case-rma-review.html'
      }).
      when('/picklist',{
        templateUrl: 'partials/picklist-opts.html'
      }).when('/notifications', {
        templateUrl : 'partials/notifications.html',
        controller : 'notificationsCtrl'
      }).when('/help', {
        templateUrl : 'partials/help.html'
      }).
      otherwise({
        redirectTo: '/home'
      });

  }]);
