/*
 * Copyright (C) 2014 TopCoder Inc., All Rights Reserved.
 *
 * Calix Mobile App UI Updates and Bug Fixes Assembly
 *
 * Author: snowone, TCSASSEMBLER
 * Version : 2.0
 * Changes:
 * 1. Add the notification service.
 * 2. Add the delete method for the attachment service..
 *
 * Version 1.5
 * Changes:
 * 1. Add the query parameters for paging feature.
 * 2. Add two helper methods to for software release property.
 */

cxApp.service('caseService', function(sfProxy) {

  this.caseData = {};
  this.currentLine = 0;
  this.currentItem = {};
  var dirty = false;
  
  function getQueryString(query){
    if(angular.isUndefined(query)){
      return "";
    }
    var queryString = "?";
    if(query.type != ''){
      queryString += 'type='+query.type + '&';
    }
    if(query.sort != ''){
      queryString += 'sort='+query.sort +'&';
    }
    if(query.status != ''){
      queryString += 'status='+query.status;
    }

    // Add offset and limit query parameters, load one more than limit for checking whether it needs to load more
    // in next request
    queryString += '&offset=' + query.offset + '&limit=' + (query.limit + 1);
    return queryString;
  }

  var caseService = {
    getAll: function(query) {
      return sfProxy.makeRequest({
        method: 'GET',
        url: forcetkClient.instanceUrl + serviceEndpoint + 'caseServices'+getQueryString(query),
        headers: {'Authorization':'OAuth ' + forcetkClient.sessionId}
      });
    },
    getCaseById: function(id) {
    	return sfProxy.makeRequest({
        method: 'GET',
        url: forcetkClient.instanceUrl + serviceEndpoint + 'caseServices/' + id,
        headers: {'Authorization':'OAuth '+forcetkClient.sessionId}
      });
    },
    closeById: function(id){
      var promise = sfProxy.makeRequest({
        method: 'PUT',
        url: forcetkClient.instanceUrl + serviceEndpoint + 'caseServices/' + id+'/close',
        headers: {'Authorization':'OAuth '+forcetkClient.sessionId}
      });
      return promise;
    },
    getUserInfo: function(id){
      // TODO: not sure how to test this!
      var promise = sfProxy.makeRequest({
        method: 'GET',
        url: forcetkClient.instanceUrl + serviceEndpoint + 'caseServices/'+id+'/partner',
        headers: {'Authorization':'OAuth '+forcetkClient.sessionId}
      });
      return promise;
    },
    save: function () {

      if(angular.isDefined(this.caseData.id)){
        // filter fields depending on type
        if(this.caseData.type === 'Technical_Service_Request'){
          delete this.caseData.items;
        } else if (this.caseData.type === 'Software_Download_Service_Request') {
          delete this.caseData.city;
          delete this.caseData.state;
          delete this.caseData.country;
          delete this.caseData.items;
        } else{
          this.caseData.serviceActivity = null;
          var length = this.caseData.items.length;
          var item = this.caseData.items[length-1];
           if(item == null || typeof item == 'undefined') {
              this.caseData.items.splice(-1,1);
              // console.log( 'deleted a null');
            }
        } 
        //remove id
        var _id = this.caseData.id;
        delete this.caseData.id;
        // status cannot be assigned
        delete this.caseData.status;
        // reported severity is not editable
        delete this.caseData.reportedSeverity;
        // $http returns a promise, which has a then function, which also returns a promise
        var promise = sfProxy.makeRequest({
          method: 'PUT',
          url: forcetkClient.instanceUrl + serviceEndpoint + 'caseServices/' + _id,
          headers: {'Authorization':'OAuth '+forcetkClient.sessionId},
          data: this.caseData
        });
        
        return promise;
      } else {
        if(this.caseData.type === 'Technical_Service_Request'){
          delete this.caseData.items;
        } else if (this.caseData.type === 'Software_Download_Service_Request') {
          delete this.caseData.items;
        } else{
          var length = this.caseData.items.length;
            var item = this.caseData.items[length-1];
            if(item == null || typeof item == 'undefined') {
                this.caseData.items.splice(-1,1);
            }
        } 
        // insert new
        var promise = sfProxy.makeRequest({
          method: 'POST',
          url: forcetkClient.instanceUrl + serviceEndpoint + 'caseServices',
          headers: {'Authorization':'OAuth '+forcetkClient.sessionId},
          data: this.caseData
        });
        // console.log('RESULT: '+JSON.stringify(promise));
        // Return the promise to the controller
        return promise;
      }
    },
    setCurrentItem: function (data) {
        dirty = true;
        this.currentItem = data;
    },
    setCase: function (data) {
        dirty = true;
        caseData = data;
    },
    setCaseProperty: function (property,value) {
      dirty = true;
      this.caseData[property] = value;
    },
    setCaseItemProperty: function (property,value,index) {
      dirty = true;
      this.caseData.items[index][property] = value;
    },
    getCase: function () {
        return caseData;
    },
    isDirty: function () {
        return dirty;
    },
    setDirty: function (data) {
        dirty = data;
    },
    clear: function (){
      reset();
    }
  };
  return caseService;
});

cxApp.service('commentService', function(sfProxy) {

  var commentsService = {
    getAllByCaseId: function(id) {
      // $http returns a promise, which has a then function, which also returns a promise
      var promise = sfProxy.makeRequest({
        method: 'GET',
        url: forcetkClient.instanceUrl + serviceEndpoint + 'caseServices/'+id+'/comments',
        headers: {'Authorization':'OAuth '+forcetkClient.sessionId}
      });
      // Return the promise to the controller
      return promise;
    },
    save: function (id,data) {
      // insert new
      var promise = sfProxy.makeRequest({
        method: 'POST',
        url: forcetkClient.instanceUrl + serviceEndpoint + 'caseServices/'+id+'/comments',
        headers: {'Authorization':'OAuth '+forcetkClient.sessionId},
        data: data
      });
      // Return the promise to the controller
      return promise;
    }
  };
  return commentsService;
});

cxApp.service('attachmentService', function(sfProxy) {

  var attachmentsService = {
    getAllByCaseId: function(id) {
      // $http returns a promise, which has a then function, which also returns a promise
      var promise = sfProxy.makeRequest({
        method: 'GET',
        url: forcetkClient.instanceUrl + serviceEndpoint + 'caseServices/'+id+'/attachments',
        headers: {'Authorization':'OAuth '+forcetkClient.sessionId}
      });
      // Return the promise to the controller
      return promise;
    },
    getByCaseIdAttachmentId: function(id, attachmentId) {
      var promise = sfProxy.makeRequest({
        method: 'GET',
        url: forcetkClient.instanceUrl + serviceEndpoint + 'caseServices/'+id+'/attachments/'+attachmentId,
        headers: {'Authorization':'OAuth '+forcetkClient.sessionId}
      });
      return promise;
    },
    save: function (id,data) {
      // insert new
      var promise = sfProxy.makeRequest({
        method: 'POST',
        url: forcetkClient.instanceUrl + serviceEndpoint + 'caseServices/'+id+'/attachments',
        headers: {'Authorization':'OAuth '+forcetkClient.sessionId},
        data: data
      });
      return promise;
    },
    delete: function (id, attachmentId) {
      // delete the given attachment.
      var promise = sfProxy.makeRequest({
        method: 'DELETE',
        url: forcetkClient.instanceUrl + serviceEndpoint + 'caseServices/'+id+'/attachments/' + attachmentId,
        headers: {'Authorization':'OAuth '+forcetkClient.sessionId}
      });
      return promise;
    }
  };
  return attachmentsService;
});

cxApp.service('queryService', function(localStorageService) {
    /**
     * The key for local storage.
     */
    var queryKey = 'calix-query';
    var typesKey = 'calix-types';
    var statusesKey = 'calix-statuses';

  this.query = {
      status: 'unassigned',
      type: 'rma,sw,tech',
      sort: 'dcn',
      offset: 0,
      limit: caseListPageSize
    };

  this.enabledTypes = {
    rma: true,
    sw: true,
    tech: true
  };

  this.enabledStatus = {
    unassigned: true,
    workInProgress: false,
    closed: false
  };

  this.persist = function() {
      if (localStorageService) {
          localStorageService.set(queryKey, this.query);
          localStorageService.set(typesKey, this.enabledTypes);
          localStorageService.set(statusesKey, this.enabledStatus);
      }
  }
  
  this.refresh = function() {
      if (localStorageService) {
          var localQuery = localStorageService.get(queryKey);
          if (localQuery) {
              this.query = localQuery;
          }
          var localTypes = localStorageService.get(typesKey);
          if (localTypes) {
              this.enabledTypes = localTypes;
          }
          var localStatuses = localStorageService.get(statusesKey);
          if (localStatuses) {
              this.enabledStatus = localStatuses;
          }
      }
  }

  this.toggleType = function(type) {
    this.enabledTypes[type] = !this.enabledTypes[type];
    this.query.type = "";
    if(this.enabledTypes.tech){
      this.query.type += 'tech,'
    }
    if(this.enabledTypes.rma){
      this.query.type += 'rma,'
    }
    if(this.enabledTypes.sw){
      this.query.type += 'sw'
    }
  }

    this.toggleStatus = function(status) {
        this.enabledStatus[status] = !this.enabledStatus[status];
        this.query.status = "";
        if(this.enabledStatus.unassigned){
            if (this.query.status) {
                this.query.status += ',';
            }
            this.query.status += 'unassigned'
        }
        if(this.enabledStatus.closed){
            if (this.query.status) {
                this.query.status += ',';
            }
            this.query.status += 'closed'
        }
        if(this.enabledStatus.workInProgress){
            if (this.query.status) {
                this.query.status += ',';
            }
            this.query.status += 'Work in progress';
        }
  }

   this.refresh();
});

cxApp.service('metadataService', function(sfProxy, localStorageService, $interval) {

    if (!localStorageService) {
        SFHybridApp.logToConsole("failed to get local storage");
    }
    /**
     * The key for local storage.
     */
    var lsKey = 'calix-metadata';

    /**
     * The local cached metadata in memory.
     */
    var metadata = localStorageService ? localStorageService.get(lsKey) : {};
    metadata = metadata ? metadata : {};

    var metadataService = {

        /**
         * Gets the current user info.
         * @return the current user info. It may return empty if the remote call hasn't finished.
         */
        currentUserInfo : function() {
            return metadata['userInfo'] ? metadata['userInfo'] : {};
        },
    
        /**
         * Gets the full metadata.
         * @return the full metadata.
         */
        allMetadata: function() {
            return metadata;
        },

        /**
         * Refreshes the metadata.
         */
        refresh : function() {
            sfProxy.makeRequest({
                method: 'GET',
                url: forcetkClient.instanceUrl + serviceEndpoint + 'metadataServices',
                headers: {'Authorization':'OAuth '+forcetkClient.sessionId}
            }).success(function(data){
                // update the metadata in memory and also update the local storage 
                metadata = data;
                if (localStorageService) {
                    localStorageService.set(lsKey, metadata);
                }
             })
            .error(function(err, status){
                SFHybridApp.logToConsole("metadata refresh error : " + err);
            });
        },

        /**
         * Check whether the given part number has the non-empty software releases.
         *
         * @param partNumberName the given part number name.
         * @return true if the given part number has the non-empty software releases. false otherwise.
         */
        hasSoftwareRelease : function(partNumberName) {
            return angular.isArray(metadata.softwareReleaseList[partNumberName]) &&
            	(metadata.softwareReleaseList[partNumberName].length > 0);
        },

        /**
         * Get the display name based on the given part number and software release value.
         *
         * @param partNumberName the given part number name.
         * @param softwareReleaseId the given software release id.
         * @return the display name.
         */
        getSoftwareReleaseDisplayName : function(partNumberName, softwareReleaseId) {
            if (!this.hasSoftwareRelease(partNumberName)) {
                return "";
            } else {
                var values = metadata.softwareReleaseList[partNumberName];
                for (var idx = 0; idx < values.length; idx++) {
                    if (values[idx].value === softwareReleaseId) {
                        return values[idx].label;
                    }
                }
                return "";
            }
        }
    };

    // start the timer to refresh the metadata
    metadataService.refresh();
    $interval(function() {
        metadataService.refresh();
    }, metadataRefreshInterval);

    return metadataService;
});

cxApp.service('notificationsService', function(sfProxy, localStorageService) {

    if (!localStorageService) {
        SFHybridApp.logToConsole("failed to get local storage");
    }
    /**
     * The key for local storage.
     */
    var lsKey = 'calix-notifications';

    var notificationsService = {
        /**
         * Refreshes the notifications.
         */
        refresh : function() {
            sfProxy.makeRequest({
                method: 'GET',
                url: forcetkClient.instanceUrl + serviceEndpoint + 'notificationServices',
                headers: {'Authorization':'OAuth '+forcetkClient.sessionId}
            }).success(function(data){
                this.notifications = data;
                if (localStorageService) {
                    localStorageService.set(lsKey, this.notifications);
                }
             })
            .error(function(err, status){
                SFHybridApp.logToConsole("notifications refresh error : " + err);
            });
        },
        /**
         * Saves the notifications.
         */
        save : function() {
            var promise = sfProxy.makeRequest({
                method: 'POST',
                url: forcetkClient.instanceUrl + serviceEndpoint + 'notificationServices',
                headers: {'Authorization':'OAuth '+forcetkClient.sessionId},
                data: this.notifications
            }).success(function(data){
                this.notifications = data;
                if (localStorageService) {
                    localStorageService.set(lsKey, this.notifications);
                }
             });
            return promise;
        }
    };

    /**
     * The local cached notifications in memory.
     */
    notificationsService.notifications = {
      newComments : true,
      statusChanges : true
    };

    var lsNotifications = localStorageService.get(lsKey);
    if (lsNotifications) {
      notificationsService.notifications = lsNotifications;
    }

    // refresh is still necessary because user may reinstall the app, in that case
    // the configuration is not in local storage but in the salesforce server
    notificationsService.refresh();

    return notificationsService;
});

