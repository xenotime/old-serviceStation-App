cxApp.service('sfProxy', function($q, $http) {
  var sfProxy = {
    deviceType: (navigator.userAgent.match(/iPad/i))  == "iPad" ? "iPad" : (navigator.userAgent.match(/iPhone/i))  == "iPhone" ? "iPhone" : (navigator.userAgent.match(/Android/i)) == "android" ? "Android" : (navigator.userAgent.match(/BlackBerry/i)) == "BlackBerry" ? "BlackBerry" : "null",
    makeRequest: function(config) {
      if(this.deviceType === 'Android') {
        var deferred = $q.defer();
        
        if(!config.data) {
          config.data = "";
        }

        if(!config.method) {
          config.method = 'GET';
        }

        deferred.promise.success = function(fn){
            deferred.promise.then(function(response){
                    fn(response)
                 }, null);
                    return deferred.promise
              };

          deferred.promise.error = function(fn){
            deferred.promise.then(null, function(response){
                    fn(response)
                 });
                 return deferred.promise;
              };    

          setTimeout(function() {
              cordova.exec(
                function(success){
                  deferred.resolve(JSON.parse(JSON.parse(success).response));
                }, 
                function(error){
                  var e = JSON.parse(error);
                  deferred.reject(JSON.parse(e.response), e.status);
                }, 
                "com.appirio.mobile.webservice", 
                "getRestData", 
                [config.url, config.method, config.data]);
          }, 100);
            
        return deferred.promise;
      } else {
        var promise = $http(config);
        
        return promise;        
      }
    }
  }

  return sfProxy;
});