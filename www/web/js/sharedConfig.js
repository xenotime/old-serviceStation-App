/*
 * Copyright (C) 2014 TopCoder Inc., All Rights Reserved.
 *
 * Calix Mobile App Case View Pagination & RMA Line Item Assembly v1.0
 *
 * Author: snowone, TCSASSEMBLER
 * Changes:
 * 1. Add the configuration item for case list's page size.
 */

// Represents the Google Analysis tracking ID.
var gaTrackingId = "UA-49626416-1";

// Represents the Google Analysis dispatch interval in seconds.
// All hits will be queued and sent per interval to reduce network calls in order to improve performance.
// This may be ignored on Android devices with Google Play Service
var gaDispatchInterval = 20;

// Represents the Calix salesforce REST service endpoint
var serviceEndpoint = '/services/apexrest/';

// Represents the link to RMA policy page
var rmaPolicyLink = 'http://portal.calix.com/portal/calixdocs/misc/RMA_Policy.htm';

// Represents the metadata refresh interval in milliseconds.
var metadataRefreshInterval = 10000;

// Represents the page size for picklist.
var picklistPageSize = 20;

// Represents the page size for case list.
var caseListPageSize = 50;