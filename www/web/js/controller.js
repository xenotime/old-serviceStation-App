/*
 * Copyright (C) 2014 TopCoder Inc., All Rights Reserved.
 *
 * Calix Mobile App Case View Pagination & RMA Line Item Assembly v1.0
 *
 * Author: snowone, TCSASSEMBLER
 * Changes:
 * 1. Add the logic for scrolling and paging.
 */

var cxControllers = angular.module('cxControllers', []);

var menuEnabled = false;

/* master controller */
cxControllers.controller('masterCtrl', ['$scope', '$routeParams', '$rootScope', 
    function($scope, $routeParams, $rootScope) {
			$scope.picklistTitle = "";

			$scope.pick = function(e, values, callback, property, selectedValue){
			    // for states, if the list is empty, we simply return thus the list will not show
			    if (property === 'state') {
			        if (angular.isUndefined(values.length)) {
	                    return;
	                }
			    }
                if (angular.isUndefined(values) || values.length === 0) {
                    $scope.errorDetail = {
                        message: "Selection Error:",
                        details: "Missing 'Product Family'"
                    };
                    app.hideLoading();
                    app.showPopup($('.errorPopup'));
                                        
                    return;
                }

                $scope.pageSize = picklistPageSize;
                var _this = $(e.currentTarget);
                $scope.picklistTitle = _this.find('span:first').html();
                $scope.values = values;
				$scope.callback = callback;
				$scope.property = property;
				$scope.selectedValue = selectedValue;

				var selectedIndex = -1;
				for (i = 0; i < $scope.values.length; i++) {
				    if ($scope.values[i].value === selectedValue) {
				        selectedIndex = i;
				        break;
                    }
				}
                // add paging support for the picklist
				if (selectedIndex == -1) {
	                $scope.number = angular.isUndefined($scope.number) ? $scope.pageSize : $scope.number + $scope.pageSize;
				} else {
				    $scope.number = selectedIndex;
				}
				if ($scope.number < $scope.pageSize) {
				    $scope.number = $scope.pageSize;
				}
                $scope.valuesToDisplay = values.slice(0, $scope.number);

                $rootScope.go('/picklist', 'slideLeft');
			};

			$scope.loadMoreOptions = function() {
			    $scope.number += $scope.pageSize;
                $scope.valuesToDisplay = $scope.values.slice(0, $scope.number);
			}

			$scope.getClass = function(type){
	    	    if(type === 'Technical_Service_Request'){
	    		    return 'technical-service';
	    	    } else if (type === 'Software_Download_Service_Request') {
	    		    return 'software'
	    	    } else if (type === 'RMA_Service_Request') {
	    	        return 'rma';
                } else if (type === 'RMA_Read_Only') {
                    return 'rma';
	    	    } else {
	    	        return 'unknown';
	    	    }
	        };
			$scope.handelNetworkError = function(err, status, errMsgPrefix) {
                console.log('NETWORK ERROR' + JSON.stringify(err));

                // Error message in response
                var errorMessage = err.details ? err.details : err.message;
                errorMessage = errorMessage ? errorMessage : err;
		    	errorMessage = errorMessage ? errorMessage : '.';
                //errormessage = errMsgPrefix ? errMsgPrefix + ': ' + errorMessage : errorMessage;
                if(errMsgPrefix) errMsgPrefix = errMsgPrefix + ': ';
                if(!err.message) err.message = 'Error'
		  		$scope.errorDetail = {
                    message: err.message,
                    details: errMsgPrefix + errorMessage
                };
                // Error message by status
 	    	    if (status == 404) {
		  		    $scope.errorDetail = {
                        message: 'Network Error',
                        details: 'Please try again later.'
                    };
	    	    }
		    	app.hideLoading();
		  		app.showPopup($('.errorPopup'));
	        };
    }
])

var menuEnabled = false;
/* home controller */
cxControllers.controller('homeCtrl', ['$scope', '$routeParams', '$rootScope', '$http',
	'caseService', 'queryService',
	function($scope, $routeParams, $rootScope, $http, caseService, queryService) {
        // console.log("home controller");
        $scope.menuEnabled = false;
		if(menuEnabled){
			window.setTimeout(function(){
				menuEnabled = false;	
			},100);
			
			$scope.menuEnabled = true;
		}

		$scope.query = queryService.query;
		$scope.enabledTypes = queryService.enabledTypes;
		$scope.enabledStatus = queryService.enabledStatus;
                                      
		$scope.search = function(){
            // Just return if there is another search is going on
            if ($scope.busy === true) {
                return;
            }

            $scope.busy = true;
            $scope.initCtrl = false;

            app.showLoading();
			caseService.getAll(queryService.query)
				.success(function(data){
                    $scope.hasMoreData = angular.isArray(data) && ((data.length - 1) === $scope.query.limit);
                    queryService.query.offset += queryService.query.limit;
                    if (angular.isArray($scope.casesData)) {
                         var total = data.length - 1;
                         if (data.length <= $scope.query.limit) {
                            total = data.length;
                         }
                         $.each(data, function(index, value) {
                            if (index < total) {
                                $scope.casesData.push(value);
                            }
                         });
                    } else {
                         $scope.casesData = data;
                    }
                    $scope.busy = false
					app.hideLoading();
				})
    			.error(function(err, status){
                    $scope.busy = false
                    $scope.handelNetworkError(err, status, 'Failed to load cases');
    		    });
		}

        $scope.busy = false;
        $scope.hasMoreData = true;
        $scope.initCtrl = true;
        queryService.query.offset = 0;

		$scope.changeSort = function(sort){
			queryService.query.sort = sort;
            $scope.casesData = null;
            $scope.hasMoreData = true;
            queryService.query.offset = 0;
			$scope.search();
		}

		$scope.changeFilter = function(filter){
			if(filter == 'Technical Service'){
				queryService.toggleType('tech');
			} else if(filter == 'Software Request'){
				queryService.toggleType('sw');
			} else if(filter == 'RMA'){
				queryService.toggleType('rma');
			} else if(filter == 'Open'){
				queryService.toggleStatus('unassigned');
			} else if(filter == 'Closed'){
				queryService.toggleStatus('closed');
			} else if (filter == 'Work in Progress') {
				queryService.toggleStatus('workInProgress');
			}
            $scope.casesData = null;
            $scope.hasMoreData = true;
            queryService.query.offset = 0;
			$scope.search();
		}

		$scope.toggleNav = function(){
			$scope.menuEnabled  = !$scope.menuEnabled ;
		}
		
		//sidebar menu toggle
		$scope.toggleList = function(e){
			angular.element(e.target).parent().toggleClass('collapsed');
		}
		
		
		$scope.navTo = function(e,id){
			var page = angular.element(e.currentTarget).attr('class').split( )[0];
			$rootScope.go('case-'+page,'slideLeft','id='+id);
		}

		$scope.logout = function(){
             var sfOAuthPlugin = cordova.require("salesforce/plugin/oauth");
             sfOAuthPlugin.logout();
		}		
		
		$('.sidebarMenu .sortOptions li .option').off().on('click',function(){
			$('.sortEnabled',$(this).closest('.sortOptions')).removeClass('sortEnabled');
			$(this).parent().addClass('sortEnabled');
			return false;
		});
		
		$('.sidebarMenu .filterOptions li .option').off().on('click',function(){
			$scope.changeFilter($(this).context.innerText);
			$(this).parent().toggleClass('sortEnabled');
			return false;
		});

        // The function to load more cases while scrolling, technically this method is not necessary but to be
        // consistent with option loading and has more meaningful method name, loadMoreCases is defined to wrap
        // search method again.
        $scope.loadMoreCases = function() {
            $scope.search();
        }

        $scope.hasCases = function() {
            return $scope.busy || (angular.isArray($scope.casesData) && ($scope.casesData.length > 0));
        }
	}
]);

/* home search controller */
cxControllers.controller('homeSearchCtrl', ['$scope', '$routeParams', '$rootScope','caseService',
    function($scope, $routeParams, $rootScope, caseService) {
    
		angular.element('.searchInput').focus();
		$scope.searchFilter = '';
		
		app.showLoading();
		caseService.getAll()
			.success(function(data){
				$scope.caseData = data;
				app.hideLoading();
			})
			.error(function(err, status){
                $scope.handelNetworkError(err, status, 'Failed to search cases');
		    });

		$scope.navTo = function(e,id){
			var page = angular.element(e.currentTarget).attr('class').split( )[0];
            if (page == 'unknown') {
                return;
            }
			$rootScope.go('case-'+page, 'slideLeft','id='+id);
		}
		
		$scope.goTohome = function(){
			$rootScope.go('back', 'slideRight');
		}
    }
]);

/* case controller */
cxControllers.controller('caseCtrl', ['$scope', '$routeParams','caseService', '$rootScope',
    function($scope, $routeParams, caseService, $rootScope) {
        $scope.hasSoftwareRelease = function (softwareRelease) {
            return angular.isString(softwareRelease) && $.trim(softwareRelease).length > 0
        }

            /////////////////
            // Call User Info service to get Partner data
            $scope.showForPartner = false;
            caseService.getUserInfo('17392111015850828802')
                                      .success(function(data){
                                              // console.log("USER Success Function: "+data);
                                               //$scope.casesData = data;
                                               $scope.userInfo = data;
                                               if(data.isPartner == "true"){
                                               $scope.showForPartner = true;
                                               }else{
                                               $scope.showForPartner = false;
                                               
                                               }
                                               })
                                      .error(function(err, status){
                                             $scope.handelNetworkError(err, status, 'Failed to get User info data');
                                             console.log("User failed Function: "+err);
                                             });
                                      
                                      
	if(angular.isDefined($routeParams.loaded)){
	    // If already loaded - open by URL
	    $scope.caseData = $rootScope.caseData;
	    $rootScope.caseData = null;
	} else {
	    // Else load from service
	    app.showLoading();
	    caseService.getCaseById($routeParams.id)
		    .success(function(data) {
		      // console.log(data);
		      $scope.caseData = data;
		      app.hideLoading();
		    }).error(function(err, status){
                $scope.handelNetworkError(err, status, 'Failed to get case');
		    });
	}
		  $scope.closeCase = function(id) {
		  	app.hidePopup($('.confirmPopup'));
		  	app.showLoading();
		  	caseService.closeById(id)
		  	.success(function(){
		  	    gaPlugin.trackEvent(null, null, "Case", "Closed", 'Case Number', $scope.caseData.caseNumber);
		  		$rootScope.go('home','slideLeft');
		  		app.hideLoading();
		  	}).error(function(err, status){
		  		$scope.handelNetworkError(err, status, 'Failed to close case');
		  	});
		  }

			$scope.confirmClose = function(){
				app.showPopup($('.confirmPopup'));
			}

			$scope.showRmaWarning = function(){
				app.showPopup($('.rmaWarningPopup'));
			}

    }
]);

/* comments controller*/
cxControllers.controller('commentsCtrl',['$scope','$route','$routeParams','$window','commentService',
    function($scope, $route, $routeParams, $window, commentService){
		
		if(angular.isDefined($routeParams.id)){
			app.showLoading();
			$scope.caseId = $routeParams.id;
			
			// query comments if not on new
			if($route.current.loadedTemplateUrl.indexOf('new') == -1){
				commentService.getAllByCaseId($routeParams.id)
					.success(function(data){
						$scope.commentList = data;
						app.hideLoading();
					})
					.error(function(err, status){
                        $scope.handelNetworkError(err, status, 'Failed to get comments');
					})
			} else {
				app.hideLoading();
			}
		}

		$scope.nav2Detials = function(){
			$window.history.go(-1);
		}
                                         
        $scope.toggleComment = function(e) {
            var actual = $(e.currentTarget).parent().find('.fluid');
            var current = $(e.currentTarget).find('.postedComment .inner');
            if (angular.isDefined(actual) && actual.length > 0 && actual[0] != current[0]) {
                actual.removeClass('fluid');
            }
            current.toggleClass('fluid');
        }
		
		$scope.postAdded = function(){
			if(angular.isUndefined($scope.comment) || $scope.comment == ''){
				var errorDetails = {
					message: 'Validation Error',
					details: 'Comment cannot be empty'
				};
                $scope.handelNetworkError(errorDetails, '', '');
				//app.showPopup($('.errorPopup'));
			} else { 
				app.showLoading();
				commentService.save($routeParams.id,$scope.comment)
					.success(function(data){
						app.hideLoading();
						gaPlugin.trackEvent(null, null, "Comment", "Created", "Comment Id", data.id);
						app.showPopup($('.confirmPopup'));
						window.setTimeout(function(){
							$window.history.go(-1);
						},3000);
					}).error(function(err, status){
			  		    $scope.handelNetworkError(err, status, 'Failed to get comment');
					});
			}		
			
			$('.postedComment .inner').on('click',function(){
				$(this).toggleClass('fluid');
			})
			
			/* expand list on click */
			window.setTimeout(function(){
			$('.postedComment .inner').off().on('click',function(){
				$(this).toggleClass('fluid');
			})
			
			},400);
		}
		
}]);

/* attachments controller */
cxControllers.controller('attachmentsCtrl',['$scope','$routeParams','attachmentService',
	function($scope,$routeParams,attachmentService){
		if(angular.isDefined($routeParams.id)){
			app.showLoading();
			$scope.caseId = $routeParams.id;
			// query attachments
			attachmentService.getAllByCaseId($routeParams.id)
				.success(function(data){
					$scope.attachmentList = data;
					app.hideLoading();
				})
				.error(function(err, status){
                    $scope.handelNetworkError(err, status, 'Failed to get attachments');
				});
		}
}])

/* edit Attachment  controller */
cxControllers.controller('attachmentEditCtrl',['$scope','$routeParams', '$window','attachmentService',
function($scope,$routeParams,$window, attachmentService){
	$scope.caseId = $routeParams.id;
	$scope.attachmentId = $routeParams.attachmentId;

	app.showLoading();
	attachmentService.getByCaseIdAttachmentId($scope.caseId,$scope.attachmentId)
        .success(function(data){
            $scope.attachment = data;
			app.hideLoading();
        }).error(function(err, status){
            $scope.handelNetworkError(err, status, 'Failed to get attachment');
        });

	$scope.fileEdited = function(){
		app.showPopup($('.confirmPopup'));
		window.setTimeout(function(){
			$window.location = '#/home';
		},3000);
	}	
	
	$scope.confirmDelete = function(){
		app.showPopup($('.delConfirmPopup'));
	}
	$('.closePopup').off().on('click',function(){
		$('.con header .title').html(currentTitle); currentTitle="";
		$('.overlay').hide();
		$(this).closest('.popup').hide();
	})
}
])

/* newComment controller*/
cxControllers.controller('newCommentCtrl',['$scope','$routeParams',
function($scope,$routeParams){
	$scope.navTo = function(e){
		var page = angular.element(e.currentTarget).attr('class').split( )[0];
		$rootScope.go('case-'+page, 'slideLeft');
	}
}
])

/* newAttachment  controller */
cxControllers.controller('newAttachmentCtrl',['$scope','$routeParams', '$window', '$rootScope', 'attachmentService',
function($scope,$routeParams, $window, $rootScope, attachmentService){
    $scope.libraryLabel = browseLibraryLabel;
    $scope.imageUri = null;
    $scope.fileUri = null;
    
	$scope.getPicture = function(sourceType){
	    // Get picture via native code
	    navigator.camera.getPicture(function(imageUri){
	        SFHybridApp.logToConsole(imageUri);
	        $scope.imageUri = imageUri;
	        // Resolve File URI returned from native code
	        window.resolveLocalFileSystemURI(imageUri, $scope.gotFileEntry, $scope.getFileEntryFailed);
	       
	    }, function(errorMsg){
	        SFHybridApp.logToConsole(errorMsg);
	    }, {
	        quality: 50,
	        sourceType: sourceType,
	        destinationType: Camera.DestinationType.FILE_URI
	    });
	}

    // Fetch metadata when image URI was resolved to local file URI
    $scope.gotFileEntry = function(fileEntry) {
        SFHybridApp.logToConsole("Got file entry");
 	    $scope.fileUri = fileEntry.toURL();
        SFHybridApp.logToConsole("Got file URI: " + $scope.fileUri);
        fileEntry.file($scope.gotMetadata, $scope.getFileFailed);
    }

    // Upload file when we got last modified date.
    $scope.gotMetadata = function(file) {
        SFHybridApp.logToConsole("Size: " + file.size + ", Last Modified: " + file.lastModifiedDate);
        $scope.fileObj = file;
        var reader = new FileReader();
        reader.onloadend = function (evt) {
            var tokens = evt.target.result.split(';');
            if (tokens.length > 1) {
                var prefix = tokens[0].split(':');
                if (prefix.length > 1) {
                    $scope.contentType = prefix[1];
                }
                tokens = tokens[1].split(',');
                if (tokens.length > 1) {
                    $scope.uploadFile(tokens[1]);
                }
            }
        };
        reader.readAsDataURL(file);
    }

    // Upload file with current timestamp if failed to get last modified date.
    $scope.getFileFailed = function() {
        SFHybridApp.logToConsole("Failed to get image metadata");
    }

    // Notify user if we can't resolve the image URI.
    $scope.getFileEntryFailed = function() {
        SFHybridApp.logToConsole("Failed to get file entry");
    }

    // Upload file in give base64 data
    $scope.uploadFile = function(base64Data) {
        var size = $scope.fileObj.size;
        var creationDate = $scope.fileObj.lastModifiedDate;
        SFHybridApp.logToConsole("Start to upload file " + $scope.fileUri +
            ', Last Modified: ' + creationDate + ', Size: ' + size);
        
        app.showLoading();
        if (size == 0 || size >= 5 * 1024 * 1024) {
            app.hideLoading();
            $scope.errorDetail = {
                message: 'Validation Error',
                details: 'Image file size must be less than 5MB to upload. Please reduce the file size and try again.'
            };
            app.showPopup($('.errorPopup'));
            return;
        }
        // Post to server
        var fileData = {
            name : $scope.fileObj.name,
            body : base64Data,
            contentType : $scope.contentType
        };
        attachmentService.save($routeParams.id,fileData)
            .success(function(data){
                gaPlugin.trackEvent(null, null, "Attachment", "Created", "Id",
                    data ? data.id : null);
                app.hideLoading();
                app.showPopup($('.confirmPopup'));
                window.setTimeout(function(){
                    $scope.$apply(function() {
                        $window.history.go(-1);
                    })
                },3000);
            }).error(function(err, status){
                $scope.handelNetworkError(err, status, 'Failed to create attachment');
            });
    }
}
])

/* create case controller */
cxControllers.controller('createEditCaseCtrl',['$scope','$routeParams', '$window', '$rootScope','caseService',
    'GlobalService','$route', 'metadataService',
	function($scope,$routeParams, $window, $rootScope, caseService, GlobalService, $route, metadataService){
		
		app.showLoading();
		$scope.globals = GlobalService;
		$scope.metadata = metadataService.allMetadata();

        $scope.showForPartner = false; 
        /////////////////
        // Call User Info service to get Partner data
        caseService.getUserInfo('17392111015850828802')
                        .success(function(data){
                                        // console.log("USER Success Function: "+data);
                                        $scope.userInfo = data;
                                      if(data.isPartner == "true"){
                                          $scope.showForPartner = true;
                                      }else{
                                          $scope.showForPartner = false;
                                 
                                      }
                                 })
                                     .error(function(err, status){
                                                     $scope.handelNetworkError(err, status, 'Failed to get User info data');
                                                      console.log("User failed Function: "+err);
                                       });
 
                                               
		//check if jump
		if(angular.isDefined($routeParams.jump)){
			$scope.jump = true;
		}

		if(angular.isDefined($routeParams.line)){
			caseService.currentLine = $routeParams.line;
			$scope.viewLine = (parseInt(caseService.currentLine));
			// if new line create and set dirty
			if(caseService.caseData.items.length <= $routeParams.line){
				caseService.caseData.items[$routeParams.line] = {}
			}
			$scope.currentItem = caseService.caseData.items[$routeParams.line];
			caseService.setDirty(true);
		}

		//if routeparams id, fill
		if(caseService.isDirty()){
			$scope.caseData = caseService.caseData;
			app.hideLoading();
		} else if(angular.isDefined($routeParams.id)){
			// if not dirty and routeparams id, fill
			caseService.getCaseById($routeParams.id)
				.success(function(data){
					caseService.caseData = data;
					$scope.caseData = caseService.caseData;
					app.hideLoading();
				}).error(function(err, status){
		  		    $scope.handelNetworkError(err, status, 'Failed to get case');
				});
		} else {
		    // the current user address information will be retrieved from metadata
		    // for country and state, they will be read from static-data.js
		    var currentUserInfo = metadataService.currentUserInfo();
		    var currentUserCountry = currentUserInfo['country'];

		    caseService.caseData = {};
            $scope.caseData = caseService.caseData;
			if($route.current.loadedTemplateUrl.indexOf('technical') != -1){
				caseService.caseData.type='Technical_Service_Request';
                caseService.caseData.urgency = $scope.metadata.urgencyList[1].value;
                if (currentUserCountry) {
                    caseService.caseData.incidentCountry = currentUserCountry;
                } else {
                    caseService.caseData.incidentCountry = $scope.globals.locationCountryData[0].value;
                }
                caseService.caseData.incidentCity = currentUserInfo['city'];
			} else if ($route.current.loadedTemplateUrl.indexOf('software') != -1){
				caseService.caseData.type='Software_Download_Service_Request';
                if (currentUserCountry) {
                    caseService.caseData.country = currentUserCountry;
                } else {
                    caseService.caseData.country = $scope.globals.locationCountryData[0].value;
                }
                caseService.caseData.city = currentUserInfo['city'];
                caseService.caseData.state = currentUserInfo['state'];
			} else {
				caseService.caseData.type='RMA_Service_Request';
                if (currentUserCountry) {
                    caseService.caseData.country = currentUserCountry;
                } else {
                    caseService.caseData.country = $scope.globals.locationCountryData[0].value;
                }
                caseService.caseData.city = currentUserInfo['city'];
                caseService.caseData.state = currentUserInfo['state'];
                caseService.caseData.addressLine1 = currentUserInfo['address1'];
                caseService.caseData.addressLine2 = currentUserInfo['address2'];
                caseService.caseData.postalCode = currentUserInfo['zip'];

                // init items array
				caseService.caseData.items=[];
			}
            app.hideLoading();
		}

		$scope.errorMessage ="";
		$scope.actionSaved = function(){
			app.showLoading();
			// validate
			var caseType = 'Case';
			if($route.current.loadedTemplateUrl.indexOf('technical') != -1){
				// technical validation
				caseType = 'Technical Service Case';
				if(undefinedAndNoValue($scope.caseData.currentPointRelease) 
					|| undefinedAndNoValue($scope.caseData.incidentCity)
					|| undefinedAndNoValue($scope.caseData.incidentCountry)
					|| undefinedAndNoValue($scope.caseData.problemDescription)
					|| undefinedAndNoValue($scope.caseData.problemSummary)
					|| undefinedAndNoValue($scope.caseData.productFamily)
					|| undefinedAndNoValue($scope.caseData.serviceActivity)
					|| undefinedAndNoValue($scope.caseData.specifySoftwareVersion)
					|| undefinedAndNoValue($scope.caseData.urgency)){
					$scope.errorMessage = "Error: Please fill all fields";
					app.hideLoading();
					return;
				}
					
			} else if ($route.current.loadedTemplateUrl.indexOf('software') != -1){
			    caseType = 'Software Case';
				// software validation
				if(undefinedAndNoValue($scope.caseData.currentPointRelease) 
					|| undefinedAndNoValue($scope.caseData.city)
					|| undefinedAndNoValue($scope.caseData.country)
					// || undefinedAndNoValue($scope.caseData.state)
					|| undefinedAndNoValue($scope.caseData.currentPointRelease)
					|| undefinedAndNoValue($scope.caseData.productFamily)
					//|| undefinedAndNoValue($scope.caseData.specifySoftwareVersion)
					|| undefinedAndNoValue($scope.caseData.upgradePointRelease)){
					$scope.errorMessage = "Error: Please fill all fields";
					app.hideLoading();
					return;
				}
				if ($scope.globals.locationStateData[$scope.caseData.country]
				    && undefinedAndNoValue($scope.caseData.state)) {
                    $scope.errorMessage = "Error: Please fill the state field";
                    app.hideLoading();
                    return;
				}
			}

			caseService
				.save().success(function(){
				    var actionType = angular.isDefined(caseService.caseData.id) ? 'Edited' : 'Created';
				    gaPlugin.trackEvent(null, null, caseType, actionType, "Case Number",
				        caseService.caseData.caseNumber);
					app.hideLoading();
					app.showPopup($('.confirmPopup'));
					window.setTimeout(function(){
						$scope.$apply(function() {
							$scope.resetService();
						  $rootScope.go('/home');
						})
					},3000);
				}).error(function(err, status){
						$scope.handelNetworkError(err, status, 'Failed to create case');
				});
		}

		$scope.resetService = function() {
			caseService.caseData = {};
			caseService.caseData.items = [];
			caseService.setDirty(false);
			caseService.currentLine = 0;
  		    caseService.currentItem = {};
		}

		function undefinedAndNoValue(value){
			return angular.isUndefined(value) || value == '';
		}

        $scope.setProductFamily = function(value,property){
          caseService.setCaseProperty(property,value);
          // clear current point release
          caseService.setCaseProperty('currentPointRelease','');
          if(caseType = 'Software Case'){
             caseService.setCaseProperty('upgradePointRelease','');
          }                                               
          $rootScope.go('back', 'slideRight');
        }

        $scope.setCountry = function(value,property){
            caseService.setCaseProperty('state', '');
            caseService.setCaseProperty(property,value);
            delete $scope.number;
            delete $scope.valuesToDisplay;
            $rootScope.go('back', 'slideRight');
        }

		$scope.setProperty = function(value,property){
			caseService.setCaseProperty(property,value);
            delete $scope.number;
            delete $scope.valuesToDisplay;
			$rootScope.go('back', 'slideRight');
		}

		$scope.setItemProperty = function(value,property){
            // Try to manage missing array element access here
            if (undefinedAndNoValue(caseService.caseData.items[caseService.currentLine])){
                caseService.caseData.items[caseService.currentLine] = {}
            }
			caseService.caseData.items[caseService.currentLine][property] = value;
	        $rootScope.go('back', 'slideRight');
		}
	
		$('.closePopup').off().on('click',function(){
			$('.con header .title').html(currentTitle); currentTitle="";
			$('.overlay').hide();
			$(this).closest('.popup').hide();
		})
		
		$scope.backAndClear = function(){
			$scope.resetService();
			$rootScope.go('back', 'slideRight');
		}

		$scope.confirmDelete = function(){
		    if ($scope.jump) {
		        $rootScope.go('back', 'slideRight');
		    } else {
			    app.showPopup($('.delConfirmPopup'));
		    }
		}
		
		$scope.rmaBarcode = _rmaBarcode;
		$scope.rmaLineBarcode = _rmaLineBarcode;

		$scope.barcodeSelect = function(e,item){
			$rootScope.go('/addPartNo', 'slideLeft','line='+caseService.currentLine);
		}

		$scope.addBarcode = function(){
		    var prefix = caseService.caseData.items[caseService.currentLine]['partPrefix'];
		    var suffix = caseService.caseData.items[caseService.currentLine]['partSuffix'];
			if(suffix && !isNaN(parseInt(suffix))
			    && prefix && !isNaN(parseInt(prefix))){		

			    $scope.errorMessage = null;
			    caseService.caseData.items[caseService.currentLine]['partNumberName'] = prefix + '-' + suffix;
			    $rootScope.go('back');
			} else {
			    $scope.errorMessage = 'Please enter a valid barcode';
			}
		}
		
		$scope.scanBarcode = function(){
		    barcodeScanner.scan(
		        function (result) {
		            $scope.$apply(function() {
		                caseService.caseData.items[caseService.currentLine]['partPrefix'] = null;
		                caseService.caseData.items[caseService.currentLine]['partSuffix'] = "";
		            });
		            SFHybridApp.logToConsole("We got a barcode\n" +
		                "Result: " + result.text + "\n" +
		                "Format: " + result.format + "\n" +
		                "Cancelled: " + result.cancelled);
		                if (!result.cancelled) {
		                    // barcode must in format of "prefix-suffix revision"
		                    var isValid = true;
		                    var tokens, prefix, suffix;
//		                    do {
		                        tokens = result.text.split('-');
//		                        if (tokens.length != 2) {
//		                            isValid = false;
//		                            break;
//		                        }
		                        prefix = tokens[0];
                                tokens = tokens[1].split(' ');
		                        suffix = tokens[0];
//		                    } while (0);
		                    
		                    if (isValid) {
            		            $scope.$apply(function() {
            		                caseService.caseData.items[caseService.currentLine]['partPrefix'] = prefix;
            		                caseService.caseData.items[caseService.currentLine]['partSuffix'] = suffix;
            		            });
		                    } else {
		                        navigator.notification.alert("The part number is invalid", null, "Message");
		                    }
		                }
		        }, 
		        function (error) {
		            SFHybridApp.logToConsole("Scanning failed: " + error);
		        },
		        1 // isPartNumber
		    );
		}

		$scope.scanSerialNumber = function(){
		    barcodeScanner.scan(
		        function (result) {
		            SFHybridApp.logToConsole("We got a barcode\n" +
		                "Result: " + result.text + "\n" +
		                "Format: " + result.format + "\n" +
		                "Cancelled: " + result.cancelled);
		                $scope.$apply(function() {
		                    caseService.caseData.items[caseService.currentLine]['serialNumber'] = '';
		                });
		                if (!result.cancelled) {
	                        $scope.$apply(function() {
	                            caseService.caseData.items[caseService.currentLine]['serialNumber'] = result.text;
	                        });
		                }
		        }, 
		        function (error) {
		            SFHybridApp.logToConsole("Scanning failed: " + error);
		        }
		    );
		}

		if(activeEl.length>0 && activeEl.hasClass('rmaLineBarcode')){
			_rmaLineBarcode = activeEl.text();
			$('.rmaLineBarcode').removeClass('isPH').text(_rmaLineBarcode);
		}else if(activeEl.length>0 && activeEl.hasClass('rmaBarcode')){
			_rmaBarcode = activeEl.text();
			$('.rmaBarcode').removeClass('isPH').text(_rmaBarcode);
		}
		
        //Create RMA record
		$scope.toLine = function(){
			//go to create first line, validate first
			if(undefinedAndNoValue($scope.caseData.city)
				|| undefinedAndNoValue($scope.caseData.country)
				// || undefinedAndNoValue($scope.caseData.state)
				|| undefinedAndNoValue($scope.caseData.addressLine1)
				//|| undefinedAndNoValue($scope.caseData.serviceActivity)
               ){
				$scope.errorMessage = "Error: Please fill all fields.";
				return;
			}
            if ($scope.globals.locationStateData[$scope.caseData.country]
                && undefinedAndNoValue($scope.caseData.state)) {
                $scope.errorMessage = "Error: Please fill the state field";
                app.hideLoading();
                return;
            }

			$rootScope.go('/createRMALine','','line=0');
		}
		
		/* add another line */
		$scope.newLine = function(){
			// validate line
			if(undefinedAndNoValue($scope.currentItem.partNumberName)
				|| undefinedAndNoValue($scope.currentItem.serialNumber)
				|| undefinedAndNoValue($scope.currentItem.failureCode)
                || undefinedAndNoValue($scope.currentItem.serviceActivity)
				|| undefinedAndNoValue($scope.currentItem.descriptionOfFailure)){
				$scope.errorMessage = "Error: Please fill all fields";
				return;
			}
			caseService.caseData.items[$scope.viewLine] = $scope.currentItem;
			$rootScope.go('/createRMALine','','line='+(parseInt(caseService.currentLine) + 1));
		}

		$scope.cancel = function(){
			$scope.resetService();
			$rootScope.go('home', 'slideRight');
		}

		$scope.back = function(){
            if(undefinedAndNoValue($scope.currentItem.partNumberName)
				|| undefinedAndNoValue($scope.currentItem.serialNumber)
				|| undefinedAndNoValue($scope.currentItem.failureCode)
                || undefinedAndNoValue($scope.currentItem.serviceActivity)
				|| undefinedAndNoValue($scope.currentItem.descriptionOfFailure)){
				delete caseService.caseData.items[caseService.currentLine];
			}
			if(caseService.currentLine == 0){
				//go back to the first screen
				$rootScope.go('/createRma');
			} else {
				//go back one item
				$rootScope.go('/createRMALine','','line='+(parseInt(caseService.currentLine) -1));
			}
		}
		
		/* increment line item n navigate next */
		$scope.review = function(){
			if(undefinedAndNoValue($scope.currentItem.partNumberName)
				|| undefinedAndNoValue($scope.currentItem.serialNumber)
				|| undefinedAndNoValue($scope.currentItem.failureCode)
				|| undefinedAndNoValue($scope.currentItem.descriptionOfFailure)){
				$scope.errorMessage = "Error: Please fill all fields";
				return;
			}
			$rootScope.go('/rmaReview');
		}
		/* Open RMA Policy link in system browser */
		$scope.openRmaPolicy = function(){
		    window.open(rmaPolicyLink, '_system');
		}

        $scope.hasSoftwareRelease =
            $scope.currentItem && metadataService.hasSoftwareRelease($scope.currentItem.partNumberName);

        $scope.getSoftwareReleaseDisplayName = function(softwareReleaseId) {
            if (!$scope.hasSoftwareRelease) {
                return "";
            } else {
                return metadataService.getSoftwareReleaseDisplayName(
                	$scope.currentItem.partNumberName, softwareReleaseId);
            }
        }
    }
])

/* verify Rma Case Ctrl */
cxControllers.controller('verifyRmaCaseCtrl',['$scope','$routeParams', '$window', '$rootScope','caseService',
    'metadataService',
	function($scope,$routeParams, $window, $rootScope,caseService, metadataService) {
		$scope.caseData = caseService.caseData;
		$scope.actionSaved = function(){
			app.showLoading();
			caseService.save()
				.success(function(){
				    var actionType = angular.isDefined(caseService.caseData.id) ? 'Edited' : 'Created';
				    gaPlugin.trackEvent(null, null, 'RMA Case', actionType, "Case Number",
				        caseService.caseData.caseNumber);
					app.hideLoading();
					app.showPopup($('.confirmPopup'));
					window.setTimeout(function(){
						$scope.$apply(function() {
							$scope.resetService();
						  $rootScope.go('/home');
						})
					},3000);
				}).error(function(err, status){
					$scope.handelNetworkError(err, status, 'Failed to create case');
				})
		}

		$scope.cancel = function(){
			$scope.resetService();
			$rootScope.go('home', 'slideRight');
		}

		$scope.resetService = function() {
			caseService.caseData = {};
			caseService.caseData.items = [];
			caseService.setDirty(false);
			caseService.currentLine = 0;
  		caseService.currentItem = {};
		}
	
		$('.closePopup').off().on('click',function(){
			$('.con header .title').html(currentTitle); currentTitle="";
			$('.overlay').hide();
			$(this).closest('.popup').hide();
		})
		
		$scope.confirmDelete = function(){
			app.showPopup($('.delConfirmPopup'));
		}
		
		/* increment line item n navigate next */
		$scope.incLineNgo=function(){
			lineItmes+=1;
			$rootScope.go('/rmaReview');
		}
		
		if(lineItmes>1){
			$('.rmaLineItem.item2').show();
		}

        $scope.hasSoftwareRelease = metadataService.hasSoftwareRelease;
        $scope.getSoftwareReleaseDisplayName = metadataService.getSoftwareReleaseDisplayName;
	}
])


var lineItmes=0;
var activeEl = "";
var _rmaBarcode = "Add Part Number";
var _rmaLineBarcode = "Add Part Number";
