cxApp.factory('GlobalService', function() {
  return {
    reportedSeverityData : [
        {label:'--None--',value:'--None--'},
        {label:'Severe',value:'Severe'},
        {label:'Not so severe',value:'Not so severe'}
      ],
    urgencyData : [
        {label:'P1:Response-2 Hours',value:'P1:Response-2 Hours'},
        {label:'P2:Response-Next Business Day',value:'P2:Response-Next Business Day'}
      ],
    serviceActivityData : [
        {label:'--None--',value:'--None--'},
        {label:'IN-SERVICE',value:'IN-SERVICE'},
        {label:'MAINTENANCE',value:'MAINTENANCE'},
        {label:'UPGRADE',value:'UPGRADE'}
      ],
    productFamilyData : [
        {label:'--None--',value:'--None--'},
        {label:'B-Series',value:'B-Series'},
        {label:'BLM 1500 Series',value:'BLM 1500 Series'},
        {label:'C-Series',value:'C-Series'},
        {label:'E-Series',value:'E-Series'},
        {label:'F-Series',value:'F-Series'},
        {label:'P-Series (ONTs)',value:'P-Series (ONTs)'},
        {label:'T-Series (ONTs)',value:'T-Series (ONTs)'},
        {label:'Software Products',value:'Software Products'},
        {label:'Others',value:'Others'}
      ],
    currentPointReleaseData : [
        {label:'--None--',value:'--None--'},
        {label:'C7 R1.X',value:'C7 R1.X'},
        {label:'C7 R2.X',value:'C7 R2.X'},
        {label:'C7 R3.X',value:'C7 R3.X'},
        {label:'C7 R4.X',value:'C7 R4.X'},
        {label:'C7 R5.X',value:'C7 R5.X'},
        {label:'C7 R6.0',value:'C7 R6.0'},
        {label:'C7 R6.1',value:'C7 R6.1'},
        {label:'C7 R7.0',value:'C7 R7.0'},
        {label:'C7 R7.2',value:'C7 R7.2'},
        {label:'C7 R8.0',value:'C7 R8.0'},
        {label:'C7 R8.0.3xx',value:'C7 R8.0.3xx'}
      ],
    incidentCountryData : [
      {label:'United States',value:'United States'},{label:'Afghanistan',value:'Afghanistan'},{label:'Albania',value:'Albania'},{label:'Algeria',value:'Algeria'},{label:'American Samoa',value:'American Samoa'},{label:'Andorra',value:'Andorra'},{label:'Angola',value:'Angola'},{label:'Anguilla',value:'Anguilla'},{label:'Antarctica',value:'Antarctica'},{label:'Antigua and Barbuda',value:'Antigua and Barbuda'},{label:'Argentina',value:'Argentina'},{label:'Armenia',value:'Armenia'},{label:'Aruba',value:'Aruba'},{label:'Australia',value:'Australia'},{label:'Austria',value:'Austria'},{label:'Azerbaijan',value:'Azerbaijan'},{label:'Bahamas',value:'Bahamas'},{label:'Bahrain',value:'Bahrain'},{label:'Bangladesh',value:'Bangladesh'},{label:'Barbados',value:'Barbados'},{label:'Belarus',value:'Belarus'},{label:'Belgium',value:'Belgium'},{label:'Belize',value:'Belize'},{label:'Benin',value:'Benin'},{label:'Bermuda',value:'Bermuda'},{label:'Bhutan',value:'Bhutan'},{label:'Bolivia',value:'Bolivia'},{label:'Bosnia and Herzegovina',value:'Bosnia and Herzegovina'},{label:'Botswana',value:'Botswana'},{label:'Bouvet Island',value:'Bouvet Island'},{label:'Brazil',value:'Brazil'},{label:'British Antarctic Territory',value:'British Antarctic Territory'},{label:'British Indian Ocean Territory',value:'ritish Indian Ocean Territory'},{label:'British Virgin Islands',value:'British Virgin Islands'},{label:'Brunei',value:'Brunei'},{label:'Bulgaria',value:'Bulgaria'},{label:'Burkina Faso',value:'Burkina Faso'},{label:'Burundi',value:'Burundi'},{label:'Cambodia',value:'Cambodia'},{label:'Cameroon',value:'Cameroon'},{label:'Canada',value:'Canada'},{label:'Canton and Enderbury Islands',value:'Canton and Enderbury Islands'},{label:'Cape Verde',value:'Cape Verde'},{label:'Cayman Islands',value:'Cayman Islands'},{label:'Central African Republic',value:'Central African Republic'},{label:'Chad',value:'Chad'},{label:'Chile',value:'Chile'},{label:'China',value:'China'},{label:'Christmas Island',value:'Christmas Island'},{label:'Cocos [Keeling] Islands',value:'Cocos [Keeling] Islands'},{label:'Colombia',value:'Colombia'},{label:'Comoros',value:'Comoros'},{label:'Congo - Brazzaville',value:'Congo - Brazzaville'},{label:'Congo - Kinshasa',value:'Congo - Kinshasa'},{label:'Cook Islands',value:'Cook Islands'},{label:'Costa Rica',value:'Costa Rica'},{label:'Croatia',value:'Croatia'},{label:'Cuba',value:'Cuba'},{label:'Cyprus',value:'Cyprus'},{label:'Czech Republic',value:'Czech Republic'},{label:'Côte d’Ivoire',value:'Côte d’Ivoire'},{label:'Denmark',value:'Denmark'},{label:'Djibouti',value:'Djibouti'},{label:'Dominica',value:'Dominica'},{label:'Dominican Republic',value:'Dominican Republic'},{label:'Dronning Maud Land',value:'Dronning Maud Land'},{label:'East Germany',value:'East Germany'},{label:'Ecuador',value:'Ecuador'},{label:'Egypt',value:'Egypt'},{label:'El Salvador',value:'El Salvador'},{label:'Equatorial Guinea',value:'Equatorial Guinea'},{label:'Eritrea',value:'Eritrea'},{label:'Estonia',value:'Estonia'},{label:'Ethiopia',value:'Ethiopia'},{label:'Falkland Islands',value:'Falkland Islands'},{label:'Faroe Islands',value:'Faroe Islands'},{label:'Fiji',value:'Fiji'},{label:'Finland',value:'Finland'},{label:'France',value:'France'},{label:'French Guiana',value:'French Guiana'},{label:'French Polynesia',value:'French Polynesia'},{label:'Gabon',value:'Gabon'},{label:'Gambia',value:'Gambia'},{label:'Georgia',value:'Georgia'},{label:'Germany',value:'Germany'},{label:'Ghana',value:'Ghana'},{label:'Gibraltar',value:'Gibraltar'},{label:'Greece',value:'Greece'},{label:'Greenland',value:'Greenland'},{label:'Grenada',value:'Grenada'},{label:'Guadeloupe',value:'Guadeloupe'},{label:'Guam',value:'Guam'},{label:'Guatemala',value:'Guatemala'},{label:'Guernsey',value:'Guernsey'},{label:'Guinea',value:'Guinea'},{label:'Guyana',value:'Guyana'},{label:'Haiti',value:'Haiti'},{label:'Honduras',value:'Honduras'},{label:'Hong Kong SAR China',value:'Hong Kong SAR China'},{label:'Hungary',value:'Hungary'},{label:'Iceland',value:'Iceland'},{label:'India',value:'India'},{label:'Indonesia',value:'Indonesia'},{label:'Iran',value:'Iran'},{label:'Iraq',value:'Iraq'},{label:'Ireland',value:'Ireland'},{label:'Israel',value:'Israel'},{label:'Italy',value:'Italy'},{label:'Jamaica',value:'Jamaica'},{label:'Japan',value:'Japan'},{label:'Jersey',value:'Jersey'},{label:'Jordan',value:'Jordan'},{label:'Kazakhstan',value:'Kazakhstan'},{label:'Kenya',value:'Kenya'},{label:'Kiribati',value:'Kiribati'},{label:'Kuwait',value:'Kuwait'},{label:'Kyrgyzstan',value:'Kyrgyzstan'},{label:'Laos',value:'Laos'},{label:'Latvia',value:'Latvia'},{label:'Lebanon',value:'Lebanon'},{label:'Lesotho',value:'Lesotho'},{label:'Liberia',value:'Liberia'},{label:'Libya',value:'Libya'},{label:'Liechtenstein',value:'Liechtenstein'},{label:'Lithuania',value:'Lithuania'},{label:'Luxembourg',value:'Luxembourg'},{label:'Macau SAR China',value:'Macau SAR China'},{label:'Macedonia',value:'Macedonia'},{label:'Madagascar',value:'Madagascar'},{label:'Malawi',value:'Malawi'},{label:'Malaysia',value:'Malaysia'},{label:'Maldives',value:'Maldives'},{label:'Mali',value:'Mali'},{label:'Malta',value:'Malta'},{label:'Martinique',value:'Martinique'},{label:'Mauritania',value:'Mauritania'},{label:'Mauritius',value:'Mauritius'},{label:'Mayotte',value:'Mayotte'},{label:'Mexico',value:'Mexico'},{label:'Micronesia',value:'Micronesia'},{label:'Moldova',value:'Moldova'},{label:'Monaco',value:'Monaco'},{label:'Mongolia',value:'Mongolia'},{label:'Montenegro',value:'Montenegro'},{label:'Montserrat',value:'Montserrat'},{label:'Morocco',value:'Morocco'},{label:'Mozambique',value:'Mozambique'},{label:'Myanmar [Burma]',value:'Myanmar [Burma]'},{label:'Namibia',value:'Namibia'},{label:'Nauru',value:'Nauru'},{label:'Nepal',value:'Nepal'},{label:'Netherlands',value:'Netherlands'},{label:'Netherlands Antilles',value:'Netherlands Antilles'},{label:'New Zealand',value:'New Zealand'},{label:'Nicaragua',value:'Nicaragua'},{label:'Niger',value:'Niger'},{label:'Nigeria',value:'Nigeria'},{label:'Niue',value:'Niue'},{label:'Norfolk Island',value:'Norfolk Island'},{label:'North Korea',value:'North Korea'},{label:'Norway',value:'Norway'},{label:'Oman',value:'Oman'},{label:'Pakistan',value:'Pakistan'},{label:'Palau',value:'Palau'},{label:'Panama',value:'Panama'},{label:'Papua New Guinea',value:'Papua New Guinea'},{label:'Paraguay',value:'Paraguay'},{label:'Peru',value:'Peru'},{label:'Philippines',value:'Philippines'},{label:'Poland',value:'Poland'},{label:'Portugal',value:'Portugal'},{label:'Puerto Rico',value:'Puerto Rico'},{label:'Qatar',value:'Qatar'},{label:'Romania',value:'Romania'},{label:'Russia',value:'Russia'},{label:'Rwanda',value:'Rwanda'},{label:'Réunion',value:'Réunion'},{label:'Samoa',value:'Samoa'},{label:'San Marino',value:'San Marino'},{label:'Saudi Arabia',value:'Saudi Arabia'},{label:'Senegal',value:'Senegal'},{label:'Serbia',value:'Serbia'},{label:'Serbia and Montenegro',value:'Serbia and Montenegro'},{label:'Seychelles',value:'Seychelles'},{label:'Sierra Leone',value:'Sierra Leone'},{label:'Singapore',value:'Singapore'},{label:'Slovakia',value:'Slovakia'},{label:'Slovenia',value:'Slovenia'},{label:'Solomon Islands',value:'Solomon Islands'},{label:'Somalia',value:'Somalia'},{label:'South Africa',value:'South Africa'},{label:'South Korea',value:'South Korea'},{label:'Spain',value:'Spain'},{label:'Sri Lanka',value:'Sri Lanka'},{label:'Sudan',value:'Sudan'},{label:'Suriname',value:'Suriname'},{label:'Swaziland',value:'Swaziland'},{label:'Sweden',value:'Sweden'},{label:'Switzerland',value:'Switzerland'},{label:'Syria',value:'Syria'},{label:'Taiwan',value:'Taiwan'},{label:'Tajikistan',value:'Tajikistan'},{label:'Tanzania',value:'Tanzania'},{label:'Thailand',value:'Thailand'},{label:'Togo',value:'Togo'},{label:'Tokelau',value:'Tokelau'},{label:'Tonga',value:'Tonga'},{label:'Trinidad and Tobago',value:'Trinidad and Tobago'},{label:'Tunisia',value:'Tunisia'},{label:'Turkey',value:'Turkey'},{label:'Turkmenistan',value:'Turkmenistan'},{label:'Tuvalu',value:'Tuvalu'},{label:'Uganda',value:'Uganda'},{label:'Ukraine',value:'Ukraine'},{label:'United Arab Emirates',value:'United Arab Emirates'},{label:'United Kingdom',value:'United Kingdom'},{label:'Uruguay',value:'Uruguay'},{label:'Uzbekistan',value:'Uzbekistan'},{label:'Vanuatu',value:'Vanuatu'},{label:'Venezuela',value:'Venezuela'},{label:'Vietnam',value:'Vietnam'},{label:'Yemen',value:'Yemen'},{label:'Zambia',value:'Zambia'},{label:'Zimbabwe',value:'Zimbabwe'}
      ],
    incidentStateData : [
      {label:'--None--', value:'--None--'},
      {label:'Alabama', value:'Alabama'},
      {label:'Alaska', value:'Alaska'},
      {label: 'American Samoa', value:'American Samoa'},
      {label:'Arizona', value:'Arizona'},
      {label:'Arkansas', value:'Arkansas'},
      {label:'California', value:'California'},
      {label:'Colorado', value:'Colorado'},
      {label:'Connecticut', value:'Connecticut'},
      {label:'Delaware', value:'Delaware'},
      {label:'District Of Columbia', value:'District Of Columbia'},
      {label:'Federated States Of Micronesia',value:'Federated States Of Micronesia'},
      {label:'Florida', value:'Florida'},
      {label:'Georgia', value:'Georgia'},
      {label:'Guam', value:'Guam'},
      {label:'Hawaii', value:'Hawaii'},
      {label:'Idaho', value:'Idaho'},
      {label:'Illinois', value:'Illinois'},
      {label:'Indiana', value:'Indiana'},
      {label:'Iowa', value:'Iowa'},
      {label:'Kansas', value:'Kansas'},
      {label:'Kentucky', value:'Kentucky'},
      {label:'Louisiana', value:'Louisiana'},
      {label:'Maine', value:'Maine'},
      {label:'Marshall Islands',value:'Marshall Islands'},
      {label:'Maryland', value:'Maryland'},
      {label:'Massachusetts', value:'Massachusetts'},
      {label:'Michigan', value:'Michigan'},
      {label:'Minnesota', value:'Minnesota'},
      {label:'Mississippi', value:'Mississippi'},
      {label:'Missouri', value:'Missouri'},
      {label:'Montana', value:'Montana'},
      {label:'Nebraska', value:'Nebraska'},
      {label:'Nevada', value:'Nevada'},
      {label:'New Hampshire', value:'New Hampshire'},
      {label:'New Jersey', value:'New Jersey'},
      {label:'New Mexico',value:'New Mexico'},
      {label:'New York', value:'New York'},
      {label:'North Carolina', value:'North Carolina'},
      {label:'North Dakota', value:'North Dakota'},
      {label:'Northern Mariana Islands', value:'Northern Mariana Islands'},
      {label:'Ohio', value:'Ohio'},
      {label:'Oklahoma', value:'Oklahoma'},
      {label:'Oregon', value:'Oregon'},
      {label:'Palau', value:'Palau'},
      {label:'Pennsylvania', value:'Pennsylvania'},
      {label:'Puerto Rico', value:'Puerto Rico'},
      {label:'Rhode Island', value:'Rhode Island'},
      {label:'South Carolina', value:'South Carolina'},
      {label:'South Dakota', value:'South Dakota'},
      {label:'Tennessee', value:'Tennessee'},
      {label:'Texas', value:'Texas'},
      {label:'Utah', value:'Utah'},
      {label:'Vermont', value:'Vermont'},
      {label:'Virgin Islands', value:''},
      {label:'Virginia', value:'Virginia'},
      {label:'Washington', value:'Washington'},
      {label:'West Virginia', value:'West Virginia'},
      {label:'Wisconsin', value:'Wisconsin'},
      {label:'Wyoming', value:'Wyoming'}
    ],

    upgradePointReleaseData : [
      {label:'--None--',value:'--None--'},
      {label:'C7 R1.X',value:'C7 R1.X'},
      {label:'C7 R2.X',value:'C7 R2.X'},
      {label:'C7 R3.X',value:'C7 R3.X'},
      {label:'C7 R4.X',value:'C7 R4.X'},
      {label:'C7 R5.X',value:'C7 R5.X'},
      {label:'C7 R6.0',value:'C7 R6.0'},
      {label:'C7 R6.1',value:'C7 R6.1'},
      {label:'C7 R7.0',value:'C7 R7.0'},
      {label:'C7 R7.2',value:'C7 R7.2'},
      {label:'C7 R8.0',value:'C7 R8.0'},
      {label:'C7 R8.0.3xx',value:'C7 R8.0.3xx'}
    ],
    networkEnvironmentData : [
      {label:'Production',value:'Production'},
      {label:'Lab',value:'Lab'}
    ],
    failureCodeData : [
      {label:'Card Failure',value:'Card Failure'},
      {label:'Boot Failure',value:'Boot Failure'},
      {label:'Degraded Performance',value:'Degraded Performance'},
      {label:'Port Failure',value:'Port Failure'},
      {label:'Software Failure',value:'Software Failure'}
    ],
    codePrefixData : [
      {label:'790',value:'790'},
      {label:'791',value:'791'}
    ]
  };
});