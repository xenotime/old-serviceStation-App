// Represents the Google Analysis tracking ID.
var gaTrackingId = "UA-48462177-1";

// Represents the Google Analysis dispatch interval in seconds.
// All hits will be queued and sent per interval to reduce network calls in order to improve performance.
// This may be ignored on Android devices with Google Play Service
var gaDispatchInterval = 20;

// Represents the Calix salesforce REST service endpoint
var serviceEndpoint = 'https://cs30.salesforce.com/services/apexrest/';

// The mock mapping of part number to part number id
var partNumber2Id = {
    '791-00002' : 'a0Sn0000000Ccax',
    '790-00036' : 'a0Sn0000000CcSp',
    '790-00033' : 'a0Sn0000000CcRc',
    '790-00032' : 'a0Sn0000000CcVU',
    '790-00027' : 'a0Sn0000000CcRE',
    '790-00026' : 'a0Sn0000000CcfE'
};
var DEFAULT_PART_NUMBER_ID = 'a0Sn0000000Ei9I';