/*angular functions*/
var cxApp = angular.module('cxApp', [
  'ngRoute',  
  'ngAnimate',
  'cxControllers'
]);

// Initialize the main module
cxApp.run(['$rootScope', '$location', '$window', '$http', function ($rootScope, $location, $window, $http) {
  
  $rootScope.closePopup = function (popup){
    app.hidePopup($(popup));  
  };

  $rootScope.go = function (path, pageAnimationClass, search) {
          window.clearTimeout();
                    
            if (typeof(pageAnimationClass) === 'undefined') { // Use a default, your choice
                $rootScope.pageAnimationClass = 'crossFade';
            }
            
            else { // Use the specified animation
            pageAnimationClass+= " inMotion";
                $rootScope.pageAnimationClass = pageAnimationClass;
                $window.setTimeout(function(){
                  $rootScope.pageAnimationClass = '';
                  $('.pageView').removeClass('inMotion').removeClass('slideLeft').removeClass('slideRight');
                  $('.fixedActions .caseActions').css('bottom','0.1px');
                  $window.setTimeout(function(){
                    $('article.screen .con').css('min-height',$('#wrapper').height()+'px');                  
                  },10);
                },700);
                
            }
            window.setTimeout(function(){
              $('article.screen .con').css('min-height',$('#wrapper').height()+'px');                  
            },100);
    
            if (path === 'back') { // Allow a 'back' keyword to go to previous page              
              $window.history.back();              
            }
            
            else { // Go to the specified path
                $location.path(path);
                if(angular.isDefined(search)){
                  $location.search(search);
                }else{
                  //reset search
                  $location.search('');
                }
            };
       
    };
    
    $rootScope.handleOpenUrl = function(openUrl) {
        if (!openUrl) return;
        SFHybridApp.logToConsole("process open url: " + openUrl);
        // Parse calix://view?case={id} to view case
        if (openUrl.indexOf("calix://view?") == 0) {
            var tokens = openUrl.split('?');
            if (tokens.length == 2) {
                var query = tokens[1];
                tokens = query.split('=');
                if (tokens.length == 2) {
                    var param = tokens[0];
                    var caseId = tokens[1];
                    if (param == 'case' && caseId) {
                        app.showLoading();
                        $http({
                            method: 'GET',
                            url: serviceEndpoint + 'caseServices/' + caseId,
                            headers: {'Authorization':'OAuth '+forcetkClient.sessionId}
                        }).success(function(data) {
                            var page = null;
                            if(data.type === 'Technical_Service_Request'){
                                page = 'technical-service';
                            } else if (data.type === 'Software_Download_Service_Request') {
                                page = 'software'
                            } else {
                                page = 'rma';
                            }
                            $rootScope.caseData = data;
                            $rootScope.go('/case-' + page,'slideLeft','id='+caseId+'&loaded=true');
                        }).error(function(err){
                            navigator.notification.alert("Cannot load given case", null, "Message");
                        });
                    }
                }
            }
        }
    }

    $rootScope.$on("$locationChangeStart", function(event, next, current) {
        var pathes = next.split('#/');
        if (pathes.length > 1) {
            var screenName = pathes[1];
            gaPlugin.trackPage(null, null, screenName);
        } else {
            SFHybridApp.logToConsole("Cannot track page, invalid location: " + next);
        }
    });
}]);


cxApp.factory('authHttpResponseInterceptor',['$q','$location','$rootScope',function($q,$location,$rootScope){
    return {
        response: function(response){
            if (response.status === 401) {
            }
            return response || $q.when(response);
        },
        responseError: function(rejection) {
            if (rejection.status === 401) {
                // Force logout on 401 error
                var sfOAuthPlugin = cordova.require("salesforce/plugin/oauth");
                sfOAuthPlugin.logout();
            }
            return $q.reject(rejection);
        }
    }
}]);

/* page routing */
cxApp.config(['$routeProvider','$httpProvider', function($routeProvider,$httpProvider) {

    // cors
    $httpProvider.defaults.useXDomain = true;
    delete $httpProvider.defaults.headers.common['X-Requested-With'];

    // 401 Unauthorized handling
    $httpProvider.interceptors.push('authHttpResponseInterceptor');

    $routeProvider.
      when('/home',{
        templateUrl: 'partials/home.html'
      }).
      when('/case-technical-service',{
        templateUrl: 'partials/case-technical-service.html'
      }).
      when('/case-software',{
        templateUrl: 'partials/case-software.html'
      }).
      when('/case-rma',{
        templateUrl: 'partials/case-rma.html'
      }).
      when('/homeSearch',{
        templateUrl: 'partials/home-search.html'
      }).
      when('/comments',{
        templateUrl: 'partials/comments.html'
      }).
      when('/commentsNone',{
        templateUrl: 'partials/comment-none.html'
      }).
      when('/attachments',{
        templateUrl: 'partials/attachments.html'
      }).      
      when('/edit-case-software',{
        templateUrl: 'partials/case-software-edit.html'
      }).
      when('/edit-case-technical-service',{
        templateUrl: 'partials/case-technical-service-edit.html'
      }).
      when('/edit-case-rma',{
        templateUrl: 'partials/case-rma-edit.html'
      }).
      when('/newcomment',{
        templateUrl: 'partials/comment-new.html',
        controller: 'newCommentCtrl'
      }).
      when('/newAttachments',{
        templateUrl: 'partials/attachment-new.html'
      }).
      when('/editAttachment',{
        templateUrl: 'partials/attachment-edit.html'
      }).
      when('/createTechnicalService',{
        templateUrl: 'partials/case-technical-service-create.html'
      }).      
      when('/createSoftware',{
        templateUrl: 'partials/case-software-create.html'
      }).      
      when('/createRma',{
        templateUrl: 'partials/case-rma-create.html',
        reloadOnSearch: true
      }).       
      when('/createRmaJump',{
        templateUrl: 'partials/case-rma-create-jump.html'
      }).    
      when('/createRMALine',{
        templateUrl: 'partials/case-rma-create-line.html',
        reloadOnSearch: true
      }).   
      when('/addPartNo',{
        templateUrl: 'partials/addPartNo.html'
      }).    
      when('/createRMAVerify',{
        templateUrl: 'partials/case-rma-create-verify.html'
      }).
      when('/rmaReview',{
        templateUrl: 'partials/case-rma-review.html'
      }).
      when('/picklist',{
        templateUrl: 'partials/picklist-opts.html'
      }).
      otherwise({
        redirectTo: '/home'
      });

  }]);